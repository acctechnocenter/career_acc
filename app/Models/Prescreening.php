<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class prescreening extends Sximo  {
	
	protected $table = 'acc_jawaban';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_jawaban.* FROM acc_jawaban  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_jawaban.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
