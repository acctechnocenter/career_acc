<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class testimonial extends Sximo  {
	
	protected $table = 'acc_testimonial';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_testimonial.* FROM acc_testimonial  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_testimonial.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
