<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class soal extends Sximo  {
	
	protected $table = 'acc_soal';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_soal.* FROM acc_soal  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_soal.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
