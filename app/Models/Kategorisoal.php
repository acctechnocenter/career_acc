<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class kategorisoal extends Sximo  {
	
	protected $table = 'acc_kategori_soal';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_kategori_soal.* FROM acc_kategori_soal  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_kategori_soal.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
