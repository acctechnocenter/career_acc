<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class job extends Sximo  {
	
	protected $table = 'acc_job';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_job.* FROM acc_job  ";
	}	

	public static function queryWhere(  ){
		
		return " WHERE acc_job.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
