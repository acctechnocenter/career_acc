<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class candidateprofile extends Sximo  {
    
    use Notifiable;
	
	protected $table = 'acc_candidate_profile';
	protected $primaryKey = 'id';
	
	protected $fillable = [
	    'id_user','title_profile', 'gender', 'dateofbirth', 'provinceofbirth', 'cityofbirth', 'identitycardnumber', 'homephone', 'mobilephone', 'maritalstatus', 'religion','lasteducation'
	];

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_candidate_profile.* FROM acc_candidate_profile  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_candidate_profile.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
