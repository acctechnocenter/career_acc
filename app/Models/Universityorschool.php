<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class universityorschool extends Sximo  {
	
	protected $table = 'acc_universityorschool';
	protected $primaryKey = 'Id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_universityorschool.* FROM acc_universityorschool  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_universityorschool.Id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
