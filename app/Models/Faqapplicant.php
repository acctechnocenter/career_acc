<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class faqapplicant extends Sximo  {
	
	protected $table = 'acc_faq_applicant';
	protected $primaryKey = 'Id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_faq_applicant.* FROM acc_faq_applicant  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_faq_applicant.Id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
