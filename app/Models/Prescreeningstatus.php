<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class prescreeningstatus extends Sximo  {
	
	protected $table = 'acc_prescreening_status';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_prescreening_status.* FROM acc_prescreening_status  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_prescreening_status.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
