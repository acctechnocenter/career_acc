<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class faq extends Sximo  {
	
	protected $table = 'acc_faq';
	protected $primaryKey = 'Id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_faq.* FROM acc_faq  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_faq.Id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
