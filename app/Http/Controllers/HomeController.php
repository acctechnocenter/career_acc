<?php  namespace App\Http\Controllers;

use App\Models\Post;
use App\Library\Markdown;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 

class HomeController extends Controller {

	public function __construct()
	{
		parent::__construct();

		$this->data['pageLang'] = 'en';
		if(\Session::get('lang') != '')
		{
			$this->data['pageLang'] = \Session::get('lang');
		}	
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index( Request $request) 
	{
        \App::setLocale(\Session::get('lang'));

		if(config('sximo.cnf_front') =='false' && $request->segment(1) =='' ) :
			return redirect('dashboard');
		endif; 	

		$page = $request->segment(1);
		\DB::table('tb_pages')->where('alias',$page)->update(array('views'=> \DB::raw('views+1')));

		
		if($page !='') {


		    //Slider
			$slider = \DB::table('acc_slider')->orderBy('createdOn','desc')->limit(5)->get();
		    //Testimonial
			$testimonial = \DB::table('acc_testimonial')->orderBy('createdOn','desc')->limit(6)->get();
			//newskotak			
			$newskotak = \DB::table('tb_pages')->where('pagetype', 'LIKE', '%post%')->orderBy('pageID','desc')->limit(7)->get();

			$sql = \DB::table('tb_pages')->where('alias','=',$page)->where('status','=','enable')->get();
			$row = $sql[0];
			if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/template/'.$row->filename.'.blade.php') && $row->filename !='')
			{
				$page_template = 'layouts.'.config('sximo.cnf_theme').'.template.'.$row->filename;
			} else {
				$page_template = 'layouts.'.config('sximo.cnf_theme').'.template.page';
			}	

			if($row->access !='')
			{
				$access = json_decode($row->access,true);	
			} else {
				$access = array();
			}	

			// If guest not allowed 
			if($row->allow_guest !=1)
			{	
				$group_id = \Session::get('gid');				
				$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );	
				if($isValid ==0)
				{
					return redirect('')
						->with(['message' => __('core.note_restric') ,'status'=>'error']);				
				}
			}

			$this->data['pages'] = $page_template;				
			$this->data['title'] = $row->title ;
			$this->data['subtitle'] = $row->sinopsis ;
			$this->data['pageID'] = $row->pageID ;
			$this->data['content'] = \PostHelpers::formatContent($row->note);
			$this->data['note'] = $row->note;
			if($row->template =='frontend'){
				$page = 'layouts.'.config('sximo.cnf_theme').'.index';
			}
			else {
				return view($page_template, $this->data);
				
			}
			
			return view( $page, $this->data);			
		}
		else {
		    
		    //job list
		    //$result = \DB::table('acc_jobfield')->where('status_jobfield','active')->orderBy('id','desc')->get();
			$result = \DB::table('acc_job')->leftJoin('acc_jobfield','acc_job.IDJobField','=','acc_jobfield.id')
						->where([['acc_job.StatusJob','active'],['acc_jobfield.status_jobfield','active']])
						->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
						->select('acc_jobfield.*')->groupBy('acc_job.IDJobField')->get();
		    $this->data['rowData'] = $result;
		    //
			
			//search bar
			$this->data['jobFieldList'] = $this->loadJobField();
			$this->data['locList'] = $this->loadLocation();
			$this->data['jobCatList'] = $this->loadJobCategory();

		    //Slider
			$slider = \DB::table('acc_slider')->orderBy('createdOn','desc')->limit(5)->get();
		    //Testimonial
			$testimonial = \DB::table('acc_testimonial')->orderBy('createdOn','desc')->limit(6)->get();
			//newskotak	
			$newskotak = \DB::table('tb_pages')->where('pagetype', 'LIKE', '%post%')->orderBy('pageID','desc')->limit(7)->get();
		    
			$sql = \DB::table('tb_pages')->where('default','1')->get();
			if(count($sql)>=1)
			{
				$row = $sql[0];
				$this->data['title'] = $row->title ;
				$this->data['subtitle'] = $row->sinopsis ;
				if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/template/'.$row->filename.'.blade.php') && $row->filename !='')
				{
					$page_template = 'layouts.'.config('sximo.cnf_theme').'.template.'.$row->filename;
				} else {
					$page_template = 'layouts.'.config('sximo.cnf_theme').'.template.page';
				}				
				$this->data['sliders'] = $slider;
				$this->data['testimonials'] = $testimonial;
				$this->data['newskotak'] = $newskotak;
				$this->data['pages'] = $page_template;
				$this->data['pageID'] = $row->pageID ;
				$this->data['content'] = \PostHelpers::formatContent($row->note);
				$this->data['note'] = $row->note;
				$page = 'layouts.'.config('sximo.cnf_theme').'.index';


				return view( $page, $this->data);	

			} else {
				return 'Please Set Default Page';
			}	
		}
	}
	
	public function  getLang( Request $request , $lang='en')
	{
		$request->session()->put('lang', $lang);
		return  Redirect::back();
	}

	public function  getSkin($skin='sximo')
	{
		\Session::put('themes', $skin);
		return  Redirect::back();
	}		

	public  function  postContact( Request $request)
	{
	
		$this->beforeFilter('csrf', array('on'=>'post'));
		$rules = array(
				'name'		=>'required',
				'subject'	=>'required',
				'message'	=>'required|min:20',
				'sender'	=>'required|email'			
		);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) 
		{
			
			$data = array('name'=>$request->input('name'),'sender'=>$request->input('sender'),'subject'=>$request->input('subject'),'notes'=>$request->input('message')); 
			$message = view('emails.contact', $data); 		
			$data['to'] = $this->config['cnf_email'];			
			if($this->config['cnf_mail'] =='swift')
			{ 
				Mail::send('user.emails.contact', $data, function ($message) use ($data) {
		    		$message->to($data['to'])->subject($data['subject']);
		    	});	

			}  else {

				$headers  	= 'MIME-Version: 1.0' . "\r\n";
				$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers 	.= 'From: '.$request->input('name').' <'.$request->input('sender').'>' . "\r\n";
					mail($data['to'],$data['subject'], $message, $headers);		
			}


	

			return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('success','Thank You , Your message has been sent !'));	
				
		} else {
			return Redirect::to($request->input('redirect'))->with('message', \SiteHelpers::alert('error','The following errors occurred'))
			->withErrors($validator)->withInput();
		}		
	}	

	public function submit( Request $request )
	{
		$formID = $request->input('form_builder_id');

		$rows = \DB::table('tb_forms')->where('formID',$formID)->get();
		if(count($rows))
		{
			$row = $rows[0];
			$forms = json_decode($row->configuration,true);
			$content = array();
			$validation = array();
			foreach($forms as $key=>$val)
			{
				$content[$key] = (isset($_POST[$key]) ? $_POST[$key] : ''); 
				if($val['validation'] !='')
				{
					$validation[$key] = $val['validation'];
				}
			}
			
			$validator = Validator::make($request->all(), $validation);	
			if (!$validator->passes()) 
					return redirect()->back()->with(['status'=>'error','message'=>'Please fill required input !'])
							->withErrors($validator)->withInput();

			
			if($row->method =='email')
			{
				// Send To Email
				$data = array(
					'email'		=> $row->email ,
					'content'	=> $content ,
					'subject'	=> "[ " .config('sximo.cnf_appname')." ] New Submited Form ",
					'title'		=> $row->name 			
				);
			
				if( config('sximo.cnf_mail') =='swift' )
				{ 				
					\Mail::send('sximo.form.email', $data, function ( $message ) use ( $data ) {
			    		$message->to($data['email'])->subject($data['subject']);
			    	});		

				}  else {

					$message 	 = view('sximo.form.email', $data);
					$headers  	 = 'MIME-Version: 1.0' . "\r\n";
					$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers 	.= 'From: '. config('sximo.cnf_appname'). ' <'.config('sximo.cnf_email').'>' . "\r\n";
						mail($data['email'], $data['subject'], $message, $headers);	
				}
				
				return redirect()->back()->with(['status'=>'success','message'=> $row->success ]);

			} else {
				// Insert into database 
				\DB::table($row->tablename)->insert($content);
				return redirect()->back()->with(['status'=>'success','message'=>  $row->success  ]);
			
			}
		} else {

			return redirect()->back()->with(['status'=>'error','message'=>'Cant process the form !']);
		}


	}

	public function getLoad()
	{	
		$result = \DB::table('tb_notification')->where('userid',\Session::get('uid'))->where('is_read','0')->orderBy('created','desc')->limit(5)->get();

		$data = array();
		$i = 0;
		foreach($result as $row)
		{
			if(++$i <=10 )
			{
				if($row->postedBy =='' or $row->postedBy == 0)
				{
					//$image = '<img src="'.asset('uploads/images/system.png').'" border="0" width="30" class="img-circle" />';
					$image = '<i class="fa fa-exclamation"></i>';
				} 
				else {
					$image = \SiteHelpers::avatar('30', $row->postedBy);
				}
				$data[] = array(
						'url'	=> $row->url,
						'title'	=> $row->title ,
						'icon'	=> $row->icon,
						'image'	=> $image,
						'text'	=> substr($row->note,0,100),
						'date'	=> date("d/m/y",strtotime($row->created))
					);
			}	
		}
	
		$data = array(
			'total'	=> count($result) ,
			'note'	=> $data
			);	
		 return response()->json($data);	
	}

	public function posts( Request $request , $read = '') 
	{
		$posts = \DB::table('tb_pages')
					->select('tb_pages.*','tb_users.username',\DB::raw('COUNT(commentID) AS comments'))
					->leftJoin('tb_users','tb_users.id','tb_pages.userid')
					->leftJoin('tb_comments','tb_comments.pageID','tb_pages.pageID')					
					->where('pagetype','post');
					if(!is_null($request->input('label'))){
						$keyword = trim($request->input('label'));
						$posts = $posts->where('labels', 'LIKE' , "%{$keyword}%%" ); 	
					}

					if($read !='') {
						$posts = $posts->where('alias',$read )->get(); 
					} 
		else {

			$posts = $posts->groupBy('tb_pages.pageID')->orderBy('tb_pages.pageID','desc')->paginate(6);
		}					

		$this->data['title']		= 'Post Articles';
		$this->data['posts']		= $posts;
		$this->data['pages']		= 'secure.posts.posts';
		base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/index.blade.php';

		if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/index.blade.php'))
		{
			$this->data['pages'] = 'layouts.'.config('sximo.cnf_theme').'.blog.index';
		}	

		if($read !=''){
			if(file_exists(base_path().'/resources/views/layouts/'.config('sximo.cnf_theme').'/blog/view.blade.php'))
			{
				if(count($posts))
				{
					$this->data['posts'] = $posts[0];
					$this->data['comments']	= \DB::table('tb_comments')
												->select('tb_comments.*','username','avatar','email')
												->leftJoin('tb_users','tb_users.id','tb_comments.UserID')
												->where([['PageID',$this->data['posts']->pageID],['comment_status','approved']])
												->get();
					\DB::table('tb_pages')->where('pageID',$this->data['posts']->pageID)->update(array('views'=> \DB::raw('views+1')));						
				} else {
					return redirect('posts');
				}	
				$this->data['title']		= $this->data['posts']->title;
				$this->data['pages'] = 'layouts.'.config('sximo.cnf_theme').'.blog.view';

			}	
		}	
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view( $page , $this->data);	
	}

	public function comment( Request $request)
	{
		$rules = array(
			'comments'	=> 'required',
			'name' => 'required',
			'email' => 'required|email'
		);
        if(config('sximo.cnf_recaptcha') =='true') {
		$token = $request['g-recaptcha-response'];
		if($token){
			$client = new \GuzzleHttp\Client();
			$response = $client->post('https://www.google.com/recaptcha/api/siteverify?secret='.config('sximo.cnf_recaptchaprivatekey').'&response='.$token);
			$results = json_decode($response->getBody()->getContents(),true);
			if($results['success']){
			   \Log::info('Recaptcha success');
			} else {
			   \Log::info('Recaptcha error code');
			    foreach( $results['error-codes'] as $err){
			   \Log::info($err);
			   }
			   return redirect('posts/'.$request->input('alias'))->with(['message'=>'','status'=>'success'])->withErrors('Error Recaptcha')->withInput();;
			}
		} else {
			\Log::info('Invalid ReCaptcha');
			return redirect('posts/'.$request->input('alias'))->with(['message'=>'','status'=>'success'])->withErrors('Invalid Recaptcha')->withInput();
		}
        }
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$data = array(
					'userID'		=> \Session::get('uid'),
					'posted'		=> date('Y-m-d H:i:s') ,
					'comments'		=> $request->input('comments'),
					'pageID'		=> $request->input('pageID'),
					'comment_name' => $request->input('name'),
					'comment_email' => $request->input('email'),
					'comment_status' => 'pending'
				);

			\DB::table('tb_comments')->insert($data);
			return redirect('posts/'.$request->input('alias'))->with(['message'=>'Thank You , your comment has been sent to our administrator','status'=>'success']);
		} else {
			return redirect('posts/'.$request->input('alias'))->with(['message'=>'The following errors occurred','status'=>'success'])
            ->withErrors($validator)->withInput();
		}
	}

	public function remove( Request $request, $pageID , $alias , $commentID )
	{
		if($commentID !='')
		{
			\DB::table('tb_comments')->where('commentID',$commentID)->delete();
			return redirect('view-comments/'.$pageID)->with('message',__('core.note_success'))->with('status','success');
       
		} else {
			return redirect('view-comments/'.$pageID)->with('message',__('core.note_error'))->with('status','error');	
		}
	}

	public function set_theme( $id ){
		session(['set_theme'=> $id ]);
		return response()->json(['status'=>'success']);
	}	
	
//added for search bar
	public function loadJobField() {
		$jobFieldList = \DB::table('acc_job')->leftJoin('acc_jobfield','acc_job.IDJobField','=','acc_jobfield.id')
						->where([['acc_job.StatusJob','active'],['acc_jobfield.status_jobfield','active']])
						->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
						->select('acc_jobfield.*')->groupBy('acc_job.IDJobField')->get();
		return $jobFieldList;
	}
	
	public function loadLocation() {
		$locList = \DB::table('acc_placement')->select('id','name')->get();
		return $locList;
	}
	
	public function loadJobCategory() {
		$jobCat = \DB::table('acc_job')->distinct()->get(['JobCategory']);
		return $jobCat;
	}
	
	public function reCaptcha( $request) {
        if(!is_null($request['g-recaptcha-response']))
        {
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . config('sximo.cnf_recaptchaprivatekey') . '&response='.$request['g-recaptcha-response'];
            $response = @file_get_contents($api_url);
            $data = json_decode($response, true);
            
            return $data;
        }
        else
        {
            return false ;
        }
    }

}
