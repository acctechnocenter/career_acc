<?php namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Validator, Redirect ; 
use App\User;


class JobController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'job';
	static $per_page	= '10';

	public function __construct()
	{		
		parent::__construct();
		$this->model = new Job();	
		
		$this->info = $this->model->makeInfo( $this->module);	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'job',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function index( Request $request )
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$this->grab( $request) ;
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');				
		// Render into template
		return view( $this->module.'.index',$this->data);
	}	

	function create( Request $request , $id =0 ) 
	{
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		
		$this->data['id'] = '';
		return view($this->module.'.form',$this->data);
	}
	function edit( Request $request , $id ) 
	{
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');
		$this->data['row'] = (array) $this->data['row'];
		
		$this->data['id'] = $id;
		return view($this->module.'.form',$this->data);
	}	
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		$task =$id ;
		switch( $task)
		{
			case 'search':
				return $this->getSearch();
				break;
			case 'lookup':
				return $this->getLookup($request );
				break;
			case 'comboselect':
				return $this->getComboselect( $request );
				break;
			case 'import':
				return $this->getImport( $request );
				break;
			case 'export':
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				return view($this->module.'.view',$this->data);	
				break;		
		}
	}
	function store( Request $request  )
	{
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if ($validator->passes()) 
				{
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					
					/* Insert logs */
					$this->model->logs($request , $id);
					if(!is_null($request->input('apply')))
						return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');

					return redirect( $this->module .'?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
				} 
				else {
					return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
							->with('message',__('core.note_error'))->with('status','error')
							->withErrors($validator)->withInput();

				}
				break;
			case 'public':
				return $this->store_public( $request );
				break;

			case 'delete':
				$result = $this->destroy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;

			case 'import':
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;		
		}	
	
	}	

	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}	
	
	public static function display(  )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Job();
		$info = $model::makeInfo('job');
		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']			
		);	
		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('job.public.view',$data);			
			}			
		} 
		else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> $info['key'] ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('job.public.index',$data);	
		}

	}
	function store_public( $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost(  $request );		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('message',__('core.note_success'))->with('status','success');
		} else {

			return  Redirect::back()->with('message',__('core.note_error'))->with('status','error')
			->withErrors($validator)->withInput();

		}	
	
	}
	
	//added by akos
	public function getJobList(Request $request){
		$result = \DB::table('acc_job')
							->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
							->leftjoin('acc_jobfield', 'acc_job.IDJobField', '=', 'acc_jobfield.id')
							->select('acc_job.*', 'acc_placement.name as placement', 'acc_jobfield.img_jobfield as img_jobfield')
							->where('acc_job.StatusJob','active')->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))
							->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))->orderBy('acc_job.id','desc')->paginate(10);
		$this->data['rowData'] = $result;
		$this->data['cat'] = '0';
		$this->data['pages'] = 'job.joblist';
		$this->data['title'] = 'Job List';
		$this->data['jobFieldList'] = $this->loadJobField();
		$this->data['locList'] = $this->loadLocation();
		$this->data['jobCatList'] = $this->loadJobCategory();
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view($page , $this->data);
	}
	
	public function getJobListParam($category){
		
		$result = \DB::table('acc_job')
		->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
		->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
		->where([['acc_jobfield.id',$category],['acc_job.StatusJob', 'active']])->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))
		->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
		->select('acc_job.*','acc_jobfield.id as idCategory','acc_jobfield.jobfield','acc_jobfield.img_jobfield as img_jobfield','acc_placement.name as placement')
		->orderBy('acc_job.id','desc')
		->paginate(10);
		$cat_title = \DB::table('acc_jobfield')->where('id', $category)->value('jobfield');
		$this->data['rowData'] = $result;
		$this->data['cat'] = $cat_title;
		$this->data['pages'] = 'job.joblist';
		$this->data['title'] = 'Job List';
		$this->data['jobFieldList'] = $this->loadJobField();
		$this->data['locList'] = $this->loadLocation();
		$this->data['jobCatList'] = $this->loadJobCategory();
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view($page , $this->data);
	}
	
	public function getJobListCat($cat){
		$catString = ucwords(str_replace("-"," ",$cat));
		$result = \DB::table('acc_job')
		->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
		->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
		->where([['acc_job.JobCategory',$catString],['acc_job.StatusJob','active']])->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))
		->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
		->select('acc_job.*','acc_jobfield.id as idCategory','acc_jobfield.jobfield','acc_jobfield.img_jobfield as img_jobfield','acc_placement.name as placement')
		->orderBy('acc_job.id','desc')
		->paginate(10);
		$this->data['rowData'] = $result;
		$this->data['cat'] = '0';
		$this->data['pages'] = 'job.joblist';
		$this->data['title'] = 'Job List';
		$this->data['jobFieldList'] = $this->loadJobField();
		$this->data['locList'] = $this->loadLocation();
		$this->data['jobCatList'] = $this->loadJobCategory();
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view($page , $this->data);
	}
	
	public function getJobListCatSub($cat,$sub){
		$catString = ucwords(str_replace("-"," ",$cat));
		if($sub == "ldp-program"){
			$subString = "LDP Program";
		} else {
			$subString = ucwords(str_replace("-"," ",$sub));
		}
		$result = \DB::table('acc_job')
		 ->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
		->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
		->where([['acc_job.JobSubCategory',$subString],['acc_job.StatusJob','active']])->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))
		->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
		->select('acc_job.*','acc_jobfield.id as idCategory','acc_jobfield.jobfield','acc_jobfield.img_jobfield as img_jobfield','acc_placement.name as placement')
		->orderBy('acc_job.id','desc')
		->paginate(10);
		$this->data['rowData'] = $result;
		$this->data['cat'] = '0';
		$this->data['pages'] = 'job.joblist';
		$this->data['title'] = 'Job List';
		$this->data['jobFieldList'] = $this->loadJobField();
		$this->data['locList'] = $this->loadLocation();
		$this->data['jobCatList'] = $this->loadJobCategory();
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view($page , $this->data);
	}
	
	public function getJobDetail($idcat, $id){
		$result = \DB::table('acc_job')->orderBy('acc_job.id','desc')
						->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
						->leftjoin('acc_jobfield', 'acc_job.IDJobField' ,'=', 'acc_jobfield.id')
						->select('acc_job.*','acc_placement.name as placement','acc_jobfield.img_jobfield as img_jobfield')
						->where([['acc_job.id', $id],['acc_job.StatusJob','active']])->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))
						->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))->first();
		$category = \DB::table('acc_jobfield')->where('id', $idcat)->value('jobfield');
		
		if(\Auth::check()){
				$userLoggedin =	\Auth::user()->id;
				$infoCandidate = \DB::table('acc_candidate_profile')->where('id_user',$userLoggedin)->first();
				if($infoCandidate){
					if($infoCandidate->Careerpreference){
						$idPref = array_map('intval', explode(',',$infoCandidate->Careerpreference));
						$pref_job = \DB::table('acc_job')
											->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
											->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
											->where([['acc_job.id','<>', $id],['acc_job.StatusJob','active']])->whereIn('acc_job.IDJobField',$idPref)
											->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
											->select('acc_job.id','acc_job.IDJobField','acc_job.JobTitleName','acc_job.JobCategory','acc_placement.name','acc_jobfield.img_jobfield','acc_job.StartDate','acc_job.EndDate')->orderBy('acc_job.id','desc')->limit(3)->get();
					} else{
						$pref_job = \DB::table('acc_job')
							->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
							->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
							->where([['acc_job.IDJobField', $idcat], ['acc_job.id','<>', $id],['acc_job.StatusJob','active']])
							->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
							->select('acc_job.id','acc_job.IDJobField','acc_job.JobTitleName','acc_job.JobCategory','acc_placement.name','acc_jobfield.img_jobfield','acc_job.StartDate','acc_job.EndDate')->orderBy('acc_job.id','desc')->limit(3)->get();
					}
				} else{
						$pref_job = \DB::table('acc_job')
							->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
							->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
							->where([['acc_job.IDJobField', $idcat], ['acc_job.id','<>', $id],['acc_job.StatusJob','active']])
							->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
							->select('acc_job.id','acc_job.IDJobField','acc_job.JobTitleName','acc_job.JobCategory','acc_placement.name','acc_jobfield.img_jobfield','acc_job.StartDate','acc_job.EndDate')->orderBy('acc_job.id','desc')->limit(3)->get();
				}
		} else {
		 $pref_job = \DB::table('acc_job')
		->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
		->leftjoin('acc_jobfield', 'acc_job.IDJobField','=','acc_jobfield.id')
		->where([['acc_job.IDJobField', $idcat], ['acc_job.id','<>', $id],['acc_job.StatusJob','active']])
		->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
		->select('acc_job.id','acc_job.IDJobField','acc_job.JobTitleName','acc_job.JobCategory','acc_placement.name','acc_jobfield.img_jobfield','acc_job.StartDate','acc_job.EndDate')->orderBy('acc_job.id','desc')->limit(3)->get();
		}
		
		$this->data['checkUser'] = $this->checkUserPre($id);
		$this->data['dataNull'] = $this->dataNull();
		
		$this->data['rowData'] = $result;
		$this->data['pref'] = $pref_job;
		$this->data['cat'] = $category;
		$this->data['pages'] = 'job.jobdetail';
		$this->data['title'] = 'Job Detail';
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		\DB::table('acc_job')->where('id',$id)->update(array('ViewJob'=> \DB::raw('ViewJob+1')));		
		
		return view($page , $this->data);
	}
	
	public function jobSearch(Request $request){
		$q = Input::get('q');
		$jf = Input::get('jf');
		$jc = Input::get('jc');
		$loc = Input::get('loc');
		$query = \DB::table('acc_job')
						->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
						->leftjoin('acc_jobfield', 'acc_job.IDJobField' ,'=', 'acc_jobfield.id')
						->where([['JobTitleName', 'LIKE', '%'.$q. '%'],['StatusJob','active']])
						->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
						->select('acc_job.*','acc_placement.name as placement','acc_jobfield.img_jobfield')
		->when($loc, function ($locQuery) use ($loc) {
			return $locQuery->where('Location', $loc);
		})
		->when($jc, function ($jcQuery) use ($jc) {
			return $jcQuery->where('JobCategory', $jc);
		})
		->when($jf, function ($jfQuery) use ($jf) {
			return $jfQuery->where('IDJobField', $jf);
		})
		->paginate(10);
		if($q){
			$query->appends(['q'=> $q]);
			$this->data['q'] = $q;
		} else { $this->data['q'] = '0'; }
		if($jf){
			$category = \DB::table('acc_jobfield')->where('id', $jf)->value('jobfield');
			$query->appends(['jf'=> $jf]);
			$this->data['jf'] = $category;
		} else { $this->data['jf'] = '0'; }
		if($jc){
			$query->appends(['jc'=> $jc]);
			$this->data['jc'] = $jc;
		} else { $this->data['jc'] = '0'; }
		if($loc){
			$placement = \DB::table('acc_placement')->where('id', $loc)->value('name');
			$query->appends(['loc'=> $loc]);
			$this->data['loc'] = $placement;
		} else { $this->data['loc'] = '0'; }
		$this->data['rowData'] = $query;
		$this->data['pages'] = 'job.jobresult';
		$this->data['title'] = 'Result Job';
		$this->data['jobFieldList'] = $this->loadJobField();
		$this->data['locList'] = $this->loadLocation();
		$this->data['jobCatList'] = $this->loadJobCategory();
		$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		return view($page, $this->data);
	}
	
	public function loadJobField() {
		$jobFieldList = \DB::table('acc_job')->leftJoin('acc_jobfield','acc_job.IDJobField','=','acc_jobfield.id')
						->where([['acc_job.StatusJob','active'],['acc_jobfield.status_jobfield','active']])
						->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
						->select('acc_jobfield.*')->groupBy('acc_job.IDJobField')->get();
		return $jobFieldList;
	}
	
	public function loadLocation() {
		$locList = \DB::table('acc_placement')->select('id','name')->get();
		return $locList;
	}
	
	public function loadJobCategory() {
		$jobCat = \DB::table('acc_job')->distinct()->get(['JobCategory']);
		return $jobCat;
	}
	
	public function checkUserPre($idjob){
			$check = true;
			if(\Auth::check()){
				$userLoggedin =	User::find(\Auth::user()->id);
				$statusPre = ['pending','accepted'];
				$findUser = \DB::table('acc_prescreening_status')->where('id_user','=',$userLoggedin->id)->whereIn('status_prescreening',$statusPre)->get();
				$alreadyPre = \DB::table('acc_prescreening_status')->where([['id_user','=', $userLoggedin->id],['id_job','=', $idjob],])->get();
				
				if(count($findUser)>0){
					$check= false;
					} else {
						$check = true;
						if(count($alreadyPre)>0){
							$check= false;
							} else {
								$check= true;
							}
						}
		}
		
		return $check;
	}
	
	public function dataNull(){
			$check = true;
			if(\Auth::check()){
				$userLoggedin =	User::find(\Auth::user()->id);
				$resProfile = \DB::table('acc_candidate_profile')->where('id_user', $userLoggedin->id)->first();
				$eduBack = \DB::table('acc_educational_background')->where('id_user', $userLoggedin->id)->first();
				if($resProfile && $eduBack){
					if($resProfile->dateofbirth){
							$check = true;
					} else if($eduBack->universityorschool){
						$check = true;
						} else{
							$check = false;
						}
				} else {
					$check = false;
				}
			}
			return $check;
	}
	
}
