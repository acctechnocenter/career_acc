<?php namespace App\Http\Controllers;

use App\Models\Prescreeningstatus;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Carbon\Carbon;

class PrescreeningstatusController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'prescreeningstatus';
	static $per_page	= '10';

	public function __construct()
	{		
		parent::__construct();
		$this->model = new Prescreeningstatus();	
		
		$this->info = $this->model->makeInfo( $this->module);	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'prescreeningstatus',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function index( Request $request )
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$this->grab( $request) ;
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');				
		// Render into template
		return view( $this->module.'.index',$this->data);
	}	

	function create( Request $request , $id =0 ) 
	{
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		
		$this->data['id'] = '';
		return view($this->module.'.form',$this->data);
	}
	function edit( Request $request , $id ) 
	{
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');
		$this->data['row'] = (array) $this->data['row'];
		
		$this->data['id'] = $id;
		return view($this->module.'.form',$this->data);
	}	
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		$task =$id ;
		switch( $task)
		{
			case 'search':
				return $this->getSearch();
				break;
			case 'lookup':
				return $this->getLookup($request );
				break;
			case 'comboselect':
				return $this->getComboselect( $request );
				break;
			case 'import':
				return $this->getImport( $request );
				break;
			case 'export':
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				return view($this->module.'.view',$this->data);	
				break;		
		}
	}
	function store( Request $request  )
	{
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if ($validator->passes()) 
				{
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					
					/* Insert logs */
					$this->model->logs($request , $id);
					if(!is_null($request->input('apply')))
						return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');

					return redirect( $this->module .'?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
				} 
				else {
					return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
							->with('message',__('core.note_error'))->with('status','error')
							->withErrors($validator)->withInput();

				}
				break;
			case 'public':
				return $this->store_public( $request );
				break;

			case 'delete':
				$result = $this->destroy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;

			case 'import':
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;		
		}	
	
	}	

	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}	
	
	public static function display(  )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Prescreeningstatus();
		$info = $model::makeInfo('prescreeningstatus');
		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']			
		);	
		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('prescreeningstatus.public.view',$data);			
			}			
		} 
		else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> $info['key'] ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('prescreeningstatus.public.index',$data);	
		}

	}
	function store_public( $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost(  $request );		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('message',__('core.note_success'))->with('status','success');
		} else {

			return  Redirect::back()->with('message',__('core.note_error'))->with('status','error')
			->withErrors($validator)->withInput();

		}	
	
	}

	public function getListApplied($statusRes, $idjob){
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		
			$this->data = array(
					'pageTitle'	=> 	'Prescreening Result',
					'pageModule'=> 'prescreeningresult',
					'moduleJob' => 'job',
					'return'	=> self::returnUrl()
					
			);
			
			$jobDetail = \DB::table('acc_job')->select('id','JobTitleName')->where('id', $idjob)->first();
			
			
			$queryResult = \DB::table('acc_prescreening_status')
			->leftJoin('acc_job', 'acc_prescreening_status.id_job','=','acc_job.id')
			->leftJoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
			->select('acc_prescreening_status.id as id_status','tb_users.first_name as first_name','tb_users.last_name as last_name','acc_job.id as id_job','acc_prescreening_status.id_user as applicants','acc_prescreening_status.skor_akhir as skor','acc_prescreening_status.status_prescreening as status','acc_prescreening_status.apply_date')
			->where([
				['acc_job.id',$idjob],
				['acc_prescreening_status.status_prescreening','=', $statusRes],
				])
			->orderBy('skor','desc')
			->paginate(100000);
			
			$sort = 'skor';
			$order = 'desc';
			$this->data['rowData'] = $queryResult;
			$this->data['jobDetail'] = $jobDetail;
			$this->data['statusRes'] = $statusRes;
			//$this->data['insort'] = $sort;
			//this->data['inorder']	= $order;

			return view( $this->module.'.list-applied',$this->data);		
	}

	public function filterSearch($statusRes, $idjob, Request $request){
		$applicant= $request->input('applicant');
		
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		
			$this->data = array(
					'pageTitle'	=> 	'Prescreening Result',
					'pageModule'=> 'prescreeningresult',
					'moduleJob' => 'job',
					'return'	=> self::returnUrl()
					
			);
			
			$jobDetail = \DB::table('acc_job')->select('id','JobTitleName')->where('id', $idjob)->first();
			
			
			$queryResult = \DB::table('acc_prescreening_status')
			->leftJoin('acc_job', 'acc_prescreening_status.id_job','=','acc_job.id')
			->leftJoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
			->select('acc_prescreening_status.id as id_status','tb_users.first_name as first_name','tb_users.last_name as last_name',\DB::raw('CONCAT(tb_users.first_name, " ", tb_users.last_name) as firstlast') , 'acc_job.id as id_job','acc_prescreening_status.id_user as applicants','acc_prescreening_status.skor_akhir as skor','acc_prescreening_status.status_prescreening as status','acc_prescreening_status.apply_date')
			->where([
				['acc_job.id',$idjob],
				['acc_prescreening_status.status_prescreening','=', $statusRes],
				])
			->when($applicant, function ($appQuery) use ($applicant) {
					return $appQuery->where(\DB::raw('CONCAT(tb_users.first_name, " ", tb_users.last_name)'), 'like', '%'.$applicant.'%');
				})
			->orderBy('skor','desc')
			->paginate(20);
			
			$sort = 'skor';
			$order = 'desc';
			$this->data['rowData'] = $queryResult;
			$this->data['jobDetail'] = $jobDetail;
			$this->data['statusRes'] = $statusRes;
			//$this->data['insort'] = $sort;
			//this->data['inorder']	= $order;

			return view( $this->module.'.list-applied',$this->data);	
	}
	
	public function getEditApplied($statusRes, $idstatus){
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		
		$queryResult = \DB::table('acc_prescreening_status')
		->leftJoin('acc_job', 'acc_prescreening_status.id_job','=','acc_job.id')
		->leftJoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
		->select('acc_prescreening_status.id as id_pre_status','acc_prescreening_status.id_job','acc_prescreening_status.id_user','acc_prescreening_status.skor_akhir',
			'acc_prescreening_status.status_prescreening','acc_job.JobTitleName as job_title','tb_users.first_name','tb_users.last_name')
		->where('acc_prescreening_status.id',$idstatus)->first();
		
		$this->data['rowData'] = $queryResult;
		$this->data['id'] = $idstatus;
		$this->data['statusRes'] = $statusRes;
		$this->data['pageTitle'] = 'Prescreening Result';
		$this->data['pageNote'] = 'Note testing';
		$this->data['pageModule'] = $this->module;
		return view( $this->module.'.edit-applied',$this->data );
	}
	
	public function getPrescreeningHist(){
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$id_user = \Auth::user()->id;
		$resultPreHist = \DB::table('acc_prescreening_status')->leftJoin('acc_job','acc_prescreening_status.id_job','=','acc_job.id')
								->select('acc_prescreening_status.*','acc_job.JobTitleName')->where('id_user',$id_user)
								->orderBy('apply_date','desc')->paginate(20);
		$statProfile = \DB::table('acc_candidate_profile')->where('id_user',$id_user)->value('ApplicantStatus');
		
		$this->data['rowData'] = $resultPreHist;
		$this->data['statProfile'] = $statProfile;
		$this->data['pageTitle'] = 'History Prescreening Result';
		$this->data['pageNote'] = 'Note testing';
		$this->data['pageModule'] = $this->module;
		return view( $this->module.'.history-pre',$this->data );
	}
	
	public function saveListApplied(Request $request ){
		$id = $request->input('id');
		$status = $request->input('type');
		$id_job = $request->input('id_job');
		$id_user = $request->input('id_user');
		$statusRes = $request->input('statusRes');
		
		try{
			\DB::table('acc_prescreening_status')->where('id',$id)->update(['status_prescreening' => $status]);
			
			if(strtolower($status) == "accepted"){
				
				try{
					$arrUser[] = $id_user;
					$arrSelection[] = $id;
					$client = new \GuzzleHttp\Client();
					$this->schHitMobileApplicant($arrUser, $client);
					$this->schHitMobileEduBack($arrUser, $client);
					$this->schHitMobileWorkExp($arrUser, $client);
					$this->schHitMobileOrgExp($arrUser, $client);
					$this->schHitMobileAppSelection($arrSelection,$client);
					$requestJson = $client->post(\Lang::get('acc.loginsf'));
					$responseJson = $requestJson->getBody()->getContents();
					$resultJson = json_decode($responseJson,true);
					if($requestJson->getStatusCode() == "200"){
						$token = $resultJson['access_token'];
						
						/* -- Send Applicant Profile--*/
						$idExt = $this->hitApplicantProfile($id_user, $client, $token);
						try{
							\DB::table('acc_candidate_profile')->where('id_user',$id_user)->update(['idExternal' => $idExt]);
						} catch(\Illuminate\Database\QueryException $ex){
								\Log::error($ex);
								return redirect('list-prescreening-result/'.$statusRes.'/'.$id_job)->with('message',__('core.note_error'))->with('status','error');
						}
						
						/*-- Send Applicant Selection --*/
						$idExtAppSel = $this->hitAppSelection($id, $client, $token);
						try{
							\DB::table('acc_prescreening_status')->where('id',$id)->update(['idExternal' => $idExtAppSel]);
						} catch(\Illuminate\Database\QueryException $ex){
								\Log::error($ex);
								return redirect('list-prescreening-result/'.$statusRes.'/'.$id_job)->with('message',__('core.note_error'))->with('status','error');
						}
						
						/*-- Send Organizational Experience --*/
						$this->hitOrgExp($id_user, $client, $token);
						
						/*-- Send Educational Background --*/
						$this->hitEduBack($id_user, $client, $token);
						
						/*-- Send Working Experience --*/
						$this->hitWorkExp($id_user, $client, $token);
						
						/*-- Send File Attachment --*/
						$this->hitFileAttach($id_user, $client, $token);
					}	
				} catch(Exception $e)	{
					\Log::error($e);
					return redirect('list-prescreening-result/'.$statusRes.'/'.$id_job)->with('message',__('core.note_error'))->with('status','error');		
				} 
			}
			
			return redirect('list-prescreening-result/'.$statusRes.'/'.$id_job)->with('message',__('core.note_success'))->with('status','success');
		} catch (\Illuminate\Database\QueryException $ex){
			\Log::error($ex);
			return redirect('list-prescreening-result/'.$statusRes.'/'.$id_job)->with('message',__('core.note_error'))->with('status','error');
		}
	}
	
	public function hitApplicantProfile($id_user, $client, $token){
		
		$post_data = array();
		$idExt = '';
		$resultQuery = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')->where('tb_users.id',$id_user)
			->select('tb_users.id','acc_candidate_profile.address','acc_candidate_profile.cityofbirth','acc_candidate_profile.cityorregency','acc_candidate_profile.country',
			'acc_candidate_profile.dateofbirth','acc_candidate_profile.subdisctrict','tb_users.email','tb_users.first_name','acc_candidate_profile.gender',
			'acc_candidate_profile.homephone','tb_users.last_name','tb_users.avatar','acc_candidate_profile.maritalstatus','acc_candidate_profile.mobilephone',
			'acc_candidate_profile.identitycardnumber','acc_candidate_profile.provinceofbirth','acc_candidate_profile.province','acc_candidate_profile.religion',
			'acc_candidate_profile.title_profile','acc_candidate_profile.postalcode','acc_candidate_profile.npwp','acc_candidate_profile.marrieddate')->first();
			
			$data = array();
			if(!empty($resultQuery->id)){ $data['WebCareer_ID__c'] = $resultQuery->id; }
			if(!empty($resultQuery->address)){ $data['Address__c'] = $resultQuery->address; }		
			if(!empty($resultQuery->cityofbirth)){ $data['City_of_Birth__c'] = $resultQuery->cityofbirth; }
			if(!empty($resultQuery->cityorregency)){ $data['City__c'] = $resultQuery->cityorregency; }
			if(!empty($resultQuery->country)){ $data['Country__c'] = $resultQuery->country; }
			if(!empty($resultQuery->dateofbirth)){ $data['Date_of_Birth__c'] = $resultQuery->dateofbirth; }
			if(!empty($resultQuery->subdisctrict)){ $data['District__c'] = $resultQuery->subdisctrict; }
			if(!empty($resultQuery->email)){ $data['Email_Address__c'] = $resultQuery->email; }
			if(!empty($resultQuery->first_name)){ $data['First_Name__c'] = $resultQuery->first_name; }
			if(!empty($resultQuery->gender)){ $data['Gender__c'] = $resultQuery->gender; }
			if(!empty($resultQuery->homephone)){ $data['Home_Phone__c'] = $resultQuery->homephone; }
			if(!empty($resultQuery->last_name)){ $data['Last_Name__c'] = $resultQuery->last_name; }
			$url_foto = url('uploads/users');
			if(!empty($resultQuery->avatar)){ $data['Link_Photo__c'] = $url_foto.'/'.$resultQuery->avatar; }
			if(!empty($resultQuery->npwp)){ $data['NPWP__c'] = $resultQuery->npwp; }
			if(!empty($resultQuery->maritalstatus)){ $data['Marital_Status__c'] = $resultQuery->maritalstatus; }
			if(!empty($resultQuery->marrieddate)){ 
			if($resultQuery->marrieddate != "0000-00-00"){$data['Married_Date__c'] = $resultQuery->marrieddate; }}
			if(!empty($resultQuery->mobilephone)){ $data['Mobile_Phone__c'] = $resultQuery->mobilephone; }
			if(!empty($resultQuery->first_name) &&  !empty($resultQuery->last_name)){ $data['Name'] = $resultQuery->first_name.' '.$resultQuery->last_name;  }
			if(!empty($resultQuery->identitycardnumber)){ $data['National_Id__c'] = $resultQuery->identitycardnumber; }
			if(!empty($resultQuery->provinceofbirth)){ $data['Province_of_Birth__c'] = $resultQuery->provinceofbirth; }
			if(!empty($resultQuery->province)){ $data['Province__c'] = $resultQuery->province; }
			if(!empty($resultQuery->religion)){ $data['Religion__c'] = $resultQuery->religion; }
			if(!empty($resultQuery->title_profile)){ $data['Title__c'] = $resultQuery->title_profile; }
			if(!empty($resultQuery->postalcode)){ $data['Zip_Code__c'] = $resultQuery->postalcode; }
			
			$post_data = array('items' => array($data));
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }
			
			$headers = [
							'Authorization' => 'Bearer ' . $token,        
							'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postapplicant'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$idExt = $resultJson2['results'][0]['Id'];
			}
		
			return $idExt;
			} catch(Exception $e){
					\Log::error($e);
					return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');	
			}
	}
	
	public function hitAppSelection($id, $client, $token){
		$post_data = array();
		$idExt = '';
		
		$resultQuery = \DB::table('acc_prescreening_status')->leftjoin('acc_job','acc_prescreening_status.id_job','=','acc_job.id')
							->leftjoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
							->leftjoin('acc_candidate_profile','acc_prescreening_status.id_user','=','acc_candidate_profile.id_user')
							->select('acc_prescreening_status.id','acc_prescreening_status.id_job','acc_prescreening_status.id_user','acc_job.idExternal as jobExt',
							'acc_job.JobTitleName', 'acc_prescreening_status.skor_akhir','acc_prescreening_status.status_prescreening',
							'acc_prescreening_status.apply_date', 'tb_users.first_name','tb_users.last_name', 'acc_candidate_profile.idExternal as candidateExt')
							->where('acc_prescreening_status.id',$id)->first();
							
		$data = array();
		if(!empty($resultQuery->id)){ $data['WebCareer_ID__c'] = $resultQuery->id; }
		if(!empty($resultQuery->candidateExt)){ $data['Applicant_Id__c'] = $resultQuery->candidateExt; }
		if(!empty($resultQuery->apply_date)){ 
			$s = $resultQuery->apply_date;
			$dt = new \DateTime($s);
			$date = $dt->format('Y-m-d');
			$time = $dt->format('H:i:s');
			$formatDate = $date.'T'.$time;
			$data['Apply_Date__c'] = $formatDate; 
		}
		if(!empty($resultQuery->jobExt)){ $data['Job_Requisition_Id__c'] = $resultQuery->jobExt; }
		if(!empty($resultQuery->JobTitleName) && !empty($resultQuery->first_name)){ 
			if(!empty($resultQuery->last_name)){
				$data['Name'] = $resultQuery->first_name.' '.$resultQuery->last_name.' - '.$resultQuery->JobTitleName; 
			} else {
				$data['Name'] = $resultQuery->first_name.' - '.$resultQuery->JobTitleName; 
			}
		}
		if(!empty($resultQuery->skor_akhir)){ $data['Score__c'] = $resultQuery->skor_akhir; }
		if($resultQuery->status_prescreening == "accepted"){ $data['Status__c'] = "New Candidate"; }

		$post_data = array('items' => array($data));
		
		if($post_data){
			\Log::info(print_r($post_data,true)); }
			
		$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
		try{				
		$requestJson2 = $client->post(\Lang::get('acc.postapplicantselection'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$idExt = $resultJson2['results'][0]['Id'];
			}
			
			return $idExt;
		} catch(Exception $e){
					\Log::error($e);
					return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');				
		}
	}
	
	public function hitOrgExp($id_user, $client, $token){
		$post_data = array();
		$idExt = [];
		
		$resultQuery = \DB::table('acc_organizational_experience')
							->leftjoin('acc_candidate_profile','acc_organizational_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_organizational_experience.id_org_exp','acc_candidate_profile.idExternal',
							'acc_organizational_experience.organization_experience_startdate','acc_organizational_experience.organizationname',
							'acc_organizational_experience.roleorposition','acc_organizational_experience.rolefunction',
							'acc_organizational_experience.organizationscope','acc_organizational_experience.organization_experience_endate')
							->where('acc_organizational_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_org_exp)){ $data['WebCareer_ID__c'] = $items->id_org_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->organization_experience_startdate)){ $data['From_Year__c'] = $items->organization_experience_startdate;}
				if(!empty($items->organizationname)){ $data['Name'] = $items->organizationname; }
				if(!empty($items->roleorposition)){ $data['Position__c'] = $items->roleorposition; } else { $data['Position__c'] = ""; }
				if(!empty($items->rolefunction)){ $data['Role_Function__c'] = $items->rolefunction; } else { $data['Role_Function__c'] = ""; }
				if(!empty($items->organizationscope)){ $data['Scope__c'] = $items->organizationscope; } else { $data['Scope__c'] = ""; }
				if(!empty($items->organization_experience_endate)){ $data['To_Year__c'] = $items->organization_experience_endate;}
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postorgexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_organizational_experience')->where('id_org_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
					}
				}
			}
			} catch(Exception $e){
				    \Log::error($e);
					return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
			}			
		}
		
		return true;
	}	
	
	public function hitEduBack($id_user, $client, $token){
		$post_data = array();
		$idExt = [];
		
		$resultQuery = \DB::table('acc_educational_background')
							->leftjoin('acc_candidate_profile','acc_educational_background.id_user','=','acc_candidate_profile.id_user')
							->leftjoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
							->select('acc_educational_background.id_edu_back','acc_candidate_profile.idExternal','acc_educational_background.lasteducation',
							'acc_educational_background.startdate', 'acc_educational_background.endate','acc_universityorschool.id as idUni', 'acc_educational_background.gpa','acc_educational_background.major','acc_educational_background.faculty',
							'acc_educational_background.otherunivorschool','acc_universityorschool.name')->where('acc_educational_background.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_edu_back)){ $data['WebCareer_ID__c'] = $items->id_edu_back; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->lasteducation)){ 
					switch($items->lasteducation){
						case "SMU": $data['Degree__c'] = "Senior High School (SMU)"; break;
						case "D1": $data['Degree__c'] = "Diploma 1 (D1)"; break;
						case "D2": $data['Degree__c'] = "Diploma 2 (D2)"; break;
						case "D3": $data['Degree__c'] = "Asociate Degree 3 (D3)"; break;
						case "D4": $data['Degree__c'] = "Diploma 4 (D4)"; break;
						case "S1": $data['Degree__c'] = "Bachelor Degree (S1)"; break;
						case "S2": $data['Degree__c'] = "Master Degree (S2)"; break;
						case "S3": $data['Degree__c'] = "Doctor (S3)"; break;
						default: $data['Degree__c'] = "";
					}
				}
				if(!empty($items->gpa)){ $data['GPA__c'] = $items->gpa;}
				if(!empty($items->major)){ $data['Major__c'] = $items->major;}
				if(!empty($items->faculty)){ $data['Faculty__c'] = $items->faculty;}
				if(!empty($items->startdate)){ $data['From_Year__c'] = $items->startdate;}
				if(!empty($items->endate)){ $data['To_Year__c'] = $items->endate;}
				if(!empty($items->idUni)){
					$data['University_Code__c'] = $items->idUni;
					if(!empty($items->name)){
						$data['University__c'] = $items->name;
					}
				}
				if(!empty($items->otherunivorschool)){ $data['Others__c'] = $items->otherunivorschool;} else {  $data['Others__c'] = ""; }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.posteduback'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_educational_background')->where('id_edu_back',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
					}
				}
			}
			} catch(Exception $e){
				\Log::error($e);
				return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
			}
		}
		return true;
	}	
	
	public function hitWorkExp($id_user, $client, $token){
		$post_data = array();
		$idExt = [];
		
		$resultQuery = \DB::table('acc_working_experience')
							->leftjoin('acc_candidate_profile','acc_working_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_working_experience.id_wor_exp','acc_candidate_profile.idExternal','acc_working_experience.company',
							'acc_working_experience.jobdescription', 'acc_working_experience.status_working_experience',
							'acc_working_experience.workingexperienceperiodenddate', 'acc_working_experience.workingexperienceperiodstartdate',
							'acc_working_experience.position','acc_working_experience.salary')
							->where('acc_working_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_wor_exp)){ $data['WebCareer_ID__c'] = $items->id_wor_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->company)){ $data['Company__c'] = $items->company; }
				if(!empty($items->jobdescription)){ $data['Job_Description__c'] = $items->jobdescription; } else { $data['Job_Description__c'] = ""; }
				if(!empty($items->status_working_experience)){ $data['Job_Status__c'] = $items->status_working_experience; } else { $data['Job_Status__c'] = ""; }
				if(!empty($items->workingexperienceperiodstartdate)){ $data['Period_Start__c'] = $items->workingexperienceperiodstartdate; }
				if(!empty($items->workingexperienceperiodenddate)){ $data['Period_End__c'] = $items->workingexperienceperiodenddate; }
				if(!empty($items->position)){ $data['Position__c'] = $items->position; } else { $data['Position__c'] = ""; }
				if(!empty($items->salary)){ $data['Salary__c'] = $items->salary; } else { $data['Salary__c'] = ""; }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postworkexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_working_experience')->where('id_wor_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
			}
		}
		return true;
	}	
	
	public function hitFileAttach($id_user, $client, $token){
		$post_data = array();
		$idExt = [];
		
		$resultQuery = \DB::table('acc_add_file_attachment')
							->leftjoin('acc_candidate_profile','acc_add_file_attachment.id_user','=','acc_candidate_profile.id_user')
							->select('acc_add_file_attachment.id_file','acc_candidate_profile.idExternal','acc_add_file_attachment.link_file','acc_add_file_attachment.file_name')
							->where('acc_add_file_attachment.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_file)){ $data['WebCareer_ID__c'] = $items->id_file; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->link_file)){ $data['File_Url__c'] = $items->link_file; }
				if(!empty($items->file_name)){ $data['Title__c'] = $items->file_name; }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{
			$requestJson2 = $client->post(\Lang::get('acc.postfile'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_add_file_attachment')->where('id_file',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
						return redirect('list-prescreening-result')->with('message',__('core.note_error'))->with('status','error');
			}
		}
		return true;
	}	


/* Bulk Action */
	public function bulkSaveStatus(Request $request){
		$resSelect = $_POST["hasil"];
		$stat = $_POST["stat"];
	
		if(!empty($resSelect)){
			for( $i=0; $i < count($resSelect); $i++){
				\DB::table('acc_prescreening_status')->where('id',$resSelect[$i])->update(['status_prescreening' => $stat]);
				$userArr[] = \DB::table('acc_prescreening_status')->where('id',$resSelect[$i])->value('id_user');
			}
			
			if(strtolower($stat) == "accepted"){
				/* -- Send Applicant Profile--*/
				try{
					$client = new \GuzzleHttp\Client();
					$this->schHitMobileApplicant($userArr, $client);
					$this->schHitMobileEduBack($userArr, $client);
					$this->schHitMobileWorkExp($userArr, $client);
					$this->schHitMobileOrgExp($userArr, $client);
					$this->schHitMobileAppSelection($resSelect,$client);
					$requestJson = $client->post(\Lang::get('acc.loginsf'));
					$responseJson = $requestJson->getBody()->getContents();
					$resultJson = json_decode($responseJson,true);
					if($requestJson->getStatusCode() == "200"){
						$token = $resultJson['access_token'];
						$this->bulkApplicantProfile($userArr, $client, $token);
						$this->bulkAppSelection($resSelect, $client, $token);
						$this->bulkEduBack($userArr, $client, $token);
						$this->bulkOrgExp($userArr, $client, $token);
						$this->bulkWorkExp($userArr, $client, $token);
						$this->bulkFileAttach($userArr, $client, $token);
					}
				} catch(Exception $e){
					\Log::error($e);
				}
			}
			
		}
	}	
	
	public function bulkApplicantProfile($id_user, $client, $token){
		$resultQuery = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
			->select('tb_users.id','acc_candidate_profile.address','acc_candidate_profile.cityofbirth','acc_candidate_profile.cityorregency',
			'acc_candidate_profile.country', 'acc_candidate_profile.dateofbirth','acc_candidate_profile.subdisctrict','tb_users.email','tb_users.first_name',
			'acc_candidate_profile.gender','acc_candidate_profile.homephone','tb_users.last_name','tb_users.avatar','acc_candidate_profile.maritalstatus',
			'acc_candidate_profile.mobilephone','acc_candidate_profile.identitycardnumber','acc_candidate_profile.provinceofbirth',
			'acc_candidate_profile.province','acc_candidate_profile.religion','acc_candidate_profile.title_profile','acc_candidate_profile.postalcode',
			'acc_candidate_profile.npwp','acc_candidate_profile.marrieddate')
			->whereIn('tb_users.id',$id_user)->get();
			
		if(count($resultQuery) > 0){
				foreach($resultQuery as $items){
					if(!empty($items->id)){ $data['WebCareer_ID__c'] = $items->id; }
					if(!empty($items->address)){ $data['Address__c'] = $items->address; } else { unset($data['Address__c']); }
					if(!empty($items->cityofbirth)){ $data['City_of_Birth__c'] = $items->cityofbirth; } else { unset($data['City_of_Birth__c']); }
					if(!empty($items->cityorregency)){ $data['City__c'] = $items->cityorregency; } else { unset($data['City__c']); }
					if(!empty($items->country)){ $data['Country__c'] = $items->country; } else { unset($data['Country__c']); }
					if(!empty($items->dateofbirth)){ $data['Date_of_Birth__c'] = $items->dateofbirth; } else { unset($data['Date_of_Birth__c']); }
					if(!empty($items->subdisctrict)){ $data['District__c'] = $items->subdisctrict; } else { unset($data['District__c']); }
					if(!empty($items->email)){ $data['Email_Address__c'] = $items->email; } else { unset($data['Email_Address__c']); }
					if(!empty($items->first_name)){ $data['First_Name__c'] = $items->first_name; } else { unset($data['First_Name__c']); }
					if(!empty($items->gender)){ $data['Gender__c'] = $items->gender; } else { unset($data['Gender__c']); }
					if(!empty($items->homephone)){ $data['Home_Phone__c'] = $items->homephone; } else { unset($data['Home_Phone__c']); }
					if(!empty($items->last_name)){ $data['Last_Name__c'] = $items->last_name; } else { unset($data['Last_Name__c']); }
					$url_foto = url('uploads/users');
					if(!empty($items->avatar)){ $data['Link_Photo__c'] = $url_foto.'/'.$items->avatar; } else { unset($data['Link_Photo__c']); }
					if(!empty($items->npwp)){ $data['NPWP__c'] = $items->npwp; } else { unset($data['NPWP__c']); }
					if(!empty($items->maritalstatus)){ $data['Marital_Status__c'] = $items->maritalstatus; } else { unset($data['Marital_Status__c']); }
					if(!empty($items->marrieddate)){ 
						if($items->marrieddate != "0000-00-00"){
							$data['Married_Date__c'] = $items->marrieddate; 
						} else { 
							unset($data['Married_Date__c']); } 
					} else { unset($data['Married_Date__c']); }
					if(!empty($items->mobilephone)){ $data['Mobile_Phone__c'] = $items->mobilephone; } else { unset($data['Mobile_Phone__c']); }
					if(!empty($items->first_name) &&  !empty($items->last_name)){ $data['Name'] = $items->first_name.' '.$items->last_name;  } else { unset($data['Name']); }
					if(!empty($items->identitycardnumber)){ $data['National_Id__c'] = $items->identitycardnumber; } else { unset($data['National_Id__c']); }
					if(!empty($items->provinceofbirth)){ $data['Province_of_Birth__c'] = $items->provinceofbirth; } else { unset($data['Province_of_Birth__c']); }
					if(!empty($items->province)){ $data['Province__c'] = $items->province; } else { unset($data['Province__c']); }
					if(!empty($items->religion)){ $data['Religion__c'] = $items->religion; } else { unset($data['Religion__c']); }
					if(!empty($items->title_profile)){ $data['Title__c'] = $items->title_profile; } else { unset($data['Title__c']); }
					if(!empty($items->postalcode)){ $data['Zip_Code__c'] = $items->postalcode; } else { unset($data['Zip_Code__c']); }
					$res[] = $data;
				}
				
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }
			
			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postapplicant'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_candidate_profile')->where('id_user',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
				\Log::error($e);
			}
		}
		return true;
	}

	public function bulkEduBack($id_user, $client, $token){
		$resultQuery = \DB::table('acc_educational_background')
							->leftjoin('acc_candidate_profile','acc_educational_background.id_user','=','acc_candidate_profile.id_user')
							->leftjoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
							->select('acc_educational_background.id_edu_back','acc_candidate_profile.idExternal','acc_educational_background.lasteducation',
							'acc_educational_background.startdate', 'acc_educational_background.endate','acc_universityorschool.id as idUni', 'acc_educational_background.gpa','acc_educational_background.major','acc_educational_background.faculty',
							'acc_educational_background.otherunivorschool','acc_universityorschool.name')->whereIn('acc_educational_background.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_edu_back)){ $data['WebCareer_ID__c'] = $items->id_edu_back; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->lasteducation)){ 
					switch($items->lasteducation){
						case "SMU": $data['Degree__c'] = "Senior High School (SMU)"; break;
						case "D1": $data['Degree__c'] = "Diploma 1 (D1)"; break;
						case "D2": $data['Degree__c'] = "Diploma 2 (D2)"; break;
						case "D3": $data['Degree__c'] = "Asociate Degree 3 (D3)"; break;
						case "D4": $data['Degree__c'] = "Diploma 4 (D4)"; break;
						case "S1": $data['Degree__c'] = "Bachelor Degree (S1)"; break;
						case "S2": $data['Degree__c'] = "Master Degree (S2)"; break;
						case "S3": $data['Degree__c'] = "Doctor (S3)"; break;
						default: unset($data['Degree__c']);
					}
				}
				if(!empty($items->gpa)){ $data['GPA__c'] = $items->gpa;} else { unset($data['GPA__c']); }
				if(!empty($items->major)){ $data['Major__c'] = $items->major;} else { unset($data['Major__c']); }
				if(!empty($items->faculty)){ $data['Faculty__c'] = $items->faculty;} else { unset($data['Faculty__c']); }
				if(!empty($items->startdate)){ $data['From_Year__c'] = $items->startdate;} else { unset($data['From_Year__c']); }
				if(!empty($items->endate)){ $data['To_Year__c'] = $items->endate;} else { unset($data['To_Year__c']); }
				if(!empty($items->idUni)){
					$data['University_Code__c'] = $items->idUni;
					if(!empty($items->name)){
						$data['University__c'] = $items->name;
					} else { unset($data['University__c']); }
				} else { unset($data['University_Code__c']); }
				if(!empty($items->otherunivorschool)){ $data['Others__c'] = $items->otherunivorschool;} else {  unset($data['Others__c']); }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.posteduback'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_educational_background')->where('id_edu_back',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
				\Log::error($e);
			}
		}
		return true;
	}	
	
	public function bulkOrgExp($id_user, $client, $token){	
		$resultQuery = \DB::table('acc_organizational_experience')
							->leftjoin('acc_candidate_profile','acc_organizational_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_organizational_experience.id_org_exp','acc_candidate_profile.idExternal',
							'acc_organizational_experience.organization_experience_startdate','acc_organizational_experience.organizationname',
							'acc_organizational_experience.roleorposition','acc_organizational_experience.rolefunction',
							'acc_organizational_experience.organizationscope','acc_organizational_experience.organization_experience_endate')
							->whereIn('acc_organizational_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_org_exp)){ $data['WebCareer_ID__c'] = $items->id_org_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->organization_experience_startdate)){ $data['From_Year__c'] = $items->organization_experience_startdate;} else { unset($data['From_Year__c']); }
				if(!empty($items->organizationname)){ $data['Name'] = $items->organizationname; } else { unset($data['Name']); }
				if(!empty($items->roleorposition)){ $data['Position__c'] = $items->roleorposition; } else { unset($data['Position__c']); }
				if(!empty($items->rolefunction)){ $data['Role_Function__c'] = $items->rolefunction; } else { unset($data['Role_Function__c']); }
				if(!empty($items->organizationscope)){ $data['Scope__c'] = $items->organizationscope; } else { unset($data['Scope__c']); }
				if(!empty($items->organization_experience_endate)){ $data['To_Year__c'] = $items->organization_experience_endate;} else { unset($data['To_Year__c']); }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

		  $headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postorgexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_organizational_experience')->where('id_org_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
				    \Log::error($e);
			}
		}		
		return true;
	}
	
	public function bulkWorkExp($id_user, $client, $token){	
		$resultQuery = \DB::table('acc_working_experience')
							->leftjoin('acc_candidate_profile','acc_working_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_working_experience.id_wor_exp','acc_candidate_profile.idExternal','acc_working_experience.company',
							'acc_working_experience.jobdescription', 'acc_working_experience.status_working_experience',
							'acc_working_experience.workingexperienceperiodenddate', 'acc_working_experience.workingexperienceperiodstartdate',
							'acc_working_experience.position','acc_working_experience.salary')
							->whereIn('acc_working_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_wor_exp)){ $data['WebCareer_ID__c'] = $items->id_wor_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->company)){ $data['Company__c'] = $items->company; }
				if(!empty($items->jobdescription)){ $data['Job_Description__c'] = $items->jobdescription; } else { $data['Job_Description__c'] = ""; }
				if(!empty($items->status_working_experience)){ $data['Job_Status__c'] = $items->status_working_experience; } else { $data['Job_Status__c'] = ""; }
				if(!empty($items->workingexperienceperiodstartdate)){ $data['Period_Start__c'] = $items->workingexperienceperiodstartdate; }
				if(!empty($items->workingexperienceperiodenddate)){ $data['Period_End__c'] = $items->workingexperienceperiodenddate; }
				if(!empty($items->position)){ $data['Position__c'] = $items->position; } else { $data['Position__c'] = ""; }
				if(!empty($items->salary)){ $data['Salary__c'] = $items->salary; } else { $data['Salary__c'] = ""; }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postworkexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_working_experience')->where('id_wor_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return true;
	}	
	
	public function bulkFileAttach($id_user, $client, $token){		
		$resultQuery = \DB::table('acc_add_file_attachment')
							->leftjoin('acc_candidate_profile','acc_add_file_attachment.id_user','=','acc_candidate_profile.id_user')
							->select('acc_add_file_attachment.id_file','acc_candidate_profile.idExternal','acc_add_file_attachment.link_file','acc_add_file_attachment.file_name')
							->whereIn('acc_add_file_attachment.id_user',$id_user)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_file)){ $data['WebCareer_ID__c'] = $items->id_file; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->link_file)){ $data['File_Url__c'] = $items->link_file; } else { unset($data['File_Url__c']); }
				if(!empty($items->file_name)){ $data['Title__c'] = $items->file_name; } else { unset($data['Title__c']); }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{
			$requestJson2 = $client->post(\Lang::get('acc.postfile'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_add_file_attachment')->where('id_file',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return true;
	}	
	
	public function bulkAppSelection($resSelect, $client, $token){		
		$resultQuery = \DB::table('acc_prescreening_status')->leftjoin('acc_job','acc_prescreening_status.id_job','=','acc_job.id')
							->leftjoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
							->leftjoin('acc_candidate_profile','acc_prescreening_status.id_user','=','acc_candidate_profile.id_user')
							->select('acc_prescreening_status.id','acc_prescreening_status.id_job','acc_prescreening_status.id_user','acc_job.idExternal as jobExt',
							'acc_job.JobTitleName', 'acc_prescreening_status.skor_akhir','acc_prescreening_status.status_prescreening',
							'acc_prescreening_status.apply_date', 'tb_users.first_name','tb_users.last_name', 'acc_candidate_profile.idExternal as candidateExt')
							->whereIn('acc_prescreening_status.id',$resSelect)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id)){ $data['WebCareer_ID__c'] = $items->id; }
				if(!empty($items->candidateExt)){ $data['Applicant_Id__c'] = $items->candidateExt; }
				if(!empty($items->apply_date)){ 
					$s = $items->apply_date;
					$dt = new \DateTime($s);
					$date = $dt->format('Y-m-d');
					$time = $dt->format('H:i:s');
					$formatDate = $date.'T'.$time;
					$data['Apply_Date__c'] = $formatDate; 
				}
				if(!empty($items->jobExt)){ $data['Job_Requisition_Id__c'] = $items->jobExt; }
				if(!empty($items->JobTitleName) && !empty($items->first_name)){ 
					if(!empty($items->last_name)){
						$data['Name'] = $items->first_name.' '.$items->last_name.' - '.$items->JobTitleName; 
					} else {
						$data['Name'] = $items->first_name.' - '.$items->JobTitleName; 
					}
				}
				if(!empty($items->skor_akhir)){ $data['Score__c'] = $items->skor_akhir; }
				if($items->status_prescreening == "accepted"){ $data['Status__c'] = "New Candidate"; }
				$res[] = $data;
			}
			
			$post_data = array('items' => $res);
			
			if($post_data){
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{
			$requestJson2 = $client->post(\Lang::get('acc.postapplicantselection'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_prescreening_status')->where('id',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return true;
	}	
	
	public function schHitMobileApplicant($id_user, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
			->select('tb_users.id','acc_candidate_profile.address','acc_candidate_profile.cityofbirth','acc_candidate_profile.cityorregency',
			'acc_candidate_profile.country', 'acc_candidate_profile.dateofbirth','acc_candidate_profile.subdisctrict','tb_users.email','tb_users.first_name',
			'acc_candidate_profile.gender','acc_candidate_profile.homephone','tb_users.last_name','tb_users.avatar','acc_candidate_profile.maritalstatus',
			'acc_candidate_profile.mobilephone','acc_candidate_profile.identitycardnumber','acc_candidate_profile.provinceofbirth',
			'acc_candidate_profile.province','acc_candidate_profile.religion','acc_candidate_profile.title_profile','acc_candidate_profile.postalcode',
			'acc_candidate_profile.npwp','acc_candidate_profile.marrieddate')
			->whereIn('tb_users.id',$id_user)->get();
			
			if(count($resultQuery) > 0){
				$res = [];
				$data = array();
				foreach($resultQuery as $items){
					if(!empty($items->id)){ $data['webCareer_ID__c'] = $items->id; }
					if(!empty($items->address)){ $data['address__c'] = $items->address; } else { unset($data['address__c']); }
					if(!empty($items->cityofbirth)){ $data['city_of_Birth__c'] = $items->cityofbirth; } else { unset($data['city_of_Birth__c']); }
					if(!empty($items->cityorregency)){ $data['city__c'] = $items->cityorregency; } else { unset($data['city__c']); }
					if(!empty($items->country)){ $data['country__c'] = $items->country; } else { unset($data['country__c']); }
					if(!empty($items->dateofbirth)){ $data['date_of_Birth__c'] = $items->dateofbirth; } else { unset($data['date_of_Birth__c']); }
					if(!empty($items->subdisctrict)){ $data['district__c'] = $items->subdisctrict; } else { unset($data['district__c']); }
					if(!empty($items->email)){ $data['email_Address__c'] = $items->email; } else { unset($data['email_Address__c']); }
					if(!empty($items->first_name)){ $data['first_Name__c'] = $items->first_name; } else { unset($data['first_Name__c']); }
					if(!empty($items->gender)){ $data['gender__c'] = $items->gender; } else { unset($data['gender__c']); }
					if(!empty($items->homephone)){ $data['home_Phone__c'] = $items->homephone; } else { unset($data['home_Phone__c']); }
					if(!empty($items->last_name)){ $data['last_Name__c'] = $items->last_name; } else { unset($data['last_Name__c']); }
					$url_foto = url('uploads/users');
					if(!empty($items->avatar)){ $data['photo__c'] = $url_foto.'/'.$items->avatar; } else { unset($data['photo__c']); }
					if(!empty($items->npwp)){ $data['npwp__c'] = $items->npwp; } else { unset($data['npwp__c']); }
					if(!empty($items->maritalstatus)){ $data['marital_Status__c'] = $items->maritalstatus; } else { unset($data['marital_Status__c']); }
					if(!empty($items->marrieddate)){ 
						if($items->marrieddate != "0000-00-00"){
							$data['married_Date__c'] = $items->marrieddate; 
						} else { 
							unset($data['married_Date__c']); } 
					} else { unset($data['married_Date__c']); }
					if(!empty($items->mobilephone)){ $data['mobile_Phone__c'] = $items->mobilephone; } else { unset($data['mobile_Phone__c']); }
					if(!empty($items->first_name) &&  !empty($items->last_name)){ $data['name'] = $items->first_name.' '.$items->last_name;  } else { unset($data['name']); }
					if(!empty($items->identitycardnumber)){ $data['national_Id__c'] = $items->identitycardnumber; } else { unset($data['national_Id__c']); }
					if(!empty($items->provinceofbirth)){ $data['province_of_Birth__c'] = $items->provinceofbirth; } else { unset($data['province_of_Birth__c']); }
					if(!empty($items->province)){ $data['province__c'] = $items->province; } else { unset($data['province__c']); }
					if(!empty($items->religion)){ $data['religion__c'] = $items->religion; } else { unset($data['religion__c']); }
					if(!empty($items->title_profile)){ $data['title__c'] = $items->title_profile; } else { unset($data['title__c']); }
					if(!empty($items->postalcode)){ $data['zip_Code__c'] = $items->postalcode; } else { unset($data['zip_Code__c']); }
					$res[] = $data;
				}
				if($res){
				\Log::info("Get Profile for Mobile");
				\Log::info(print_r($res,true)); }
				
				$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
		 try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postapplicant'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			} catch(Exception $e){
				\Log::error($e);
				}
			} 
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileEduBack($id_user, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_educational_background')
							->leftjoin('acc_candidate_profile','acc_educational_background.id_user','=','acc_candidate_profile.id_user')
							->leftjoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
							->select('acc_educational_background.id_edu_back','acc_candidate_profile.id_user','acc_educational_background.lasteducation',
							'acc_educational_background.startdate', 'acc_educational_background.endate','acc_universityorschool.id as idUni', 'acc_educational_background.gpa','acc_educational_background.major','acc_educational_background.faculty',
							'acc_educational_background.otherunivorschool','acc_universityorschool.name')->whereIn('acc_educational_background.id_user',$id_user)->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_edu_back)){ $data['webCareer_ID__c'] = $items->id_edu_back; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->lasteducation)){ 
					switch($items->lasteducation){
						case "SMU": $data['degree__c'] = "Senior High School (SMU)"; break;
						case "D1": $data['degree__c'] = "Diploma 1 (D1)"; break;
						case "D2": $data['degree__c'] = "Diploma 2 (D2)"; break;
						case "D3": $data['degree__c'] = "Asociate Degree 3 (D3)"; break;
						case "D4": $data['degree__c'] = "Diploma 4 (D4)"; break;
						case "S1": $data['degree__c'] = "Bachelor Degree (S1)"; break;
						case "S2": $data['degree__c'] = "Master Degree (S2)"; break;
						case "S3": $data['degree__c'] = "Doctor (S3)"; break;
						default: unset($data['degree__c']);
					}
				}
				if(!empty($items->gpa)){ $data['gpa__c'] = $items->gpa;} else { unset($data['gpa__c']); }
				if(!empty($items->major)){ $data['major__c'] = $items->major;} else { unset($data['major__c']); }
				if(!empty($items->faculty)){ $data['faculty__c'] = $items->faculty;} else { unset($data['faculty__c']); }
				if(!empty($items->startdate)){ 
					if($items->startdate != "0000-00-00"){
							$data['from_Year__c'] = $items->startdate; 
						} else { unset($data['from_Year__c']); } 
				} else { unset($data['from_Year__c']); }
				if(!empty($items->endate)){ 
					if($items->endate != "0000-00-00"){
						$data['to_Year__c'] = $items->endate;
					} else { unset($data['to_Year__c']); }
				} else { unset($data['to_Year__c']); }
				if(!empty($items->idUni)){
					$data['university_Code__c'] = $items->idUni;
					if(!empty($items->name)){
						$data['university__c'] = $items->name;
					} else { unset($data['university__c']); }
				} else { unset($data['university_Code__c']); }
				if(!empty($items->otherunivorschool)){ $data['Others__c'] = $items->otherunivorschool;} else {  unset($data['Others__c']); }
				$res[] = $data;
			}

			if($res){
			\Log::info("Get Educational Background for Mobile");
			\Log::info(print_r($res,true)); }
			
			$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_posteduback'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_educational_background')->where('id_edu_back',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
				\Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileWorkExp($id_user, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_working_experience')
							->leftjoin('acc_candidate_profile','acc_working_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_working_experience.id_wor_exp','acc_candidate_profile.id_user','acc_working_experience.company',
							'acc_working_experience.jobdescription', 'acc_working_experience.status_working_experience',
							'acc_working_experience.workingexperienceperiodenddate', 'acc_working_experience.workingexperienceperiodstartdate',
							'acc_working_experience.position','acc_working_experience.salary')
							->whereIn('acc_working_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_wor_exp)){ $data['webCareer_ID__c'] = $items->id_wor_exp; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->company)){ $data['company__c'] = $items->company; } else { unset($data['company__c']); }
				if(!empty($items->jobdescription)){ $data['job_Description__c'] = $items->jobdescription; } else { unset($data['job_Description__c']); }
				if(!empty($items->status_working_experience)){ $data['job_Status__c'] = $items->status_working_experience; } else { unset($data['job_Status__c']); }
				if(!empty($items->workingexperienceperiodstartdate)){ 
					if($items->workingexperienceperiodstartdate != "0000-00-00"){
						$data['period_Start__c'] = $items->workingexperienceperiodstartdate; 
					} else { unset($data['period_Start__c']);}
				} else { unset($data['period_Start__c']); }
				if(!empty($items->workingexperienceperiodenddate)){ 
					if($items->workingexperienceperiodenddate != "0000-00-00"){
						$data['period_End__c'] = $items->workingexperienceperiodenddate; 
					} else { unset($data['period_End__c']); }
				} else { unset($data['period_End__c']); }
				if(!empty($items->position)){ $data['position__c'] = $items->position; } else { unset($data['position__c']); }
				if(!empty($items->salary)){ $data['salary__c'] = $items->salary; } else { unset($data['salary__c']); }
				$res[] = $data;
			}
			if($res){
			\Log::info("Get Working Experience for Mobile");
			\Log::info(print_r($res,true)); }
			
			$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postworkexp'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_working_experience')->where('id_wor_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileOrgExp($id_user, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_organizational_experience')
							->leftjoin('acc_candidate_profile','acc_organizational_experience.id_user','=','acc_candidate_profile.id_user')
							->select('acc_organizational_experience.id_org_exp','acc_candidate_profile.id_user',
							'acc_organizational_experience.organization_experience_startdate','acc_organizational_experience.organizationname',
							'acc_organizational_experience.roleorposition','acc_organizational_experience.rolefunction',
							'acc_organizational_experience.organizationscope','acc_organizational_experience.organization_experience_endate')
							->whereIn('acc_organizational_experience.id_user',$id_user)->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_org_exp)){ $data['webCareer_ID__c'] = $items->id_org_exp; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->organization_experience_startdate)){ 
					if($items->organization_experience_startdate != "0000-00-00"){
						$data['from_Year__c'] = $items->organization_experience_startdate;
					} else { unset($data['from_Year__c']); }
				} else { unset($data['from_Year__c']); }
				if(!empty($items->organizationname)){ $data['name'] = $items->organizationname; } else { unset($data['name']); }
				if(!empty($items->roleorposition)){ $data['position__c'] = $items->roleorposition; } else { unset($data['position__c']); }
				if(!empty($items->rolefunction)){ $data['role_Function__c'] = $items->rolefunction; } else { unset($data['role_Function__c']); }
				if(!empty($items->organizationscope)){ $data['scope__c'] = $items->organizationscope; } else { unset($data['scope__c']); }
				if(!empty($items->organization_experience_endate)){ 
					if($items->organization_experience_endate != "0000-00-00"){
						$data['to_Year__c'] = $items->organization_experience_endate;
					} else { unset($data['to_Year__c']); }
				} else { unset($data['to_Year__c']); }
				$res[] = $data;
			}
			
			if($res){
			\Log::info("Get Organizational Experience for Mobile");
			\Log::info(print_r($res,true)); }

		  $headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postorgexp'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_organizational_experience')->where('id_org_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
				    \Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}	
	
	public function schHitMobileAppSelection($resSelect, $client){		
		$resultQuery = \DB::table('acc_prescreening_status')->leftjoin('acc_job','acc_prescreening_status.id_job','=','acc_job.id')
							->leftjoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
							->leftjoin('acc_candidate_profile','acc_prescreening_status.id_user','=','acc_candidate_profile.id_user')
							->select('acc_prescreening_status.id','acc_prescreening_status.id_job','acc_prescreening_status.id_user','acc_job.idExternal as jobExt',
							'acc_job.JobTitleName', 'acc_prescreening_status.skor_akhir','acc_prescreening_status.status_prescreening',
							'acc_prescreening_status.apply_date', 'tb_users.first_name','tb_users.last_name', 'acc_candidate_profile.idExternal as candidateExt')
							->whereIn('acc_prescreening_status.id',$resSelect)->get();
		
		if(count($resultQuery) >= 1){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id)){ $data['webCareerId'] = $items->id; }
				//if(!empty($items->candidateExt)){ $data['Applicant_Id__c'] = $items->candidateExt; }
				if(!empty($items->apply_date)){ 
					$s = $items->apply_date;
					$dt = new \DateTime($s);
					$date = $dt->format('Y-m-d');
					$time = $dt->format('H:i:s');
					$formatDate = $date.'T'.$time;
					$data['applyDate'] = $formatDate; 
				}
				if(!empty($items->JobTitleName)){ $data['jobRequisition'] = $items->JobTitleName; }
				if(!empty($items->id_job)){ $data['jobRequisitionId'] = $items->id_job; }
				if(!empty($items->id_user)){ $data['applicantWebCareerId'] = $items->id_user; }
				if(!empty($items->JobTitleName) && !empty($items->first_name)){ 
					if(!empty($items->last_name)){
						$data['name'] = $items->first_name.' '.$items->last_name.' - '.$items->JobTitleName; 
					} else {
						$data['name'] = $items->first_name.' - '.$items->JobTitleName; 
					}
				}
				if(!empty($items->skor_akhir)){ $data['score'] = $items->skor_akhir; }
				//if($items->status_prescreening == "accepted"){ $data['Status__c'] = "New Candidate"; }
				$res[] = $data;
			}
			
			if($res){
			\Log::info("Get Application Selection for Mobile");
			\Log::info(print_r($res,true)); }

			$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postapplicantselection'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_prescreening_status')->where('id',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return true;
	}	
	
}
