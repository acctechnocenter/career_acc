<?php namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\User;
use Validator, Redirect;
use App\Models\Candidateprofile; //candidateprofile

class DashboardController extends Controller {

	public function __construct()
	{
		parent::__construct();
		
        $this->data = array(
            'pageTitle' =>  $this->config['cnf_appname'],
            'pageNote'  =>  'Welcome to Dashboard',
            
        );			
	}

	public function index( Request $request )
	{
		if(\Auth::check()){
			$userId = \Auth::user()->id;
			$is_user= \AccHelpers::is_user($userId);
			if(!$is_user){
				$countPending = \DB::table('tb_comments')->where('comment_status','pending')->count();
				$query = \DB::table('tb_comments')->leftJoin('tb_pages','tb_comments.pageID','=','tb_pages.pageID')
								->where('tb_comments.comment_status','pending')->select('tb_comments.pageID','tb_pages.title', \DB::raw('count(*) as pending'))->groupBy('tb_comments.pageID')->orderBy('pending','desc')->limit(5)->get();
				$this->data['countPending'] = $countPending;
				$this->data['rowData'] = $query;
				return view('dashboard.index',$this->data);
			} else {
				return redirect('user/profile');
			}
			
		} else {
			return redirect('');
		}	
	}	
	
	function getApplicant(){    

	    $result = \DB::table('tb_users')
	    ->join('acc_candidate_profile', 'tb_users.id','=','acc_candidate_profile.id_user')
	    ->select('tb_users.id as idApplicant', 'tb_users.first_name' , 'tb_users.last_name','tb_users.email','acc_candidate_profile.gender','acc_candidate_profile.dateofbirth','acc_candidate_profile.cityorregency','tb_users.active')
	    ->paginate(10);
	    $this->data['rowData'] = $result;
	    $this->data['title'] = 'Applicant Menu';
	    return view('applicantmenu.Applicant-list' , $this->data);
	}
		
	public function filterSearchApp(Request $request){
		$firstname= Input::get('appfirstname');
		$lastname= Input::get('applastname');
		$email= Input::get('appemail');
		
		$result = \DB::table('tb_users')
			->join('acc_candidate_profile', 'tb_users.id','=','acc_candidate_profile.id_user')
			->select('tb_users.id as idApplicant', 'tb_users.first_name' , 'tb_users.last_name','tb_users.email','acc_candidate_profile.gender','acc_candidate_profile.dateofbirth','acc_candidate_profile.cityorregency','tb_users.active')
			->when($firstname, function ($firstnameQuery) use ($firstname) {
					return $firstnameQuery->where('tb_users.first_name', 'like', '%'.$firstname.'%');
				})
			->when($lastname, function ($lastnameQuery) use ($lastname) {
					return $lastnameQuery->where('tb_users.last_name', 'like', '%'.$lastname.'%');
				})
			->when($email, function ($emailQuery) use ($email) {
					return $emailQuery->where('tb_users.email', 'like', '%'.$email.'%');
				})
			->paginate(10);
		$this->data['rowData'] = $result;
		$this->data['title'] = 'Applicant Menu';
		return view('applicantmenu.Applicant-list' , $this->data);		
	}
	
	public function getApplicantDetail($idApplicant){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    	    
	    $result = \DB::table('tb_users')
	    ->join('acc_candidate_profile', 'tb_users.id','=','acc_candidate_profile.id_user')
	    ->select('tb_users.id as idApplicant', 'tb_users.first_name' , 'tb_users.last_name','tb_users.active','acc_candidate_profile.*')->where('id_user','=',$idApplicant)->get();
	    
	    $fileattachData = \DB::table('acc_add_file_attachment')->where('id_user','=',$idApplicant)->get();
	    
	    $this->data = array(
	        'pageTitle'	=> 'Applicant Detail Menu',
	        'info'		=> $idApplicant,
	        'rowData' => $result,
	        'fileattachData' => $fileattachData
	    );
	    
	    return view('applicantmenu.ApplicantDetail' , $this->data);
	}
	
	public function getEdubackDetail($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $educationalbackData = \DB::table('acc_educational_background')
		->leftJoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
		->select('acc_educational_background.*','acc_universityorschool.name')->where('id_user','=',$id)->get();
	    $this->data['rowData'] = $educationalbackData;	  
		$this->data['edu_exp'] = $id;		
	    $this->data['title'] = 'View Educational Exp';
	    return view('applicantmenu.edubackdetail' , $this->data);
	}
	
	public function getOrgexpDetail($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $OrgexpDetail = \DB::table('acc_organizational_experience')->where('id_user','=',$id)->get();
	    $this->data['rowData'] = $OrgexpDetail;
	    $this->data['org_exp'] = $id;	
	    $this->data['title'] = 'View Organizational Experience Detail';
	    return view('applicantmenu.orgexpdetail' , $this->data);
	}
	
	public function getWorkexpDetail($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');	
	    
	    $WorkexpDetail = \DB::table('acc_working_experience')->where('id_user','=',$id)->get();
	    $this->data['rowData'] = $WorkexpDetail;
	    $this->data['work_exp'] = $id;	
	    $this->data['title'] = 'View Working Experience Detail';
	    return view('applicantmenu.workexpdetail' , $this->data);
	}

	//tambahan save edit personal data applicant menu
	public function getEditDataApplicant($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $applicantData = \DB::table('acc_candidate_profile')
	    ->leftJoin('acc_jobfield','acc_candidate_profile.Careerpreference','=','acc_jobfield.id')
	    ->leftJoin('tb_users','acc_candidate_profile.id_user','=','tb_users.id')
	    ->select('acc_candidate_profile.*','acc_jobfield.*', 'tb_users.id as id', 'tb_users.*')
	    ->where('id_user','=',$id)->first();
	    $jobfield = \DB::table('acc_jobfield')->get();
	    
	    
	    if(count($applicantData) > 0){
	        $interest = $applicantData->Careerpreference;
	        $showgender = $applicantData->gender;
	    } else {
	        $interest = '';
	        $showgender = '';
	    }
	    $explode_career = explode(", ", $interest);
	    $explode_gender = explode(", ", $showgender);
	    
	    $this->data = array(
	        'pageTitle'	=> 'My Profile',
	        'pageNote'	=> 'View Detail My Info',
	        'id' => $id,
	        'applicantData'		=> $applicantData,
	        'explode_career'=>$explode_career,
	        'explode_gender' => $explode_gender,
	        'jobfield'=> $jobfield
	    );
	    return view('applicantmenu.editdataapplicant' , $this->data);
	}
	
	
	public function postUpdateApplicantData( Request $request)
	{
		
		// Make Sure users Logged 
		if(!\Auth::check()) 
		return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    $this->model = new Candidateprofile();
	    
	    $id = $request->input('id');
	    $id_user = $request->input('id_user');
	    
	    //personal details
	    $title_profile = $request->input('title_profile');
	    $gender= $request->input('gender');
	    $dateofbirth = $request->input('dateofbirth');
	    $provinceofbirth = $request->input('provinceofbirth');
	    $cityofbirth = $request->input('cityofbirth');
	    $identitycardnumber = $request->input('identitycardnumber');
	    $homephone = $request->input('homephone');
	    $mobilephone = $request->input('mobilephone');
	    $npwp = $request->input('npwp');
	    $maritalstatus = $request->input('maritalstatus');
	    $marrieddate = $request->input('marrieddate');
	    $religion = $request->input('religion');
	    $expectedsalaryfrom = $request->input('expectedsalaryfrom');
	    $expectedsalaryupto = $request->input('expectedsalaryupto');
	    $optyfrom = $request->input('optyfrom');
	    $agreement = $request->input('terms');
	    $careerpreference = $request->input('careerpreference');
	    
	    $imp_career = implode(", ", $careerpreference);
	    
	    //Home Adress
	    $country= $request->input('country');
	    $address= $request->input('address');
	    $subdisctrict= $request->input('subdisctrict');
	    $cityorregency = $request->input('cityorregency');
	    $province = $request->input('province');
	    $postalcode = $request->input('postalcode');    
	      
	    //yourinterestandadditionalinformationinfo
	    $careerpreference = $request->input('careerpreference');
	    
	    
	    $expectedsalary = $request->input('expectedsalary');
	    $expectedsalaryupto = $request->input('expectedsalaryupto');
	    $optyfrom = $request->input('optyfrom');
	    
	    
	    $items = array(
	        'title_profile' => $title_profile,
	        'gender' => $gender,
	        'dateofbirth' => $dateofbirth,
	        'provinceofbirth' => $provinceofbirth,
	        'cityofbirth' => $cityofbirth,
	        'identitycardnumber' => $identitycardnumber,
	        'homephone' => $homephone,
	        'mobilephone' => $mobilephone,
	        'npwp' => $npwp,
	        'maritalstatus' => $maritalstatus,
	        'marrieddate' => $marrieddate,
	        'religion' => $religion,
	        'careerpreference'=> $imp_career,
	        'expectedsalaryfrom'=> $expectedsalaryfrom,
	        'expectedsalaryupto'=> $expectedsalaryupto,
	        'optyfrom' => $optyfrom,
	        'agreement' => $agreement,
	        'country'=>'Indonesia',
	        'address'=>$address,
	        'subdisctrict'=>$subdisctrict,
	        'cityorregency'=>$cityorregency,
	        'province'=>$province,
	        'postalcode'=>$postalcode,
	        'updated_date' => date('Y-m-d H:i:s'),
	        'created_date' => date('Y-m-d H:i:s')
	        
	    );
	    
	    try{
	        $items = [
	            'title_profile' => $title_profile,
	            'gender' => $gender,
	            'dateofbirth' => $dateofbirth,
	            'provinceofbirth' => $provinceofbirth,
	            'cityofbirth' => $cityofbirth,
	            'identitycardnumber' => $identitycardnumber,
	            'homephone' => $homephone,
	            'mobilephone' => $mobilephone,
	            'npwp' => $npwp,
	            'maritalstatus' => $maritalstatus,
	            'marrieddate' => $marrieddate,
	            'religion' => $religion,
	            'careerpreference'=> $imp_career,
	            'expectedsalaryfrom'=> $expectedsalaryfrom,
	            'expectedsalaryupto'=> $expectedsalaryupto,
	            'optyfrom' => $optyfrom,
	            'agreement' => $agreement,
	            'country'=>'Indonesia',
	            'address'=>$address,
	            'subdisctrict'=>$subdisctrict,
	            'cityorregency'=>$cityorregency,
	            'province'=>$province,
	            'postalcode'=>$postalcode,
	            'updated_date' => date('Y-m-d H:i:s'),
	            'created_date' => date('Y-m-d H:i:s')
	        ];
	        
	        \DB::table('acc_candidate_profile')->where('id_user',$id)->update($items);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }    

	    
	}
	
	//tambahan save edit personal data applicant menu

	//tambahan educational back applicant menu
	public function getEditEduAppMenu($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $result = \DB::table('acc_educational_background')
	    ->leftjoin('acc_universityorschool', 'acc_educational_background.universityorschool','=','acc_universityorschool.id')
	    ->select('acc_educational_background.id_user as id','acc_educational_background.*', 'acc_universityorschool.*')
	    ->where('id_edu_back',$id)->orderBy('id_edu_back','desc')->first();

	    $test_name = \DB::table('acc_universityorschool')->orderBy('name','asc')->get();
	    
	    $this->data = array(
	        'pageTitle'	=> 'Edit Educational Exp',
	        'rowData'		=> $result,
	        'universityorschoolname'		=> $test_name
	    );
	    
	    return view('applicantmenu.appmenu_edit_edu_exp' , $this->data);
	    
	}
	
	public function getAddEduapp(Request $request, $idApplicantAddedu){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');	   
	    
	    $result = \DB::table('acc_educational_background')->orderBy('id_edu_back','desc')->first();	    
	    //lookupuniversity
	    $universityorschooldb = \DB::table('acc_universityorschool')->orderBy('name','asc')->get();
	    
	    $this->data = array(
	        'pageTitle'	=> 'Add Educational Exp Applicant Menu',
	        'rowData'		=> $result,
	        'idApplicantAddedu' =>$idApplicantAddedu,	        
	        'universityorschoolname'		=> $universityorschooldb
	    );
	    
	    return view('applicantmenu.appmenu_add_edu_exp' , $this->data);
	}
	
	public function saveAddEduExpAppMenu(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit = $request->input('id_edit');
	    $id_user = $request->input('id_user');
	    $lasteducation = $request->input('lasteducation');
	    $universityorschool = $request->input('universityorschool');
	    $faculty = $request->input('faculty');
	    $major = $request->input('major');
	    $gpa = $request->input('gpa');
	    $startdate = $request->input('startdate');
	    $endate = $request->input('endate');
	    $otherunivorschool = $request->input('otherunivorschool');
	    
	    try{
	        $insert = [
	            'id_edu_back' => $id_edit,
	            'id_user' => $id_user,
	            'lasteducation'     => $lasteducation,
	            'universityorschool'   => $universityorschool,
	            'faculty'     => $faculty,
	            'major'    => $major,
	            'gpa'     => $gpa,
	            'startdate'    => $startdate,
	            'endate'    => $endate,
	            'otherunivorschool'=>$otherunivorschool,
	            'updated_date' => date('Y-m-d H:i:s')
	            
	        ];
	        
	        \DB::table('acc_educational_background')->where('id_user',$id_user)  ->insert($insert);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	}
	
	public function saveEditEduExpAppMenu(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit = $request->input('id_edit');
	    $id_user = $request->input('id_user');
	    $lasteducation = $request->input('lasteducation');
	    $universityorschool = $request->input('universityorschool');
	    $faculty = $request->input('faculty');
	    $major = $request->input('major');
	    $gpa = $request->input('gpa');
	    $startdate = $request->input('startdate');
	    $endate = $request->input('endate');
	    $otherunivorschool = $request->input('otherunivorschool');
	    
	    try{
	        $update = [
	            'id_edu_back' => $id_edit,
	            'id_user' => $id_user,
	            'lasteducation'     => $lasteducation,
	            'universityorschool'   => $universityorschool,
	            'faculty'     => $faculty,
	            'major'    => $major,
	            'gpa'     => $gpa,
	            'startdate'    => $startdate,
	            'endate'    => $endate,
	            'otherunivorschool'=>$otherunivorschool,
	            'updated_date' => date('Y-m-d H:i:s')
	            
	        ];
	        
	        \DB::table('acc_educational_background')->where('id_edu_back',$id_edit)->update($update);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	}
	/*delete records applicant menu educational background*/
	public function destroyAppMenuEdu($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    try{
	        \DB::table('acc_educational_background')->where('id_edu_back',$id)->delete();
	        return redirect('applicant-menu')->with('message',__('core.note_success'))->with('status','success');
	    }catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu')->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}
	
	/*delete records applicant menu educational background*/
	//tambahan educational back applicant menu

	//tambahan organizational exp applicant menu
	
	//add
	public function getAddorgapp(Request $request, $idApplicantAddorg){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $result = \DB::table('acc_organizational_experience')->orderBy('id_org_exp','desc')->first();
	    $this->data['rowData'] = $result;
	    $this->data['org_exp'] = '0';
	    $this->data['idApplicantAddorg']= $idApplicantAddorg;
	    $this->data['title'] = 'Add Organization Exp';
	    return view('applicantmenu.appmenu_add_org_exp' , $this->data);
	}
	
	public function saveAddOrgExpAppMenu(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit_org = $request->input('id_edit_org');
	    $id_user = $request->input('id_user');
	    $organizationname = $request->input('organizationname');
	    $organizationscope = $request->input('organizationscope');
	    $organization_experience_startdate = $request->input('organization_experience_startdate');
	    $organization_experience_endate = $request->input('organization_experience_endate');
	    $rolefunction = $request->input('rolefunction');
	    $roleorposition = $request->input('roleorposition');
	    
	    try{
	        $insert = [
	            'id_org_exp' => $id_edit_org,
	            'id_user' => $id_user,
	            'organizationname'     => $organizationname,
	            'organizationscope'   => $organizationscope,
	            'organization_experience_startdate'     => $organization_experience_startdate,
	            'organization_experience_endate'    => $organization_experience_endate,
	            'rolefunction'     => $rolefunction,
	            'roleorposition'    => $roleorposition,
	            'created_date' => date('Y-m-d H:i:s')
	        ];
	        
	        \DB::table('acc_organizational_experience')->where('id_org_exp',$id_edit_org)->insert($insert);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	}
	
	//edit
	public function getEditOrgAppMenu($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    $result = \DB::table('acc_organizational_experience')->where('id_org_exp',$id)->orderBy('id_org_exp','desc')->first();
	    $this->data['rowData'] = $result;
	    $this->data['org_exp'] = '0';
	    $this->data['title'] = 'Edit Organization Exp';
	    return view('applicantmenu.appmenu_edit_org_exp' , $this->data);
	}
	
	public function saveEditOrgExpAppMenu(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit = $request->input('id_edit');
	    $id_user = $request->input('id_user');
	    $organizationname = $request->input('organizationname');
	    $organizationscope = $request->input('organizationscope');
	    $organization_experience_startdate = $request->input('organization_experience_startdate');
	    $organization_experience_endate = $request->input('organization_experience_endate');
	    $rolefunction = $request->input('rolefunction');
	    $roleorposition = $request->input('roleorposition');
	    
	    try{
	        $update = [
	            'id_org_exp' => $id_edit,
	            'id_user' => $id_user,
	            'organizationname'     => $organizationname,
	            'organizationscope'   => $organizationscope,
	            'organization_experience_startdate'     => $organization_experience_startdate,
	            'organization_experience_endate'    => $organization_experience_endate,
	            'rolefunction'     => $rolefunction,
	            'roleorposition'    => $roleorposition,
	            'updated_date' => date('Y-m-d H:i:s')
	        ];
	        
	        \DB::table('acc_organizational_experience')->where('id_org_exp',$id_edit)->update($update);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}
/*delete Organizational Experience applicant menu*/
	public function destroyAppMenuOrg($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    try{
	        \DB::table('acc_organizational_experience')->where('id_org_exp',$id)->delete();
	        return redirect('applicant-menu')->with('message',__('core.note_success'))->with('status','success');
	    }catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu')->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}
	/*delete Organizational Experience applicant menu*/
	//tambahan organizational exp applicant menu

	//tambahan working exp applicant menu
	//add
	public function getAddworkapp(Request $request, $idApplicantAddwork){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $result = \DB::table('acc_working_experience')->orderBy('id_wor_exp','desc')->first();
	    $this->data['rowData'] = $result;
	    $this->data['work_exp'] = '0';
	    $this->data['idApplicantAddwork']= $idApplicantAddwork;
	    $this->data['title'] = 'Add Work Exp';
	    return view('applicantmenu.appmenu_add_work_exp' , $this->data);
	}
	
	public function saveAddworkExpAppMenu(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit_work = $request->input('id_edit_work');
	    $id_user = $request->input('id_user');
	    $company = $request->input('company');
	    $workingexperienceperiodstartdate = $request->input('workingexperienceperiodstartdate');
	    $workingexperienceperiodenddate = $request->input('workingexperienceperiodenddate');
	    $position = $request->input('position');
	    $category = $request->input('category');
	    $status_working_experience = $request->input('status_working_experience');
	    $jobdescription = $request->input('jobdescription');
	    $salary = $request->input('salary');
	    $Reasonofleaving = $request->input('Reasonofleaving');
	    
	    try{
	        $insert = [
	            'id_wor_exp' => $id_edit_work,
	            'id_user' => $id_user,
	            'company'     => $company,
	            'workingexperienceperiodstartdate'   => $workingexperienceperiodstartdate,
	            'workingexperienceperiodenddate'     => $workingexperienceperiodenddate,
	            'position'    => $position,
	            'category'     => $category,
	            'status_working_experience'    => $status_working_experience,
	            'jobdescription'    => $jobdescription,
	            'salary'     => $salary,
	            'Reasonofleaving'    => $Reasonofleaving,
	            'created_date' => date('Y-m-d H:i:s')
	        ];	        
	        \DB::table('acc_working_experience')->where('id_wor_exp',$id_edit_work)->insert($insert);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}
	
	//edit working experience
	public function getEditWorkAppMenu($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    $result = \DB::table('acc_working_experience')->where('id_wor_exp',$id)->orderBy('id_wor_exp','desc')->first();
	    $this->data['rowData'] = $result;
	    $this->data['work_exp'] = '0';
	    $this->data['title'] = 'Edit Working Exp';
	    return view('applicantmenu.appmenu_edit_work_exp' , $this->data);
	}
	
	public function saveEditWorkExpAppMenu(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $id_edit = $request->input('id_edit');
		$id_user = $request->input('id_user');
	    $company = $request->input('company');
	    $workingexperienceperiodstartdate = $request->input('workingexperienceperiodstartdate');
	    $workingexperienceperiodenddate = $request->input('workingexperienceperiodenddate');
	    $position = $request->input('position');
	    $category = $request->input('category');
	    $status_working_experience = $request->input('status_working_experience');
	    $jobdescription = $request->input('jobdescription');
	    $salary = $request->input('salary');
	    $Reasonofleaving = $request->input('Reasonofleaving');
	    
	    
	    try{
	        $update = [
	            'id_wor_exp' => $id_edit,
			'id_user' => $id_user,
	            'company'     => $company,
	            'workingexperienceperiodstartdate'   => $workingexperienceperiodstartdate,
	            'workingexperienceperiodenddate'     => $workingexperienceperiodenddate,
	            'position'    => $position,
	            'category'     => $category,
	            'status_working_experience'    => $status_working_experience,
	            'jobdescription'    => $jobdescription,
	            'salary'     => $salary,
	            'Reasonofleaving'    => $Reasonofleaving,
	            'updated_date' => date('Y-m-d H:i:s'),
	        ];
	        
	        \DB::table('acc_working_experience')->where('id_wor_exp',$id_edit)->update($update);
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_success'))->with('status','success');
	    } catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}

	/*delete Working Experience applicant menu*/
	public function destroyAppMenuWork($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

	    try{
	        \DB::table('acc_working_experience')->where('id_wor_exp',$id)->delete();
	        return redirect('applicant-menu')->with('message',__('core.note_success'))->with('status','success');
	    }catch (\Illuminate\Database\QueryException $ex){
	        return redirect('applicant-menu')->with('message',__('core.note_error'))->with('status','error');
	    }
	    
	}
	/*delete Working Experience applicant menu*/
	//tambahan working exp applicant menu

//tambahan file attachment
	public function getEditFileAttachmentApplicant($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $applicantData = \DB::table('acc_candidate_profile')
	    ->leftJoin('acc_jobfield','acc_candidate_profile.Careerpreference','=','acc_jobfield.id')
	    ->leftJoin('tb_users','acc_candidate_profile.id_user','=','tb_users.id')
	    ->select('acc_candidate_profile.*','acc_jobfield.*', 'tb_users.id as id', 'tb_users.*')
	    ->where('id_user','=',$id)->first();
	    
	    $fileattachData = \DB::table('acc_add_file_attachment')
	    ->where('id_user','=',$id)->get();
	    
	    $this->data = array(
	        'pageTitle'	=> 'My Fileattachment Applicant menu',
	        'pageNote'	=> 'View Fileattachment',
	        'fileattachData' => $fileattachData,
	        'applicantData'		=> $applicantData,
		'test_id' =>$id
	    );
	    return view('applicantmenu.editfileattchment',$this->data);    
	}	
	
	public function indexfileattachmentappicant(){
	    return view('editfileattchment');
	}
	
	public function postupdatefileattachmentapplicantm(Request $request){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
	    
	    $info =	User::find($request->input('test_id'));
	    $id_user = $request->input('test_id');
	    $first = str_replace(" ","_",$info->first_name);
	    $last = str_replace(" ","_",$info->last_name);
	    
	    $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];
	    $tempVal=[];	    
 	    foreach($jenisFile as $itemFile){
 	        $tempVal[$itemFile]= 'mimes:jpeg,png,jpg,docx,doc,pdf|max:2048';
 	    }	    
	    $validator = Validator::make($request->all(), $tempVal);
	    
	    if ($validator->passes()) {
	        
	        foreach($jenisFile as $fl){
	            
	            if(!is_null($request->file($fl))){
	                
	                $file = request()->file($fl);
	                $getimageName = $first.'_'.$last.'_'.$fl.'_'.time().'.'.$request->$fl->getClientOriginalExtension();
	                $uploadSuccess= $request->$fl->move(public_path().'/uploads/fileattachment/', $getimageName);
	                $type=$fl;
	                $saveurl= url('uploads/fileattachment/'. $getimageName);
	                
	                $check = \DB::table('acc_add_file_attachment')->where([['id_user','=',$info->id],['type_file','=',$fl]])->first();
	                
	                if($check){
	                    $tempQuery = $check->id_file;
	                    $tempEdit = [
	                        'file_name' => $getimageName,
	                        'updated_at' => date('Y-m-d H:i:s'),
	                        'link_file' =>  $saveurl
	                    ];
	                    //$removeFile = unlink(public_path().'/uploads/fileattachment/'.$check->file_name);
	                    try{
	                        \DB::table('acc_add_file_attachment')->where('id_file',$tempQuery)->update($tempEdit);
	                    } catch(\Illuminate\Database\QueryException $ex)	{
	                        \Log::info($ex);
	                        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message','File Attachment Gagal di upload!')->with('status','error');
	                    }
	                } else {
	                    $tempAdd = [
	                        'file_name' => $getimageName,
	                        'id_user' => $info->id,
	                        'created_at' => date('Y-m-d H:i:s'),
	                        'type_file' => $type,
	                        'link_file' =>  $saveurl
	                    ];
	                    try{
	                        \DB::table('acc_add_file_attachment')->insert($tempAdd);
	                    } catch(\Illuminate\Database\QueryException $ex)	{
	                        \Log::info($ex);
	                        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message','File Attachment Gagal di upload!')->with('status','error');
	                    }
	                }
	            }
	        }
	        
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message','File Attachment Berhasil Tersimpan!')->with('status','success');
	        
	    }else {
	        return redirect('applicant-menu/ApplicantDetail/'.$id_user)->with('message','File Attachment Gagal di upload!')->with('status','error')->withErrors($validator)->withInput();
	    }
	    
	}
/*delete File Attachment applicant menu*/
	public function destroyAppMenuFileattach(Request $request, $id){
	    	    
 	    $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];
	   
 	    $checks = \DB::table('acc_add_file_attachment')->where('id_file',$id)->first();
 	    $tempid = $checks->id_user;
	    
 	    if($checks){
 	        $tempQuery_delete = $checks->id_file;
 	        $tempEdit_delete = [
 	            'link_file' =>  '',
		    'updated_at' => date('Y-m-d H:i:s')
 	        ];
 	        $removeFiles = unlink(public_path().'/uploads/fileattachment/'.$checks->file_name); 	         
  	         try{
  	             \DB::table('acc_add_file_attachment')->where('id_file',$tempQuery_delete)->update($tempEdit_delete);
  	             //\DB::table('acc_add_file_attachment')->where('id_file',$id)->delete();
  	             return redirect('applicant-menu/ApplicantDetail/'.$tempid)->with('message',__('core.note_success'))->with('status','success');
 	         }catch (\Illuminate\Database\QueryException $ex){
 	             return redirect('applicant-menu/ApplicantDetail/'.$tempid)->with('message',__('core.note_error'))->with('status','error');
  	         }
 	    }	    
	    
	}
	/*delete File Attachment applicant menu*/	//tambahan file attachment
	
	public function editApplicantStatus(Request $request, $id){
		$user = $request->getUser();
		$pass = $request->getPassword();
		$id_applicant_selection = $request->id_applicant_selection;
		$tracking_status = $request->tracking_status;
		$type = $request->type;
		$place = $request->place;
		$pic = $request->pic;
		$type_status = $request->type_status;
		$scheduled_date = $request->scheduled_date;
		$times = $request->times;
		$applicant_status = $request->applicant_status;
		$name = $request->name;
		
		$checkIDExisting = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
										->where('tb_users.id',$id)->first();
		$checkPreRes = \DB::table('acc_prescreening_status')->where([['id',$id_applicant_selection],['id_user',$id]])->first();
		
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			if($checkIDExisting && $checkPreRes){
				$response = [
					'success' => true,
					'id_user' => $id,
					'id_applicant_selection' => $id_applicant_selection,
					'tracking_status' => $tracking_status,
					'type' => $type,
					'place' => $place,
					'pic' => $pic,
					'type_status' => $type_status,
					'scheduled_date' => $scheduled_date,
					'times' => $times,
					'applicant_status' => $applicant_status,
					'name' => $name,
					];
				if($applicant_status === NULL){
					if(strtolower($type_status) == "failed"){
						\DB::table('acc_candidate_profile')->where('id_user',$id)->update(['ApplicantStatus' => null ]);
						\DB::table('acc_prescreening_status')->where('id',$id_applicant_selection)->update(['status_prescreening' => "failed"]);
					} else {
						\DB::table('acc_candidate_profile')->where('id_user',$id)->update(['ApplicantStatus' => $tracking_status ]);
					}
					\DB::table('tb_notification')->insert(
						['userid' => $id, 'url' => url('user/profile'), 'title' => 'Progress your applied job', 'note' => 'Status Anda berubah menjadi '.$tracking_status, 
						'created' => date("Y-m-d H:i:s"), 'is_read' => '0']
					);
					$this->sendEmail($id_applicant_selection, $type, $type_status, $place, $pic, $scheduled_date, $times, $name);
				} else {
					if(strtolower($applicant_status) == "hired"){
						\DB::table('acc_candidate_profile')->where('id_user',$id)->update(['ApplicantStatus' => $applicant_status ]);
						\DB::table('tb_users')->where('id',$id)->update(['active' => '0']);
					} else if(strtolower($applicant_status) == "rejected") {
						\DB::table('acc_candidate_profile')->where('id_user',$id)->update(['ApplicantStatus' => null ]);
						\DB::table('acc_prescreening_status')->where('id',$id_applicant_selection)->update(['status_prescreening' => "failed"]);
					}
					\DB::table('tb_notification')->insert(
						['userid' => $id, 'url' => url('user/profile'), 'title' => 'Progress your applied job', 'note' => 'Status Anda berubah menjadi '.$applicant_status, 
						'created' => date("Y-m-d H:i:s"), 'is_read' =>'0']
					);
				}
			} else{
				$response = [
				'success' => false,
				'message' => "Data not found"
				];
			}
		} else{
			$response = [
				'success' => false,
				'message' => "Permission denied"
			];
		}

        return response()->json($response, 200);
	}
	
		/* function sending email */
		public function sendEmail($id_applicant_selection, $type, $type_status, $place, $pic, $scheduled_date, $times,$name){
			$result = \DB::table('acc_prescreening_status')->leftJoin('tb_users','acc_prescreening_status.id_user','=','tb_users.id')
							->leftJoin('acc_job','acc_prescreening_status.id_job','=','acc_job.id')->leftJoin('acc_placement','acc_job.Location','=','acc_placement.id')
							->select('tb_users.first_name','tb_users.last_name', 'tb_users.email','acc_job.JobTitleName','acc_placement.name')
							->where('acc_prescreening_status.id',$id_applicant_selection)->first();
			if($result){
			$data = array(
					'firstname'	=> $result->first_name ,
					'lastname'	=> $result->last_name ,
					'email'		=> $result->email,
					'job'		=> $result->JobTitleName,
					'placement' => $result->name,
					'scheduled_date' => $scheduled_date,
					'times' => $times,
					'place' => $place,
					'pic' => $pic,
					'name' => $name
				);
				
				if(strtolower($type) == "interview"){
					if(strtolower($type_status) == "in progress"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Invitation to ".$name." for ".$result->JobTitleName." ".$result->name." in Astra Credit Companies";
						$message = view('emails.undangan-interview', $data);
						\Mail::send('emails.undangan-interview', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					} else if(strtolower($type_status) == "failed"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Your ".$name." Result for ".$result->JobTitleName." ".$result->name." at Astra Credit Companies";
						$message = view('emails.failed-interview', $data);
						\Mail::send('emails.failed-interview', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					}					
				} else if(strtolower($type) == "psycho test"){
					if(strtolower($type_status) == "in progress"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Invitation to ".$name." for ".$result->JobTitleName." ".$result->name." in Astra Credit Companies";
						$message = view('emails.undangan-psikotes', $data);
						\Mail::send('emails.undangan-psikotes', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					} else if(strtolower($type_status) == "failed"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Your ".$name." Result for ".$result->JobTitleName." ".$result->name." at Astra Credit Companies";
						$message = view('emails.failed-psikotes', $data);
						\Mail::send('emails.failed-psikotes', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					}
				} else if(strtolower($type) == "medical check up"){
					if(strtolower($type_status) == "in progress"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Medical Check Up & Administrative Process for ".$result->JobTitleName." ".$result->name." at Astra Credit Companies";
						$message = view('emails.undangan-mcu', $data);
						\Mail::send('emails.undangan-mcu', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					} else if(strtolower($type_status) == "failed"){
						$data['subject'] = "[ " .$this->config['cnf_appname']." ] Your Medical Check Up Result for  ".$result->JobTitleName." ".$result->name." at Astra Credit Companies";
						$message = view('emails.failed-mcu', $data);
						\Mail::send('emails.failed-mcu', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					}
				}

			\Log::info('email prescreening success: '.$type.'-'.$type_status.' to '.$data['email']);
			} 
			return true;
	}
	
	public function upsertUniv(Request $request){
		$user = $request->getUser();
		$pass = $request->getPassword();
		$data = $request->data;
		
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			
			foreach($data as $element){
				$idUniv = $element['id'];
				$nameUniv = $element['name'];	
				
				$checkUniv = \DB::table('acc_universityorschool')->where('id',$idUniv)->first();
				if(count($checkUniv) > 0){
					try{
						\DB::table('acc_universityorschool')->where('id',$idUniv)->update(['name' => $nameUniv]);
					} catch(\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						$response = [
							'success' => false,
							'message' => "Exception query",
						];
					}
				} else {
					try{
						\DB::table('acc_universityorschool')->insert(['id' => $idUniv, 'name' => $nameUniv]);
					} catch(\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
						$response = [
							'success' => false,
							'message' => "Exception query",
						];
					}	
				}
				$response = [
					'success' => true,
					'message' => "Data saved",
				];
			}
		} else {
				$response = [
					'success' => false,
					'message' => "Permission denied",
				];
		}
		
		return response()->json($response, 200);
	}

	public function sendSubsVacancy(Request $request){
		$user = $request->getUser();
		$pass = $request->getPassword();
		
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			$arrProfile = [];
			$queryUser = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
									->select('tb_users.id as id_tb_users','tb_users.username','tb_users.first_name','tb_users.last_name','tb_users.email','acc_candidate_profile.Careerpreference','acc_candidate_profile.id as id_candidate')
									->where('tb_users.subscribe','subscribe')->whereNotNull('acc_candidate_profile.Careerpreference')
									->whereNull('acc_candidate_profile.ApplicantStatus')
									->get();
			
			foreach($queryUser as $item){
				$arrJob = [];	
				$data = array();
				$arrExplode = explode(', ', $item->Careerpreference);
				$queryJob = \DB::table('acc_job')->leftJoin('acc_placement','acc_placement.id','=','acc_job.Location')->where('acc_job.StatusJob','active')
				->whereIn('acc_job.IDJobField', $arrExplode)
				->whereDate('acc_job.StartDate', '<=', date("Y-m-d"))->whereDate('acc_job.EndDate', '>=', date("Y-m-d"))
				->whereBetween('acc_job.CreatedDate',[date("Y-m-d H:i:s", strtotime("-7 day")), date("Y-m-d H:i:s")])
				->select('acc_job.id as id_job','acc_job.JobTitleName as job_title','acc_job.IDJobField as id_jobfield','acc_placement.name as location','acc_job.MaxAge as max_age','acc_job.Degree as min_degree','acc_job.GPA as min_ipk')->get();
				foreach($queryJob as $job){
					$arrJob[] = $job;
				}
				
				$item->jobpreferences = $arrJob;
				$arrProfile[] = $item;
				
				//send email
				if(!empty($arrJob)){
					$data = array(
						'id_user' => $item->id_tb_users,
						'username' => $item->username,
						'id_candidate' => $item->id_candidate,
						'firstname' => $item->first_name,
						'lastname' => $item->last_name,
						'email' => $item->email,
						'arrayjob' => $arrJob
					);
					$data['subject'] = "[ " .$this->config['cnf_appname']." ] Astra Credit Companies is looking candidates like you!";
						$message = view('emails.subscribe-job', $data);
						\Mail::send('emails.subscribe-job', $data, function ($message) use ($data) {
							$message->to($data['email'])->subject($data['subject']);
						});
					\Log::info("Success email vacancy job to: ".$data['email']);
				}
				
			}
			
			
			$response = [
					'success' => true,
					'user' => $arrProfile,
					'message' => "Email sent",
			];
		} else {
			$response = [
					'success' => false,
					'message' => "Permission denied",
			];
		}
		
		return response()->json($response,200);
	}
	
	public function getUnsubscribe($id_user){
		$page = 'user.unsubscribe';
		$this->data['title'] = 'Unsubscribe Email';
		$queryResult = \DB::table('tb_users')->where('id',$id_user)->first();
		
		if(!empty($queryResult)){			
			try{
				\DB::table('tb_users')->where('id',$id_user)->update(['subscribe' => 'unsubscribe']);
			} catch(\Illuminate\Database\QueryException $ex){
				\Log::error($ex);
				return view($page , $this->data);
			}
		}
		
		return view($page , $this->data);
	}
	
	public function schedulerProfileIndex(Request $request){
		$user = $request->getUser();
		$pass = $request->getPassword();
		$startDate = $request->start_date;
		$endDate = $request->end_date;
		$arrProfile = [];
		$arrEduBack = [];
		$arrWorkExp = [];
		$arrOrgExp = [];
		$arrFileAttach = [];
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			try{
				\Log::info('-------- Start Scheduler --------');
				$client = new \GuzzleHttp\Client();
				$this->schHitMobileApplicant($startDate, $endDate, $client);
				$this->schHitMobileEduBack($startDate, $endDate, $client);
				$this->schHitMobileWorkExp($startDate, $endDate, $client);
				$this->schHitMobileOrgExp($startDate, $endDate, $client);
				$requestJson = $client->post(\Lang::get('acc.loginsf'));
				$responseJson = $requestJson->getBody()->getContents();
				$resultJson = json_decode($responseJson,true);
				
				if($requestJson->getStatusCode() == "200"){
					$token = $resultJson['access_token'];
					$arrProfile = $this->schHitApplicant($startDate, $endDate, $client, $token);
					$arrEduBack = $this->schHitEduBack($startDate, $endDate, $client, $token);
					$arrWorkExp = $this->schHitWorkExp($startDate, $endDate, $client, $token);
					$arrOrgExp = $this->schHitOrgExp($startDate, $endDate, $client, $token);
					$arrFileAttach = $this->schHitFileAttach($startDate, $endDate, $client, $token);
				
				\Log::info('-------- End Scheduler --------');
				$response = [
						'success' => true,
						'message' => "Sync success",
						'profile' => $arrProfile,
						'educational_background' => $arrEduBack,
						'working_experience' => $arrWorkExp,
						'organizational_experience' => $arrOrgExp,
						'file_attachment' => $arrFileAttach
					];
				} else{
					$response = [
						'success' => false,
						'message' => "Connection error",
					];
				}
			} catch(Exception $e){
				\Log::error($e);
				$response = [
						'success' => false,
						'message' => "Sync failed",
					];
			}
		} else {
				$response = [
					'success' => false,
					'message' => "Permission denied",
				];
		}
		return response()->json($response,200);
	}
	
	public function schHitApplicant($startDate, $endDate, $client, $token){
		$resultJson2['results'] = [];
		$resultQuery = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
			->select('tb_users.id','acc_candidate_profile.address','acc_candidate_profile.cityofbirth','acc_candidate_profile.cityorregency',
			'acc_candidate_profile.country', 'acc_candidate_profile.dateofbirth','acc_candidate_profile.subdisctrict','tb_users.email','tb_users.first_name',
			'acc_candidate_profile.gender','acc_candidate_profile.homephone','tb_users.last_name','tb_users.avatar','acc_candidate_profile.maritalstatus',
			'acc_candidate_profile.mobilephone','acc_candidate_profile.identitycardnumber','acc_candidate_profile.provinceofbirth',
			'acc_candidate_profile.province','acc_candidate_profile.religion','acc_candidate_profile.title_profile','acc_candidate_profile.postalcode',
			'acc_candidate_profile.npwp','acc_candidate_profile.marrieddate')
			->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_candidate_profile.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_candidate_profile.updated_date)'), [$startDate, $endDate])->orWhereBetween(\DB::raw('DATE(tb_users.updated_at)'), [$startDate, $endDate]); })
			->whereNotNull('acc_candidate_profile.idExternal')->get();
			
			if(count($resultQuery) > 0){
				$res = [];
				$data = array();
				$post_data = array();
				foreach($resultQuery as $items){
					if(!empty($items->id)){ $data['WebCareer_ID__c'] = $items->id; }
					if(!empty($items->address)){ $data['Address__c'] = $items->address; } else { $data['Address__c'] = null; }
					if(!empty($items->cityofbirth)){ $data['City_of_Birth__c'] = $items->cityofbirth; } else { $data['City_of_Birth__c'] = null; }
					if(!empty($items->cityorregency)){ $data['City__c'] = $items->cityorregency; } else { $data['City__c'] = null; }
					if(!empty($items->country)){ $data['Country__c'] = $items->country; } else { $data['Country__c'] = null; }
					if(!empty($items->dateofbirth)){ 
						if($items->dateofbirth != "0000-00-00"){
							$data['Date_of_Birth__c'] = $items->dateofbirth; 
						} else { 
							$data['Date_of_Birth__c'] = null; }
					} else { $data['Date_of_Birth__c'] = null; }
					if(!empty($items->subdisctrict)){ $data['District__c'] = $items->subdisctrict; } else { $data['District__c'] = null; }
					if(!empty($items->email)){ $data['Email_Address__c'] = $items->email; } else { $data['Email_Address__c'] = null; }
					if(!empty($items->first_name)){ $data['First_Name__c'] = $items->first_name; } else { $data['First_Name__c'] = null; }
					if(!empty($items->gender)){ $data['Gender__c'] = $items->gender; } else { $data['Gender__c'] = null; }
					if(!empty($items->homephone)){ $data['Home_Phone__c'] = $items->homephone; } else { $data['Home_Phone__c'] = null; }
					if(!empty($items->last_name)){ $data['Last_Name__c'] = $items->last_name; } else { $data['Last_Name__c'] = null; }
					$url_foto = url('uploads/users');
					if(!empty($items->avatar)){ $data['Link_Photo__c'] = $url_foto.'/'.$items->avatar; } else { $data['Link_Photo__c'] = null; }
					if(!empty($items->npwp)){ $data['NPWP__c'] = $items->npwp; } else { $data['NPWP__c'] = null; }
					if(!empty($items->maritalstatus)){ $data['Marital_Status__c'] = $items->maritalstatus; } else { $data['Marital_Status__c'] = null; }
					if(!empty($items->marrieddate)){ 
						if($items->marrieddate != "0000-00-00"){
							$data['Married_Date__c'] = $items->marrieddate; 
						} else { 
							$data['Married_Date__c'] = null; } 
					} else { $data['Married_Date__c'] = null; }
					if(!empty($items->mobilephone)){ $data['Mobile_Phone__c'] = $items->mobilephone; } else { $data['Mobile_Phone__c'] = null; }
					if(!empty($items->first_name) &&  !empty($items->last_name)){ $data['Name'] = $items->first_name.' '.$items->last_name;  } else { $data['Name'] = null; }
					if(!empty($items->identitycardnumber)){ $data['National_Id__c'] = $items->identitycardnumber; } else { $data['National_Id__c'] = null; }
					if(!empty($items->provinceofbirth)){ $data['Province_of_Birth__c'] = $items->provinceofbirth; } else { $data['Province_of_Birth__c'] = null; }
					if(!empty($items->province)){ $data['Province__c'] = $items->province; } else { $data['Province__c'] = null; }
					if(!empty($items->religion)){ $data['Religion__c'] = $items->religion; } else { $data['Religion__c'] = null; }
					if(!empty($items->title_profile)){ $data['Title__c'] = $items->title_profile; } else { $data['Title__c'] = null; }
					if(!empty($items->postalcode)){ $data['Zip_Code__c'] = $items->postalcode; } else { $data['Zip_Code__c'] = null; }
					$res[] = $data;
				}
				$post_data = array('items' => $res);
				if($post_data){
				\Log::info("Get Profile");
				\Log::info(print_r($post_data,true)); }
				
				$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postapplicant'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			} catch(Exception $e){
				\Log::error($e);
				}	
			}
		return $resultJson2['results'];
	}
	
	public function schHitEduBack($startDate, $endDate, $client, $token){
		$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_educational_background')
								->select('acc_educational_background.id_edu_back','acc_candidate_profile.idExternal','acc_educational_background.lasteducation',
									'acc_educational_background.startdate', 'acc_educational_background.endate','acc_universityorschool.id as idUni', 
									'acc_educational_background.gpa','acc_educational_background.major','acc_educational_background.faculty',
									'acc_educational_background.otherunivorschool','acc_universityorschool.name')
								->leftJoin('acc_candidate_profile', 'acc_educational_background.id_user','=','acc_candidate_profile.id_user')
								->leftjoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_educational_background.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_educational_background.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			$post_data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_edu_back)){ $data['WebCareer_ID__c'] = $items->id_edu_back; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->lasteducation)){ 
					switch($items->lasteducation){
						case "SMU": $data['Degree__c'] = "Senior High School (SMU)"; break;
						case "D1": $data['Degree__c'] = "Diploma 1 (D1)"; break;
						case "D2": $data['Degree__c'] = "Diploma 2 (D2)"; break;
						case "D3": $data['Degree__c'] = "Asociate Degree 3 (D3)"; break;
						case "D4": $data['Degree__c'] = "Diploma 4 (D4)"; break;
						case "S1": $data['Degree__c'] = "Bachelor Degree (S1)"; break;
						case "S2": $data['Degree__c'] = "Master Degree (S2)"; break;
						case "S3": $data['Degree__c'] = "Doctor (S3)"; break;
						default: unset($data['Degree__c']);
					}
				}
				if(!empty($items->gpa)){ $data['GPA__c'] = $items->gpa;} else { $data['GPA__c'] = null; }
				if(!empty($items->major)){ $data['Major__c'] = $items->major;} else { $data['Major__c'] = null; }
				if(!empty($items->faculty)){ $data['Faculty__c'] = $items->faculty;} else { $data['Faculty__c'] = null; }
				if(!empty($items->startdate)){ 
					if($items->startdate != "0000-00-00"){
							$data['From_Year__c'] = $items->startdate; 
						} else { 
							$data['From_Year__c'] = null; } 
				} else { $data['From_Year__c'] = null; }
				if(!empty($items->endate)){ 
					if($items->endate != "0000-00-00"){
						$data['To_Year__c'] = $items->endate;
					} else { 
						$data['To_Year__c'] = null; }
				} else { $data['To_Year__c'] = null; }
				if(!empty($items->idUni)){
					$data['University_Code__c'] = $items->idUni;
					if(!empty($items->name)){
						$data['University__c'] = $items->name;
					} else { $data['University__c'] = null; }
				} else { $data['University_Code__c'] = null; }
				if(!empty($items->otherunivorschool)){ $data['Others__c'] = $items->otherunivorschool;} else {  $data['Others__c'] = null; }
				$res[] = $data;
			}
			$post_data = array('items' => $res);
			if($post_data){
			\Log::info("Get Educational Background");
			\Log::info(print_r($post_data,true)); }
			
			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.posteduback'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_educational_background')->where('id_edu_back',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
				\Log::error($e);
			}
		}
		return $resultJson2['results'];
	}
	
	public function schHitWorkExp($startDate, $endDate, $client, $token){
		$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_working_experience')
								->select('acc_working_experience.id_wor_exp','acc_candidate_profile.idExternal','acc_working_experience.company',
									'acc_working_experience.jobdescription', 'acc_working_experience.status_working_experience',
									'acc_working_experience.workingexperienceperiodenddate', 'acc_working_experience.workingexperienceperiodstartdate',
									'acc_working_experience.position','acc_working_experience.salary')
								->leftJoin('acc_candidate_profile', 'acc_working_experience.id_user','=','acc_candidate_profile.id_user')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_working_experience.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_working_experience.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			$post_data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_wor_exp)){ $data['WebCareer_ID__c'] = $items->id_wor_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->company)){ $data['Company__c'] = $items->company; } else { unset($data['Company__c']); }
				if(!empty($items->jobdescription)){ $data['Job_Description__c'] = $items->jobdescription; } else { $data['Job_Description__c'] = null; }
				if(!empty($items->status_working_experience)){ $data['Job_Status__c'] = $items->status_working_experience; } else { $data['Job_Status__c'] = null; }
				if(!empty($items->workingexperienceperiodstartdate)){ 
					if($items->workingexperienceperiodstartdate != "0000-00-00"){
						$data['Period_Start__c'] = $items->workingexperienceperiodstartdate; 
					} else { $data['Period_Start__c'] = null;}
				} else { $data['Period_Start__c'] = null; }
				if(!empty($items->workingexperienceperiodenddate)){ 
					if($items->workingexperienceperiodenddate != "0000-00-00"){
						$data['Period_End__c'] = $items->workingexperienceperiodenddate; 
					} else { $data['Period_End__c'] = null; }
				} else { $data['Period_End__c'] = null; }
				if(!empty($items->position)){ $data['Position__c'] = $items->position; } else { $data['Position__c'] = null; }
				if(!empty($items->salary)){ $data['Salary__c'] = $items->salary; } else { $data['Salary__c'] = null; }
				$res[] = $data;
			}
			$post_data = array('items' => $res);
			if($post_data){
			\Log::info("Get Working Experience");
			\Log::info(print_r($post_data,true)); }
			
			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postworkexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_working_experience')->where('id_wor_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return $resultJson2['results'];
	}

	public function schHitOrgExp($startDate, $endDate, $client, $token){
		$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_organizational_experience')
								->select('acc_organizational_experience.id_org_exp','acc_candidate_profile.idExternal',
								'acc_organizational_experience.organization_experience_startdate','acc_organizational_experience.organizationname',
								'acc_organizational_experience.roleorposition','acc_organizational_experience.rolefunction',
								'acc_organizational_experience.organizationscope','acc_organizational_experience.organization_experience_endate')
								->leftJoin('acc_candidate_profile', 'acc_organizational_experience.id_user','=','acc_candidate_profile.id_user')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_organizational_experience.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_organizational_experience.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			$post_data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_org_exp)){ $data['WebCareer_ID__c'] = $items->id_org_exp; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->organization_experience_startdate)){ 
					if($items->organization_experience_startdate != "0000-00-00"){
						$data['From_Year__c'] = $items->organization_experience_startdate;
					} else { $data['From_Year__c'] = null; }
				} else { $data['From_Year__c'] = null; }
				if(!empty($items->organizationname)){ $data['Name'] = $items->organizationname; } else { unset($data['Name']); }
				if(!empty($items->roleorposition)){ $data['Position__c'] = $items->roleorposition; } else { $data['Position__c'] = null; }
				if(!empty($items->rolefunction)){ $data['Role_Function__c'] = $items->rolefunction; } else { $data['Role_Function__c'] = null; }
				if(!empty($items->organizationscope)){ $data['Scope__c'] = $items->organizationscope; } else { $data['Scope__c'] = null; }
				if(!empty($items->organization_experience_endate)){ 
					if($items->organization_experience_endate != "0000-00-00"){
						$data['To_Year__c'] = $items->organization_experience_endate;
					} else { $data['To_Year__c'] = null; }
				} else { $data['To_Year__c'] = null; }
				$res[] = $data;
			}
			$post_data = array('items' => $res);
			if($post_data){
			\Log::info("Get Organizational Experience");
			\Log::info(print_r($post_data,true)); }

		  $headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.postorgexp'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_organizational_experience')->where('id_org_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
				    \Log::error($e);
			}
		}
		return $resultJson2['results'];
	}	
	
	public function schHitFileAttach($startDate, $endDate, $client, $token){
		$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_add_file_attachment')
								->select('acc_add_file_attachment.id_file','acc_candidate_profile.idExternal','acc_add_file_attachment.link_file','acc_add_file_attachment.file_name')
								->leftJoin('acc_candidate_profile', 'acc_add_file_attachment.id_user','=','acc_candidate_profile.id_user')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_add_file_attachment.created_at)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_add_file_attachment.updated_at)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			$post_data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_file)){ $data['WebCareer_ID__c'] = $items->id_file; }
				if(!empty($items->idExternal)){ $data['Applicant_Id__c'] = $items->idExternal; }
				if(!empty($items->link_file)){ $data['File_Url__c'] = $items->link_file; } else { unset($data['File_Url__c']); }
				if(!empty($items->file_name)){ $data['Title__c'] = $items->file_name; } else { unset($data['Title__c']); }
				$res[] = $data;
			}
			$post_data = array('items' => $res);
			if($post_data){
			\Log::info("Get File Attachment");
			\Log::info(print_r($post_data,true)); }

			$headers = [
						'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{
			$requestJson2 = $client->post(\Lang::get('acc.postfile'), [
					'headers' => $headers,
					'json'=>  $post_data,
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_add_file_attachment')->where('id_file',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			}
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		return $resultJson2['results'];
	}	
	
	public function schedulerDeleteProfile(Request $request){
		$user = $request->getUser();
		$pass = $request->getPassword();
		$startDate = $request->start_date;
		$endDate = $request->end_date;
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			
			$resultQuery = \DB::table('acc_delete_profile')->whereBetween(\DB::raw('DATE(delete_date)'), [$startDate,$endDate])->get();
			
			$res = [];
			$data = array();
			if(count($resultQuery) > 0){
				foreach($resultQuery as $items){
					if(!empty($items->id_profile)){ $data['id'] = $items->id_profile; } else { unset($data['id']); }
					if(!empty($items->id_external)){ $data['id_external'] = $items->id_external; } else { unset($data['id_external']); }
					if(!empty($items->type_profile)){ $data['type'] = $items->type_profile; } else { unset($data['type']); }
					if(!empty($items->delete_date)){ $data['date'] = $items->delete_date; } else { unset($data['date']); }
					$res[] = $data;
				}		
			}
				$response = [
						'success' => true,
						'data' => $res,
					];
		} else{
			$response = [
					'success' => false,
					'message' => "Permission denied",
				];
		}
		return response()->json($response,200);
	}
	
	public function schHitMobileApplicant($startDate, $endDate, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('tb_users')->leftJoin('acc_candidate_profile','tb_users.id','=','acc_candidate_profile.id_user')
			->select('tb_users.id','acc_candidate_profile.address','acc_candidate_profile.cityofbirth','acc_candidate_profile.cityorregency',
			'acc_candidate_profile.country', 'acc_candidate_profile.dateofbirth','acc_candidate_profile.subdisctrict','tb_users.email','tb_users.first_name',
			'acc_candidate_profile.gender','acc_candidate_profile.homephone','tb_users.last_name','tb_users.avatar','acc_candidate_profile.maritalstatus',
			'acc_candidate_profile.mobilephone','acc_candidate_profile.identitycardnumber','acc_candidate_profile.provinceofbirth',
			'acc_candidate_profile.province','acc_candidate_profile.religion','acc_candidate_profile.title_profile','acc_candidate_profile.postalcode',
			'acc_candidate_profile.npwp','acc_candidate_profile.marrieddate')
			->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_candidate_profile.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_candidate_profile.updated_date)'), [$startDate, $endDate])->orWhereBetween(\DB::raw('DATE(tb_users.updated_at)'), [$startDate, $endDate]); })
			->whereNotNull('acc_candidate_profile.idExternal')->get();
			
			if(count($resultQuery) > 0){
				$res = [];
				$data = array();
				foreach($resultQuery as $items){
					if(!empty($items->id)){ $data['webCareer_ID__c'] = $items->id; }
					if(!empty($items->address)){ $data['address__c'] = $items->address; } else { $data['address__c'] = null; }
					if(!empty($items->cityofbirth)){ $data['city_of_Birth__c'] = $items->cityofbirth; } else { $data['city_of_Birth__c'] = null; }
					if(!empty($items->cityorregency)){ $data['city__c'] = $items->cityorregency; } else { $data['city__c'] = null; }
					if(!empty($items->country)){ $data['country__c'] = $items->country; } else { $data['country__c'] = null; }
					if(!empty($items->dateofbirth)){ 
						if($items->dateofbirth != "0000-00-00"){
							$data['date_of_Birth__c'] = $items->dateofbirth;
						} else { 
							$data['date_of_Birth__c'] = null;}
					} else { $data['date_of_Birth__c'] = null; }
					if(!empty($items->subdisctrict)){ $data['district__c'] = $items->subdisctrict; } else { $data['district__c'] = null; }
					if(!empty($items->email)){ $data['email_Address__c'] = $items->email; } else { $data['email_Address__c'] = null; }
					if(!empty($items->first_name)){ $data['first_Name__c'] = $items->first_name; } else { $data['first_Name__c'] = null; }
					if(!empty($items->gender)){ $data['gender__c'] = $items->gender; } else { $data['gender__c'] = null; }
					if(!empty($items->homephone)){ $data['home_Phone__c'] = $items->homephone; } else { $data['home_Phone__c'] = null; }
					if(!empty($items->last_name)){ $data['last_Name__c'] = $items->last_name; } else { $data['last_Name__c'] = null; }
					$url_foto = url('uploads/users');
					if(!empty($items->avatar)){ $data['photo__c'] = $url_foto.'/'.$items->avatar; } else { $data['photo__c'] = null; }
					if(!empty($items->npwp)){ $data['npwp__c'] = $items->npwp; } else { $data['npwp__c'] = null; }
					if(!empty($items->maritalstatus)){ $data['marital_Status__c'] = $items->maritalstatus; } else { $data['marital_Status__c'] = null; }
					if(!empty($items->marrieddate)){ 
						if($items->marrieddate != "0000-00-00"){
							$data['married_Date__c'] = $items->marrieddate; 
						} else { 
							$data['married_Date__c'] = null; } 
					} else { $data['married_Date__c'] = null; }
					if(!empty($items->mobilephone)){ $data['mobile_Phone__c'] = $items->mobilephone; } else { $data['mobile_Phone__c'] = null; }
					if(!empty($items->first_name) &&  !empty($items->last_name)){ $data['name'] = $items->first_name.' '.$items->last_name;  } else { $data['name'] = null; }
					if(!empty($items->identitycardnumber)){ $data['national_Id__c'] = $items->identitycardnumber; } else { $data['national_Id__c'] = null; }
					if(!empty($items->provinceofbirth)){ $data['province_of_Birth__c'] = $items->provinceofbirth; } else { $data['province_of_Birth__c'] = null; }
					if(!empty($items->province)){ $data['province__c'] = $items->province; } else { $data['province__c'] = null; }
					if(!empty($items->religion)){ $data['religion__c'] = $items->religion; } else { $data['religion__c'] = null; }
					if(!empty($items->title_profile)){ $data['title__c'] = $items->title_profile; } else { $data['title__c'] = null; }
					if(!empty($items->postalcode)){ $data['zip_Code__c'] = $items->postalcode; } else { $data['zip_Code__c'] = null; }
					$res[] = $data;
				}
				if($res){
				\Log::info("Get Profile for Mobile");
				\Log::info(print_r($res,true)); }
				
				$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
		 try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postapplicant'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			} catch(Exception $e){
				\Log::error($e);
				}
			} 
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileEduBack($startDate, $endDate, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_educational_background')
								->select('acc_educational_background.id_edu_back','acc_candidate_profile.id_user','acc_educational_background.lasteducation',
									'acc_educational_background.startdate', 'acc_educational_background.endate','acc_universityorschool.id as idUni', 
									'acc_educational_background.gpa','acc_educational_background.major','acc_educational_background.faculty',
									'acc_educational_background.otherunivorschool','acc_universityorschool.name')
								->leftJoin('acc_candidate_profile', 'acc_educational_background.id_user','=','acc_candidate_profile.id_user')
								->leftjoin('acc_universityorschool','acc_educational_background.universityorschool','=','acc_universityorschool.id')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_educational_background.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_educational_background.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_edu_back)){ $data['webCareer_ID__c'] = $items->id_edu_back; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->lasteducation)){ 
					switch($items->lasteducation){
						case "SMU": $data['degree__c'] = "Senior High School (SMU)"; break;
						case "D1": $data['degree__c'] = "Diploma 1 (D1)"; break;
						case "D2": $data['degree__c'] = "Diploma 2 (D2)"; break;
						case "D3": $data['degree__c'] = "Asociate Degree 3 (D3)"; break;
						case "D4": $data['degree__c'] = "Diploma 4 (D4)"; break;
						case "S1": $data['degree__c'] = "Bachelor Degree (S1)"; break;
						case "S2": $data['degree__c'] = "Master Degree (S2)"; break;
						case "S3": $data['degree__c'] = "Doctor (S3)"; break;
						default: unset($data['degree__c']);
					}
				}
				if(!empty($items->gpa)){ $data['gpa__c'] = $items->gpa;} else { $data['gpa__c'] = null; }
				if(!empty($items->major)){ $data['major__c'] = $items->major;} else { $data['major__c'] = null; }
				if(!empty($items->faculty)){ $data['faculty__c'] = $items->faculty;} else { $data['faculty__c'] = null; }
				if(!empty($items->startdate)){ 
					if($items->startdate != "0000-00-00"){
							$data['from_Year__c'] = $items->startdate; 
						} else { $data['from_Year__c'] = null; } 
				} else { $data['from_Year__c'] = null; }
				if(!empty($items->endate)){ 
					if($items->endate != "0000-00-00"){
						$data['to_Year__c'] = $items->endate;
					} else { $data['to_Year__c'] = null; }
				} else { $data['to_Year__c'] = null; }
				if(!empty($items->idUni)){
					$data['university_Code__c'] = $items->idUni;
					if(!empty($items->name)){
						$data['university__c'] = $items->name;
					} else { $data['university__c'] = null; }
				} else { $data['university_Code__c'] = null; }
				if(!empty($items->otherunivorschool)){ $data['Others__c'] = $items->otherunivorschool;} else { $data['Others__c'] = null; }
				$res[] = $data;
			}

			if($res){
			\Log::info("Get Educational Background for Mobile");
			\Log::info(print_r($res,true)); }
			
			$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_posteduback'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_educational_background')->where('id_edu_back',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
				\Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileWorkExp($startDate, $endDate, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_working_experience')
								->select('acc_working_experience.id_wor_exp','acc_candidate_profile.id_user','acc_working_experience.company',
									'acc_working_experience.jobdescription', 'acc_working_experience.status_working_experience',
									'acc_working_experience.workingexperienceperiodenddate', 'acc_working_experience.workingexperienceperiodstartdate',
									'acc_working_experience.position','acc_working_experience.salary')
								->leftJoin('acc_candidate_profile', 'acc_working_experience.id_user','=','acc_candidate_profile.id_user')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_working_experience.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_working_experience.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_wor_exp)){ $data['webCareer_ID__c'] = $items->id_wor_exp; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->company)){ $data['company__c'] = $items->company; } else { unset($data['company__c']); }
				if(!empty($items->jobdescription)){ $data['job_Description__c'] = $items->jobdescription; } else { $data['job_Description__c'] = null; }
				if(!empty($items->status_working_experience)){ $data['job_Status__c'] = $items->status_working_experience; } else { $data['job_Status__c'] = null; }
				if(!empty($items->workingexperienceperiodstartdate)){ 
					if($items->workingexperienceperiodstartdate != "0000-00-00"){
						$data['period_Start__c'] = $items->workingexperienceperiodstartdate; 
					} else { $data['period_Start__c'] = null;}
				} else { $data['period_Start__c'] = null; }
				if(!empty($items->workingexperienceperiodenddate)){ 
					if($items->workingexperienceperiodenddate != "0000-00-00"){
						$data['period_End__c'] = $items->workingexperienceperiodenddate; 
					} else { $data['period_End__c'] = null; }
				} else { $data['period_End__c'] = null; }
				if(!empty($items->position)){ $data['position__c'] = $items->position; } else { $data['position__c'] = null; }
				if(!empty($items->salary)){ $data['salary__c'] = $items->salary; } else { $data['salary__c'] = null; }
				$res[] = $data;
			}
			if($res){
			\Log::info("Get Working Experience for Mobile");
			\Log::info(print_r($res,true)); }
			
			$headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postworkexp'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_working_experience')->where('id_wor_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
						\Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}
	
	public function schHitMobileOrgExp($startDate, $endDate, $client){
		//$resultJson2['results'] = [];
		$resultQuery = \DB::table('acc_organizational_experience')
								->select('acc_organizational_experience.id_org_exp','acc_candidate_profile.id_user',
								'acc_organizational_experience.organization_experience_startdate','acc_organizational_experience.organizationname',
								'acc_organizational_experience.roleorposition','acc_organizational_experience.rolefunction',
								'acc_organizational_experience.organizationscope','acc_organizational_experience.organization_experience_endate')
								->leftJoin('acc_candidate_profile', 'acc_organizational_experience.id_user','=','acc_candidate_profile.id_user')
								->where(function($q) use($startDate, $endDate) { $q->whereBetween(\DB::raw('DATE(acc_organizational_experience.created_date)'),[$startDate,$endDate])->orWhereBetween(\DB::raw('DATE(acc_organizational_experience.updated_date)'), [$startDate, $endDate]); })
								->whereNotNull('acc_candidate_profile.idExternal')->get();
		
		if(count($resultQuery) > 0){
			$res = [];
			$data = array();
			foreach($resultQuery as $items){
				if(!empty($items->id_org_exp)){ $data['webCareer_ID__c'] = $items->id_org_exp; }
				if(!empty($items->id_user)){ $data['webCareer_Applicant_Id'] = $items->id_user; }
				if(!empty($items->organization_experience_startdate)){ 
					if($items->organization_experience_startdate != "0000-00-00"){
						$data['from_Year__c'] = $items->organization_experience_startdate;
					} else { $data['from_Year__c'] = null; }
				} else { $data['from_Year__c'] = null; }
				if(!empty($items->organizationname)){ $data['name'] = $items->organizationname; } else { unset($data['name']); }
				if(!empty($items->roleorposition)){ $data['position__c'] = $items->roleorposition; } else { $data['position__c'] = null; }
				if(!empty($items->rolefunction)){ $data['role_Function__c'] = $items->rolefunction; } else { $data['role_Function__c'] = null; }
				if(!empty($items->organizationscope)){ $data['scope__c'] = $items->organizationscope; } else { $data['scope__c'] = null; }
				if(!empty($items->organization_experience_endate)){ 
					if($items->organization_experience_endate != "0000-00-00"){
						$data['to_Year__c'] = $items->organization_experience_endate;
					} else { $data['to_Year__c'] = null; }
				} else { $data['to_Year__c'] = null; }
				$res[] = $data;
			}
			
			if($res){
			\Log::info("Get Organizational Experience");
			\Log::info(print_r($res,true)); }

		  $headers = [
						//'Authorization' => 'Bearer ' . $token,        
						'Content-Type'        => 'application/json',
						];
			try{			
			$requestJson2 = $client->post(\Lang::get('acc.mobile_postorgexp'), [
					'headers' => $headers,
					'json'=>  $res,
					'auth'=> [\Lang::get('acc.mobile_username_api'),\Lang::get('acc.mobile_password_api')],
			]);
			
			$responseJson2 = $requestJson2->getBody()->getContents();
			$resultJson2 = json_decode($responseJson2,true);
			
			/*if($requestJson2->getStatusCode() == "200"){
				$tempRes = $resultJson2['results'];
				foreach($tempRes as $itemRes){
					try{
					\DB::table('acc_organizational_experience')->where('id_org_exp',$itemRes['WebCareer_ID__c'])->update(['idExternal' => $itemRes['Id']]);
					} catch (\Illuminate\Database\QueryException $ex){
						\Log::error($ex);
					}
				}
			} */
			} catch(Exception $e){
				    \Log::error($e);
			}
		}
		//return $resultJson2['results'];
		return true;
	}		
	
}