<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Libary\SiteHelpers;
use Socialize;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Redirect ;
use App\Models\Candidateprofile; //candidateprofile
use App\Models\fileattach;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class UserController extends Controller {
    
    
    protected $layout = "layouts.main";
    
    public function __construct() {
        parent::__construct();
        $this->data = array();
        
    }
    
    public function getRegister() {
        
        if(config('sximo.cnf_regist') =='false') :
        if(\Auth::check()):
        return redirect('')->with(['message'=>'Youre already login','status'=>'error']);
        else:
        return redirect('user/login');
        endif;
        else :
        $this->data['socialize'] =  config('services');
        return view('user.register', $this->data);
        endif ;
    }
    public function postCreate( Request $request) {
        
        $rules = array(
            'username'=>'required|alpha_num|between:3,12|unique:tb_users',
            'firstname'=>'required|regex:/^[\pL\s\-]+$/u|min:2',
            'lastname'=>'required|regex:/^[\pL\s\-]+$/u|min:2',
            'email'=>'required|email|unique:tb_users',
            'password'=>'required|between:6,12|confirmed',
            'password_confirmation'=>'required|between:6,12'
        );
        if(config('sximo.cnf_recaptcha') =='true') {
		$token = $request['g-recaptcha-response'];
		if($token){
			$client = new \GuzzleHttp\Client();
			$response = $client->post('https://www.google.com/recaptcha/api/siteverify?secret='.config('sximo.cnf_recaptchaprivatekey').'&response='.$token);
			$results = json_decode($response->getBody()->getContents(),true);
			if($results['success']){
			   \Log::info('Recaptcha success');
			} else {
			   \Log::info('Recaptcha error code');
			    foreach( $results['error-codes'] as $err){
			   \Log::info($err);
			   }
			   return redirect('user/register')->withErrors('Error ReCaptcha. Please try again.')->withInput();
			}
		} else {
			\Log::info('Invalid ReCaptcha');
			return redirect('user/register')->withErrors('Invalid ReCaptcha')->withInput();
		}
        }
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $code = rand(10000,10000000);
            $authen = new User;
            $authen->username = $request->input('username');
            $authen->first_name = $request->input('firstname');
            $authen->last_name = $request->input('lastname');
            $authen->email = trim($request->input('email'));
            $authen->activation = $code;
            $authen->group_id = $this->config['cnf_group'];
            $authen->password = \Hash::make($request->input('password'));
            if($this->config['cnf_activation'] == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
            $authen->save();
            
			/*$id =\DB::table('tb_users')->where('email', $authen->email)->value('id');
			\DB::table('acc_candidate_profile')->insert(['id_user' => $id]);
			\DB::table('acc_educational_background')->insert(['id_user' => $id]);
			\DB::table('acc_working_experience')->insert(['id_user' => $id]);
			\DB::table('acc_file_attachment')->insert(['id_user' => $id]);*/
            
            $data = array(
		'username' 	=> $request->input('username'),
                'firstname'	=> $request->input('firstname') ,
                'lastname'	=> $request->input('lastname') ,
                'email'		=> $request->input('email') ,
                'password'	=> $request->input('password') ,
                'code'		=> $code ,
                'subject'	=> "[ " .$this->config['cnf_appname']." ] REGISTRATION "
            );
            if(config('sximo.cnf_activation') == 'confirmation')
            {
                $to = $request->input('email');
                $subject = "[ " .$this->config['cnf_appname']." ] REGISTRATION ";
                if($this->config['cnf_mail'] =='swift')
                {
                    \Mail::send('user.emails.registration', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                }  else {
                    $message = view('user.emails.registration', $data);
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: '.$this->config['cnf_appname'].' <'.$this->config['cnf_email'].'>' . "\r\n";
                    //mail($to, $subject, $message, $headers);
                    \Mail::send('user.emails.registration', $data, function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                }
                
                $message = "Thanks for registering! . Please check your inbox and follow activation link";
                
            } elseif($this->config['cnf_activation']=='manual') {
                $message = "Thanks for registering! . We will validate you account before your account active";
            } else {
                $message = "Thanks for registering! . Your account is active now ";
                
            }
            
            
            return redirect('user/login')->with(['message' => $message,'status'=>'success']);
        } else {
            return redirect('user/register')->with(['message'=>'The following errors occurred','status'=>'success'])
            ->withErrors($validator)->withInput();
        }
    }
    
    public function getActivation( Request $request  )
    {
        $num = $request->input('code');
        if($num =='')
            return redirect('user/login')->with(['message'=>'Invalid Code Activation!','status'=>'error']);
            
            $user =  User::where('activation','=',$num)->get();
            if (count($user) >=1)
            {
                \DB::table('tb_users')->where('activation', $num )->update(array('active' => 1,'activation'=>''));
                return redirect('user/login')->with(['message'=>'Your account is active now!','status'=>'success']);
                
            } else {
                return redirect('user/login')->with(['message'=>'Invalid Code Activation!','status'=>'error']);
            }
            
            
            
    }
    
    public function getLogin() {
        
        if(\Auth::check())
        {
            return redirect('')->with(['message'=>'success','Youre already login','status'=>'success']);
            
        } else {
            $this->data['socialize'] =  config('services');
            return View('user.login',$this->data);
            
        }
    }
    
    public function reCaptcha( $request)
    {
        /*if(!is_null($request['g-recaptcha-response']))
        {
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . config('sximo.cnf_recaptchaprivatekey') . '&response='.$request['g-recaptcha-response'];
            $response = @file_get_contents($api_url);
            $data = json_decode($response, true);
            
            return $data;
        }
        else
        {
            return false ;
        }*/
    }
    
    public function postSignin( Request $request) {
        
        $rules = array(
            'email'=>'required',
            'password'=>'required',
        );

		$vEmail = $request->input('email');
    $redirect = $request->input('redirect');
		$checkEmail = \DB::table('tb_users')->where('username', $vEmail)->orWhere('email',$vEmail)->first();

		if(!empty($checkEmail)){
			$checkLogin = \DB::table('acc_login_attempt')->where('user',$checkEmail->id)->first();
			if(!empty($checkLogin)){
				$attempts = config('sximo.limit_login_attempts');
				$time_att = config('sximo.limit_login_time');
				$date1 = Carbon::parse($checkLogin->login_time);
				$now = Carbon::now();
				$length = $date1->diffInMinutes($now);
				if($checkLogin->login_attempts >= $attempts){
						if($length <= $time_att){
							return response()->json(['status' => 'error', 'message' => 'You failed '. $attempts .' times. Please wait for '. $time_att .' minutes to try again. ' ]);
						} else {
							\DB::table('acc_login_attempt')->where('user',$checkEmail->id)->delete();
						}
				} else {
					if($length > $time_att){
						\DB::table('acc_login_attempt')->where('user',$checkEmail->id)->delete();
					}
				}
			}
		}

        if(config('sximo.cnf_recaptcha') =='true') {
		$token = $request['g-recaptcha-response'];
		if($token){
			$client = new \GuzzleHttp\Client();
			$response = $client->post('https://www.google.com/recaptcha/api/siteverify?secret='.config('sximo.cnf_recaptchaprivatekey').'&response='.$token);
			$results = json_decode($response->getBody()->getContents(),true);
			if($results['success']){
			   \Log::info('Recaptcha success');
			} else {
			   \Log::info('Recaptcha error code');
			    foreach( $results['error-codes'] as $err){
			   \Log::info($err);
			   }
			   return response()->json(['status' => 'error', 'message' => 'Error ReCaptcha. Please try again.']);
			}
		} else {
			\Log::info('Invalid Captcha');
			return response()->json(['status' => 'error', 'message' => 'Invalid ReCaptcha']);

		}
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            
            //$remember = (!is_null($request->get('remember')) ? 'true' : 'false' );
	    $remember = false;
            
            if (\Auth::attempt(array('email'=>$request->input('email'), 'password'=> $request->input('password') ), $remember )
                or
                \Auth::attempt(array('username'=>$request->input('email'), 'password'=> $request->input('password') ), $remember )
                
                ) {
                    if(\Auth::check())
                    {
                        $row = User::find(\Auth::user()->id);
                        if($row->active =='0')
                        {
                            // inactive
                            if($request->ajax() == true )
                            {
								\Auth::logout();
                                return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
                            } else {
                                \Auth::logout();
                                return redirect('user/login')->with(['status' => 'error', 'message' => 'Your Account is not active']);
                            }
                            
                        } else if($row->active=='2')
                        {
                            
                            if($request->ajax() == true )
                            {
								\Auth::logout();
                                return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
                            } else {
                                // BLocked users
                                \Auth::logout();
                                return redirect('user/login')->with(['status' => 'error', 'message' => 'Your Account is BLocked']);
                            }
                        } else {
                            \DB::table('tb_users')->where('id', '=',$row->id )->update(array('last_login' => date("Y-m-d H:i:s")));
			    $checkAfter = \DB::table('acc_login_attempt')->where('user', $row->id)->first();
				if(!empty($checkAfter)){
					\DB::table('acc_login_attempt')->where('user', $row->id)->delete();
				}
                            $level = 99;
                            $sql = \DB::table('tb_groups')->where('group_id' , $row->group_id )->get();
                            if(count($sql))
                            {
                                $l = $sql[0];
                                $level = $l->level ;
                            }
                            
                            $session = array(
                                'gid' => $row->group_id,
                                'uid' => $row->id,
                                'eid' => $row->email,
                                'll' => $row->last_login,
                                'fid' =>  $row->first_name.' '. $row->last_name,
                                'username' =>  $row->username ,
                                'join'	=>  $row->created_at ,
                                'level'	=> $level
                            );
                            /* Set Lang if available */
                            if(!is_null($request->input('language')))
                            {
                                $session['lang'] = $request->input('language');
                            } else {
                                $session['lang'] = config('sximo.cnf_lang');
                                
                            }
                            
                            
                            session($session);
                            if($request->ajax() == true )
                            {
                                if(!empty($redirect)) { 
                                   if( config('sximo.cnf_front') =='false') :
                                  return response()->json(['status' => 'success', 'url' => url('dashboard')]);
                                  else :
                                  return response()->json(['status' => 'success', 'url' => url($redirect)]);
                                  endif;
                                }else{
                                   if( config('sximo.cnf_front') =='false') :
                                    return response()->json(['status' => 'success', 'url' => url('dashboard')]);
                                    else :
                                    return response()->json(['status' => 'success', 'url' => url('')]);
                                    endif;
                                }
                               
                                
                            }
                            else {
                                if( config('sximo.cnf_front') =='false') :
                                return redirect('dashboard');
                                else :
                                return redirect('');
                                endif;
                            }
                        }
                    }
                    
                }
                else {
                    
                    if($request->ajax() == true )
                    {
						if(!empty($checkEmail)){
							$checkLogin2 = \DB::table('acc_login_attempt')->where('user',$checkEmail->id)->first();
							if(!empty($checkLogin2)){
								$addAttempts = $checkLogin2->login_attempts + 1;
								$loginTime = date('Y-m-d H:i:s');
								\DB::table('acc_login_attempt')->where('id',$checkLogin2->id)->update(['login_attempts' => $addAttempts, 'login_time' => $loginTime]);
							} else {
								$addAttempts = '1';
								$loginTime = date('Y-m-d H:i:s');
								$userLoginAtt = $checkEmail->id;
								\DB::table('acc_login_attempt')->insert(['user' => $userLoginAtt ,'login_attempts' => $addAttempts, 'login_time' => $loginTime]);
							}
						}
                        return response()->json(['status' => 'error', 'message' => 'Your username/password combination was incorrect']);
                    } else {
						if(!empty($checkEmail)){
							$checkLogin2 = \DB::table('acc_login_attempt')->where('user',$checkEmail->id)->first();
							if(!empty($checkLogin2)){
								$addAttempts = $checkLogin2->login_attempts + 1;
								$loginTime = date('Y-m-d H:i:s');
								\DB::table('acc_login_attempt')->where('id',$checkLogin2->id)->update(['login_attempts' => $addAttempts, 'login_time' => $loginTime]);
							} else {
								$addAttempts = '1';
								$loginTime = date('Y-m-d H:i:s');
								$userLoginAtt = $checkEmail->id;
								\DB::table('acc_login_attempt')->insert(['user' => $userLoginAtt ,'login_attempts' => $addAttempts, 'login_time' => $loginTime]);
							}
						}
                        
                        return redirect('user/login')
                        ->with(['status' => 'error', 'message' => 'Your username/password combination was incorrect'])
                        ->withInput();
                    }
                }
        }
        else {
            
            if($request->ajax() == true)
            {
                return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
            } else {
                
                return redirect('user/login')
                ->with(['status' => 'error', 'message' => 'The following  errors occurred'])
                ->withErrors($validator)->withInput();
            }
            
        }
    }
    /*edit and add educational exp*/
    public function getEditEdu($id){
	
	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

          $result = \DB::table('acc_educational_background')->leftjoin('acc_universityorschool', 'acc_educational_background.universityorschool','=','acc_universityorschool.id')->where('id_edu_back',$id)->orderBy('id_edu_back','desc')->first();

        $test_name = \DB::table('acc_universityorschool')->orderBy('name','asc')->get();
               
        $this->data = array(
            'pageTitle'	=> 'Edit Educational Exp',
            'rowData'		=> $result,
            'universityorschoolname'		=> $test_name
        );

        return view('profile.edit_edu_exp' , $this->data);
    }
    
    public function getaddEdu(Request $request){
        if(!\Auth::check()) return redirect('user/login');
        
        $info =	User::find(\Auth::user()->id);
        
        $result = \DB::table('acc_educational_background')->where('id_user','=',$info->id)->orderBy('id_edu_back','desc')->first();
        
        //lookupuniversity
        $universityorschooldb = \DB::table('acc_universityorschool')->orderBy('name','asc')->get();
        
        $this->data = array(
            'pageTitle'	=> 'Add Educational Exp',
            'rowData'		=> $result,
            'universityorschool'		=> $universityorschooldb
        );
        
        return view('profile.add_edu_exp' , $this->data);
    }
    
    public function saveEditEduExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_edit = $request->input('id_edit');
        $lasteducation = $request->input('lasteducation');
        $universityorschool = $request->input('universityorschool');
        $faculty = $request->input('faculty');
        $major = $request->input('major');
        $gpa = $request->input('gpa');
        $startdate = $request->input('startdate');
        $endate = $request->input('endate');
	$otherunivorschool = $request->input('otherunivorschool');

        
        try{
            $update = [
                'id_edu_back' => $id_edit,
                'lasteducation'     => $lasteducation,
                'universityorschool'   => $universityorschool,
                'faculty'     => $faculty,
                'major'    => $major,
                'gpa'     => $gpa,
                'startdate'    => $startdate,
                'endate'    => $endate,
		'otherunivorschool' => $otherunivorschool,
		'updated_date' => date('Y-m-d H:i:s')
            ];
            
            \DB::table('acc_educational_background')->where('id_edu_back',$id_edit)->update($update);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    public function saveAddEduExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_add_edu = $request->input('id_add_edu');
        $id_user = $request->input('id_user');
        $lasteducation = $request->input('lasteducation');
        $universityorschool = $request->input('universityorschool');
        $faculty = $request->input('faculty');
        $major = $request->input('major');
        $gpa = $request->input('gpa');
        $startdate = $request->input('startdate');
        $endate = $request->input('endate');
        
        try{
            $insert = [
                'id_edu_back' => $id_add_edu,
                'id_user' => $id_user,
                'lasteducation'     => $lasteducation,
                'universityorschool'   => $universityorschool,
                'faculty'     => $faculty,
                'major'    => $major,
                'gpa'     => $gpa,
                'startdate'    => $startdate,
                'endate'    => $endate,
		'created_date' => date('Y-m-d H:i:s')
            ];
            
            \DB::table('acc_educational_background')->where('id_edu_back',$id_add_edu)->insert($insert);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    /*edit and add educational exp*/
    /*delete Educational Background*/
    public function destroyEduExp($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

        try{
		$checkId = \DB::table('acc_educational_background')->where('id_edu_back',$id)->value('idExternal');
			if(!empty($checkId)){
				$items = array(
					'id_profile'		=> $id,
					'id_external'		=> $checkId,
					'type_profile'	=> "educational_background",
					'delete_date'	=> date('Y-m-d H:i:s'),
				);
				\DB::table('acc_delete_profile')->insert($items);
			}
            \DB::table('acc_educational_background')->where('id_edu_back',$id)->delete();
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        }catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
        
    }
    /*delete Educational Background*/
    
    /*edit and add organizational exp*/
    
    public function getEditOrg($id){
	
	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

        $result = \DB::table('acc_organizational_experience')->where('id_org_exp',$id)->orderBy('id_org_exp','desc')->first();
        $this->data['rowData'] = $result;
        $this->data['org_exp'] = '0';
        $this->data['title'] = 'Edit Organization Exp';
        return view('profile.organi_exp' , $this->data);
    }
    
    public function getaddOrg(Request $request){
        if(!\Auth::check()) return redirect('user/login');
        
        $info =	User::find(\Auth::user()->id);
        
        $result = \DB::table('acc_organizational_experience')->where('id_user','=',$info->id)->orderBy('id_org_exp','desc')->first();
        $this->data['rowData'] = $result;
        $this->data['org_exp'] = '0';
        $this->data['title'] = 'Add Organization Exp';
        return view('profile.add_organi_exp' , $this->data);
    }
    
    public function saveEditOrgExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_edit = $request->input('id_edit');
        $organizationname = $request->input('organizationname');
        $organizationscope = $request->input('organizationscope');
        $organization_experience_startdate = $request->input('organization_experience_startdate');
        $organization_experience_endate = $request->input('organization_experience_endate');
        $rolefunction = $request->input('rolefunction');
        $roleorposition = $request->input('roleorposition');
        
        try{
            $update = [
                'id_org_exp' => $id_edit,
                'organizationname'     => $organizationname,
                'organizationscope'   => $organizationscope,
                'organization_experience_startdate'     => $organization_experience_startdate,
                'organization_experience_endate'    => $organization_experience_endate,
                'rolefunction'     => $rolefunction,
                'roleorposition'    => $roleorposition,
		'updated_date' => date('Y-m-d H:i:s') 
            ];
            
            \DB::table('acc_organizational_experience')->where('id_org_exp',$id_edit)->update($update);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    public function saveAddOrgExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_edit_org = $request->input('id_edit_org');
        $id_user = $request->input('id_user');
        $organizationname = $request->input('organizationname');
        $organizationscope = $request->input('organizationscope');
        $organization_experience_startdate = $request->input('organization_experience_startdate');
        $organization_experience_endate = $request->input('organization_experience_endate');
        $rolefunction = $request->input('rolefunction');
        $roleorposition = $request->input('roleorposition');
        
        try{
            $insert = [
                'id_org_exp' => $id_edit_org,
                'id_user' => $id_user,
                'organizationname'     => $organizationname,
                'organizationscope'   => $organizationscope,
                'organization_experience_startdate'     => $organization_experience_startdate,
                'organization_experience_endate'    => $organization_experience_endate,
                'rolefunction'     => $rolefunction,
                'roleorposition'    => $roleorposition,
		'created_date' => date('Y-m-d H:i:s')
            ];
            
            \DB::table('acc_organizational_experience')->where('id_org_exp',$id_edit_org)->insert($insert);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    /*edit and add organizational exp*/
    /*delete Organizational Experience*/
    public function destroyOrgExp($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');	

        try{
		$checkId = \DB::table('acc_organizational_experience')->where('id_org_exp',$id)->value('idExternal');
			if(!empty($checkId)){
				$items = array(
					'id_profile'		=> $id,
					'id_external'		=> $checkId,
					'type_profile'	=> "organizational_experience",
					'delete_date'	=> date('Y-m-d H:i:s'),
				);
				\DB::table('acc_delete_profile')->insert($items);
			}
            \DB::table('acc_organizational_experience')->where('id_org_exp',$id)->delete();
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        }catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
        
    }
    /*delete Organizational Experience*/
    
    /*edit and add Working exp*/
    
    public function getaddWork(Request $request){
        if(!\Auth::check()) return redirect('user/login');
        
        $info =	User::find(\Auth::user()->id);
        
        $result = \DB::table('acc_working_experience')->where('id_user','=',$info->id)->orderBy('id_wor_exp','desc')->first();
        $this->data['rowData'] = $result;
        $this->data['work_exp'] = '0';
        $this->data['title'] = 'Add Work Exp';
        return view('profile.add_work_exp' , $this->data);
    }
    
    public function getEditWork($id){
	
	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

        $result = \DB::table('acc_working_experience')->where('id_wor_exp',$id)->orderBy('id_wor_exp','desc')->first();
        $this->data['rowData'] = $result;
        $this->data['work_exp'] = '0';
        $this->data['title'] = 'Edit Working Exp';
        return view('profile.edit_work_exp' , $this->data);
    }
    
    public function saveEditWorkExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_edit = $request->input('id_edit');
        $company = $request->input('company');
        $workingexperienceperiodstartdate = $request->input('workingexperienceperiodstartdate');
        $workingexperienceperiodenddate = $request->input('workingexperienceperiodenddate');
        $position = $request->input('position');
        $category = $request->input('category');
        $status_working_experience = $request->input('status_working_experience');
        $jobdescription = $request->input('jobdescription');
        $salary = $request->input('salary');
        $Reasonofleaving = $request->input('Reasonofleaving');
        
        
        try{
            $update = [
                'id_wor_exp' => $id_edit,
                'company'     => $company,
                'workingexperienceperiodstartdate'   => $workingexperienceperiodstartdate,
                'workingexperienceperiodenddate'     => $workingexperienceperiodenddate,
                'position'    => $position,
                'category'     => $category,
                'status_working_experience'    => $status_working_experience,
                'jobdescription'    => $jobdescription,
                'salary'     => $salary,
                'Reasonofleaving'    => $Reasonofleaving,
		'updated_date' => date('Y-m-d H:i:s')
            ];
            
            \DB::table('acc_working_experience')->where('id_wor_exp',$id_edit)->update($update);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    public function saveAddWorkExp(Request $request ){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');
        
        $id_edit_work = $request->input('id_edit_work');
        $id_user = $request->input('id_user');
        $company = $request->input('company');
        $workingexperienceperiodstartdate = $request->input('workingexperienceperiodstartdate');
        $workingexperienceperiodenddate = $request->input('workingexperienceperiodenddate');
        $position = $request->input('position');
        $category = $request->input('category');
        $status_working_experience = $request->input('status_working_experience');
        $jobdescription = $request->input('jobdescription');
        $salary = $request->input('salary');
        $Reasonofleaving = $request->input('Reasonofleaving');
        
        try{
            $insert = [
                'id_wor_exp' => $id_edit_work,
                'id_user' => $id_user,
                'company'     => $company,
                'workingexperienceperiodstartdate'   => $workingexperienceperiodstartdate,
                'workingexperienceperiodenddate'     => $workingexperienceperiodenddate,
                'position'    => $position,
                'category'     => $category,
                'status_working_experience'    => $status_working_experience,
                'jobdescription'    => $jobdescription,
                'salary'     => $salary,
                'Reasonofleaving'    => $Reasonofleaving,
		'created_date' => date('Y-m-d H:i:s')
            ];
            
            \DB::table('acc_working_experience')->where('id_wor_exp',$id_edit_work)->insert($insert);
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
    }
    
    /*edit and add Working exp*/
    
    /*delete Working Experience*/
    public function destroyWorkExp($id){

	// Make Sure users Logged 
	if(!\Auth::check()) 
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');

        try{
			$checkId = \DB::table('acc_working_experience')->where('id_wor_exp',$id)->value('idExternal');
			if(!empty($checkId)){
				$items = array(
					'id_profile'		=> $id,
					'id_external'		=> $checkId,
					'type_profile'	=> "working_experience",
					'delete_date'	=> date('Y-m-d H:i:s'),
				);
				\DB::table('acc_delete_profile')->insert($items);
			}
            \DB::table('acc_working_experience')->where('id_wor_exp',$id)->delete();
            return redirect('user/profile/')->with('message',__('core.note_success'))->with('status','success');
        }catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
        
    }
    /*delete Working Experience*/
    

    
    /*add file attachment*/
    public function fileindex(){
        return view("profile");
    }
    
    public function filestore(Request $request){
        $info =	User::find(\Auth::user()->id);
		$first = str_replace(" ","_",$info->first_name);
		$last = str_replace(" ","_",$info->last_name);
		
		$jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];
		$tempVal=[];
		foreach($jenisFile as $itemFile){
			$tempVal[$itemFile]= 'mimes:jpeg,png,jpg,docx,doc,pdf|max:2048';
		}
		
        $validator = Validator::make($request->all(), $tempVal);
        
        if ($validator->passes()) {
			foreach($jenisFile as $fl){
            if(!is_null($request->file($fl))){
                $file = request()->file($fl);
                $getimageName = $first.'_'.$last.'_'.$fl.'_'.time().'.'.$request->$fl->getClientOriginalExtension();
                $uploadSuccess= $request->$fl->move(public_path().'/uploads/fileattachment/', $getimageName);
				$type=$fl;
				$saveurl= url('uploads/fileattachment/'. $getimageName);
				
				$check = \DB::table('acc_add_file_attachment')->where([['id_user','=',$info->id],['type_file','=',$fl]])->first();
				
				if($check){
					$tempQuery = $check->id_file;
					$tempEdit = [
						'file_name' => $getimageName,
						'updated_at' => date('Y-m-d H:i:s'),
						'link_file' =>  $saveurl
					];	
					//$removeFile = unlink(public_path().'/uploads/fileattachment/'.$check->file_name);
					try{
						\DB::table('acc_add_file_attachment')->where('id_file',$tempQuery)->update($tempEdit);
									} catch(\Illuminate\Database\QueryException $ex)	{
						\Log::info($ex);
						return redirect('user/profile')->with('message','File Attachment Gagal di upload!')->with('status','error');		
				} 
				} else {
					$tempAdd = [
						'file_name' => $getimageName,
						'id_user' => $info->id , 
						'created_at' => date('Y-m-d H:i:s'),
						'type_file' => $type,
						'link_file' =>  $saveurl
					];
					try{
					\DB::table('acc_add_file_attachment')->insert($tempAdd);
					} catch(\Illuminate\Database\QueryException $ex)	{
						\Log::info($ex);
						return redirect('user/profile')->with('message','File Attachment Gagal di upload!')->with('status','error');		
					} 
				}
            }
			}
      
            return redirect('user/profile')->with('message','File Attachment Berhasil Tersimpan!')->with('status','success');
            
        }else {
            return redirect('user/profile')->with('message','File Attachment Gagal di upload!')->with('status','error')->withErrors($validator)->withInput();
        }
    }
    /*delete File Attachment applicant menu*/
    public function destroyAppMenuFileattachprofile(Request $request, $id){
        
        $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];
        
        $checks = \DB::table('acc_add_file_attachment')->where('id_file',$id)->first();
        $tempid = $checks->id_user;
        
        if($checks){
            $tempQuery_delete = $checks->id_file;
            $tempEdit_delete = [
                'link_file' =>  '',
		'updated_at' => date('Y-m-d H:i:s')
            ];
            $removeFile = unlink(public_path().'/uploads/fileattachment/'.$checks->file_name);
            try{
                \DB::table('acc_add_file_attachment')->where('id_file',$tempQuery_delete)->update($tempEdit_delete);
                return redirect('user/profile')->with('message',__('core.note_success'))->with('status','success');
            }catch (\Illuminate\Database\QueryException $ex){
                return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
            }
        }
        
    }
    /*delete File Attachment applicant menu*/
    /*add file attachment*/
    
    
    
    public function getProfile(Request $request) {
        
        if(!\Auth::check()) return redirect('user/login');
        
        $info =	User::find(\Auth::user()->id);
        
        $applicantData = \DB::table('acc_candidate_profile')->where('id_user','=',$info->id)->first();
        $educationalbackData = \DB::table('acc_educational_background')
        ->leftjoin('acc_universityorschool', 'acc_educational_background.universityorschool','=','acc_universityorschool.id')->where('id_user','=',$info->id)->get();
        $organiexpData = \DB::table('acc_organizational_experience')->where('id_user','=',$info->id)->get();
        $workexpData = \DB::table('acc_working_experience')->where('id_user','=',$info->id)->get();
        $fileattachData = \DB::table('acc_add_file_attachment')->where('id_user','=',$info->id)->get();     
		$jobfield = \DB::table('acc_jobfield')->get();
        
        if(count($applicantData) > 0){
            $interest = $applicantData->Careerpreference;
            $showgender = $applicantData->gender;
        } else {
            $interest = '';
            $showgender = '';
        }
        $explode_career = explode(", ", $interest);        
        $explode_gender = explode(", ", $showgender);
        
        $this->data = array(
            'pageTitle'	=> 'My Profile',
            'pageNote'	=> 'View Detail My Info',
            'info'		=> $info,
            'applicantData'		=> $applicantData,
            'educationalbackData' => $educationalbackData,
            'organiexpData' => $organiexpData,
            'workexpData' => $workexpData,
            'fileattachData' => $fileattachData,
            'explode_career'=>$explode_career,
            'explode_gender' => $explode_gender,
			'jobfield'=> $jobfield,
        );
        //var_dump('ok');exit;
        
        return view('user.profile',$this->data);
    }
    
    public function postSaveprofile( Request $request)
    {
        if(!\Auth::check()) return redirect('user/login');
        $rules = array(
            'first_name'=>'required|alpha_num|min:2',
            'last_name'=>'required|alpha_num|min:2',
        );
        
        if($request->input('email') != \Session::get('eid'))
        {
            $rules['email'] = 'required|email|unique:tb_users';
        }
        
        if(!is_null($request->file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';
        
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->passes()) {
            
            
            if(!is_null($request->file('avatar')))
            {
                $file = $request->file('avatar');
                $destinationPath = './uploads/users/';
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension(); //if you need extension of the file
                $newfilename = \Session::get('uid').'.'.$extension;
                $uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);
                if( $uploadSuccess ) {
                    $data['avatar'] = $newfilename;
                }
                $orgFile = $destinationPath.'/'.$newfilename;
                \SiteHelpers::cropImage('80' , '80' , $orgFile ,  $extension,	 $orgFile)	;
                
            }
            
            $user = User::find(\Session::get('uid'));
            $user->first_name 	= $request->input('first_name');
            $user->last_name 	= $request->input('last_name');
            $user->email 		= $request->input('email');
	    $user->subscribe = $request->input('sub');
            if(isset( $data['avatar']))  $user->avatar  = $newfilename;
            $user->save();
            
            $newUser = User::find(\Session::get('uid'));
            
            \Session::put('fid',$newUser->first_name.' '.$newUser->last_name);
            
            return redirect('user/profile')->with('message','Profile has been saved!')->with('status','success');
        } else {
            return redirect('user/profile')->with('message','The following errors occurred')->with('status','error')
            ->withErrors($validator)->withInput();
        }
        
    }
    
    // 	public function postSaveFileattachment( Request $request)
    // 	{
    
    // 	    if ($validator->passes()) {
    
    
    // 	        return redirect('user/profile')->with('message','Profile has been saved!')->with('status','success');
    // 	    }
    // 	    else {
    // 	        return redirect('user/profile')->with('message','The following errors occurred')->with('status','error')
    // 	        ->withErrors($validator)->withInput();
    
    // 	    }
    
    // 	}
    
    public function postSaveApplicantData( Request $request)
    {
        if(!\Auth::check()) return redirect('user/login');
        $this->model = new candidateprofile();
        //$this->info = $this->model->makeInfo('candidateprofile');
        //$rules = $this->validateForm();
        //$validator = Validator::make($request->all(), $rules);
        //
        //if ($validator->passes()) {
        
        // $data = $this->validatePost( $request );
        //$id = $this->model->insertRow($data , $request->input('id'));
        
        $id_user = $request->input('id');
        $id_edu_background = $request->input('id_edu');
        $id_organi_exp = $request->input('id_organi');
        $id_work_exp = $request->input('id_work');
        
        //personal details
        $title_profile = $request->input('title_profile');
        $gender= $request->input('gender');
        $dateofbirth = $request->input('dateofbirth');
        $provinceofbirth = $request->input('provinceofbirth');
        $cityofbirth = $request->input('cityofbirth');
        $identitycardnumber = $request->input('identitycardnumber');
        $homephone = $request->input('homephone');
        $mobilephone = $request->input('mobilephone');
        $npwp = $request->input('npwp');
        $maritalstatus = $request->input('maritalstatus');
	$marrieddate = $request->input('marrieddate');
        $religion = $request->input('religion');
        $expectedsalaryfrom = $request->input('expectedsalaryfrom');
        $expectedsalaryupto = $request->input('expectedsalaryupto');
        $optyfrom = $request->input('optyfrom');
        $agreement = $request->input('terms');
        $careerpreference = $request->input('careerpreference');
        if(count($careerpreference) > 0){
        $imp_career = implode(", ", $careerpreference);
		} else {
			$imp_career =null;
		}

        //Home Adress        
        $country= $request->input('country');    
        $address= $request->input('address');        
        $subdisctrict= $request->input('subdisctrict');
        $cityorregency = $request->input('cityorregency');
        $province = $request->input('province');
        $postalcode = $request->input('postalcode');
        
        
        //Educational Background
        $lasteducation = $request->input('lasteducation');
        $universityorschool = $request->input('universityorschool');
        $faculty = $request->input('faculty');
        $major = $request->input('major');
        $gpa = $request->input('gpa');
        $startdate = $request->input('startdate');
        $endate = $request->input('endate');
        
        //Organizational exp
        $organizationname = $request->input('organizationname');
        $organizationscope = $request->input('organizationscope');
        $organization_experience_startdate = $request->input('organization_experience_startdate');
        $organization_experience_endate = $request->input('organization_experience_endate');
        $rolefunction = $request->input('rolefunction');
        $roleorposition = $request->input('roleorposition');
        
        //working exp
        $company = $request->input('company');
        $workingexperienceperiodstartdate = $request->input('workingexperienceperiodstartdate');
        $workingexperienceperiodenddate = $request->input('workingexperienceperiodenddate');
        $position = $request->input('position');
        $category = $request->input('category');
        $status_working_experience = $request->input('status_working_experience');
        $jobdescription = $request->input('jobdescription');
        $salary = $request->input('salary');
        $Reasonofleaving = $request->input('Reasonofleaving');
        
        //yourinterestandadditionalinformationinfo
        $careerpreference = $request->input('careerpreference');
        
        
        $expectedsalary = $request->input('expectedsalary');
        $expectedsalaryupto = $request->input('expectedsalaryupto');
        $optyfrom = $request->input('optyfrom');
        
        
        
        
        $items = array(
            'title_profile' => $title_profile,
            'gender' => $gender,
            'dateofbirth' => $dateofbirth,
            'provinceofbirth' => $provinceofbirth,
            'cityofbirth' => $cityofbirth,
            'identitycardnumber' => $identitycardnumber,
            'homephone' => $homephone,
            'mobilephone' => $mobilephone,
	    'npwp' => $npwp, 
            'maritalstatus' => $maritalstatus,
	    'marrieddate' => $marrieddate, 
            'religion' => $religion,
            'careerpreference'=> $imp_career,
            'expectedsalaryfrom'=> $expectedsalaryfrom,
            'expectedsalaryupto'=> $expectedsalaryupto,
            'optyfrom' => $optyfrom,
            'agreement' => $agreement,
            'country'=>'Indonesia',
            'address'=>$address,
            'subdisctrict'=>$subdisctrict,
            'cityorregency'=>$cityorregency,
            'province'=>$province,
            'postalcode'=>$postalcode,
	    'updated_date' => date('Y-m-d H:i:s'),
            'created_date' => date('Y-m-d H:i:s')
            
        );
        $items_edu = array(
            'lasteducation'=> $lasteducation,
            'universityorschool'=> $universityorschool,
            'faculty'=> $faculty,
            'major'=> $major,
            'gpa'=> $gpa,
            'startdate'=> $startdate,
            'endate'=> $endate
        );
        
        $items_organi = array(
            'organizationname'=> $organizationname,
            'organizationscope'=> $organizationscope,
            'organization_experience_startdate'=> $organization_experience_startdate,
            'organization_experience_endate'=> $organization_experience_endate,
            'rolefunction'=> $rolefunction,
            'roleorposition'=> $roleorposition
            
            
        );
        
        $items_working_exp = array(
            'company'=> $company,
            'workingexperienceperiodstartdate'=> $workingexperienceperiodstartdate,
            'workingexperienceperiodenddate'=> $workingexperienceperiodenddate,
            'position'=> $position,
            'category'=> $category,
            'status_working_experience'=> $status_working_experience,
            'jobdescription'=> $jobdescription,
            'salary'=> $salary,
            'Reasonofleaving'=> $Reasonofleaving
        );
		
        $info =	User::find(\Auth::user()->id);      
        
        $check = \DB::table('acc_candidate_profile')->where('id_user','=',$info->id)->first();
        
        if (!$check)
        {
            $items['id_user'] = $info->id;
            \DB::table('acc_candidate_profile')->insert($items);
        }else {
            \DB::table('acc_candidate_profile')->where('id',$id_user)->update($items);
        }
        try{
            \DB::table('acc_candidate_profile')->where('id',$id_user)->update($items);
            \DB::table('acc_educational_background')->where('id_edu_back',$id_edu_background)->update($items_edu);
            \DB::table('acc_organizational_experience')->where('id_org_exp', $id_organi_exp)->update($items_organi);
            \DB::table('acc_working_experience')->where('id_wor_exp', $id_work_exp)->update($items_working_exp);
            return redirect('user/profile')->with('message',__('core.note_success'))->with('status','success');
        } catch (\Illuminate\Database\QueryException $ex){
            return redirect('user/profile')->with('message',__('core.note_error'))->with('status','error');
        }
        
    }
    
    public function postSavepassword( Request $request)
    {
        $rules = array(
            'password'=>'required|between:6,12',
	    'old_password'=>'required',
            'password_confirmation'=>'required|between:6,12'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $user = User::find(\Session::get('uid'));
            $user->password = \Hash::make($request->input('password'));
            $user->save();
            
            return redirect('user/profile')->with(['status' => 'success', 'message' => 'Password has been saved!'] );
        } else {
            return redirect('user/profile')->with(['status' => 'error', 'message' => 'The following errors occurred'])
            ->withErrors($validator)->withInput();
        }
        
    }
    
    public function getReminder()
    {
        
        return view('user.remind');
    }
    
    public function postRequest( Request $request)
    {
        
        $rules = array(
            'credit_email'=>'required|email'
        );
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            
            $user =  User::where('email','=',$request->input('credit_email'));
            if($user->count() >=1)
            {
                $user = $user->get();
                $user = $user[0];
                $data = array('token'=>$request->input('_token'));
                $to = $request->input('credit_email');
                $subject = "[ " .config('sximo.cnf_appname')." ] REQUEST PASSWORD RESET ";
                $data['subject'] =  $subject;
                $data['email'] = $to;
		$data['firstname'] = $user->first_name;
		$data['lastname'] = $user->last_name;
                
                if(config('sximo.cnf_mail') =='swift')
                {
                    
                    \Mail::send('user.emails.auth.reminder', $data, function ($message) use ($data)  {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                        
                        
                }  else {
                    
                    
                    $message = view('user.emails.auth.reminder', $data);
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: '.config('sximo.cnf_appname').' <'.config('sximo.cnf_email').'>' . "\r\n";
                    //mail($to, $subject, $message, $headers);
                    \Mail::send('user.emails.auth.reminder', $data, function ($message) use ($data)  {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                }
                
                
                $affectedRows = User::where('email', '=',$user->email)
                ->update(array('reminder' => $request->input('_token')));
                
                return redirect('user/login')->with(['message' => 'Please check your email','status'=>'success']);
                
            } else {
                return redirect('user/login?reset')->with(['message' => 'Cant find email address','status'=>'error']);
            }
            
        }  else {
            returnredirect('user/login?reset')->with(['message' => 'The following errors occurred','status'=>'error'])->withErrors($validator)->withInput();
        }
    }
    
    public function getReset( $token = '')
    {
        if(\Auth::check()) return redirect('dashboard');
        
        $user = User::where('reminder','=',$token);;
        if($user->count() >=1)
        {
            $this->data['verCode']= $token;
            return view('user.remind',$this->data);
            
        } else {
            return redirect('user/login')->with(['message'=>'Cant find your reset code','status'=>'error']);
        }
        
    }
    
    public function postDoreset( Request $request , $token = '')
    {
        $rules = array(
            'password'=>'required|between:6,12|confirmed',
            'password_confirmation'=>'required|between:6,12'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            
            $user =  User::where('reminder','=',$token);
            if($user->count() >=1)
            {
                $data = $user->get();
                $user = User::find($data[0]->id);
                $user->reminder = '';
                $user->password = \Hash::make($request->input('password'));
                $user->save();
            }
            
            return redirect('user/login')->with(['message'=>'Password has been saved!','status'=>'success'] );
        } else {
            return redirect('user/reset/'.$token)->with(['message'=>'The following errors occurred','status'=>'error'])->withErrors($validator)->withInput();
        }
        
    }
    
    public function getLogout() {
        \Auth::logout();
        \Session::flush();
        return redirect('')->with(['message'=>'Your are now logged out!','status'=>'success']);
    }
    
    function socialize( $social )
    {
        return Socialize::driver($social)->redirect();
    }
    
    function autosocialize( $social )
    {
        $user = Socialize::driver($social)->user();
        $users = \DB::table('tb_users')->where('email',$user->email)->get();
        
        if(count($users)){
            $row = $users[0];
            return self::autoSignin($row->id);
            
        } else {
            return redirect('user/login')
            ->with(['message'=>'You have not registered yet ','status'=>'error']);
        }
        
    }
    
    function autoSignin($id)
    {
        
        if(is_null($id)){
            return redirect('user/login')
            ->with(['message'=>'You have not registered yet ','status'=>'error']);
        } else{
            
            \Auth::loginUsingId( $id );
            if(\Auth::check())
            {
                $row = User::find(\Auth::user()->id);
                
                if($row->active =='0')
                {
                    // inactive
                    \Auth::logout();
                    return redirect('user/login')->with(['message'=>'Your Account is not active','status'=>'error']);
                    
                } else if($row->active=='2')
                {
                    // BLocked users
                    \Auth::logout();
                    return redirect('user/login')->with(['message'=>'Your Account is BLocked','status'=>'error']);
                } else {
                    $session = array(
                        'gid' => $row->group_id,
                        'uid' => $row->id,
                        'eid' => $row->email,
                        'll' => $row->last_login,
                        'fid' =>  $row->first_name.' '. $row->last_name,
                        'username' =>  $row->username ,
                        'join'	=>  $row->created_at
                    );
                    if($this->config['cnf_front'] =='false') :
                    return redirect('dashboard');
                    else :
                    return redirect('');
                    endif;
                    
                    
                }
                
                
            }
        }
        
    }
	
	public function checkID(){
		$idKtp = $_POST["data"];	
		$query = \DB::table('acc_candidate_profile')->where('identitycardnumber', $idKtp)->get();
		if(count($query) > 0){
			$temp = "yes";
		} else {
			$temp = "no";
		}
		
		return $temp;
	}

	public function checkPassword(){
		$oldPassword = $_POST["data"];	
		$dbEmail = \Auth::user()->email;
		
		if(\Auth::attempt(array('email'=>$dbEmail, 'password'=> $oldPassword))){
			$temp="yes";
		} else {
			$temp = "no";
		}
		
		return $temp;
	}
    
    
}