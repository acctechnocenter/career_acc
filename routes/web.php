<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Default Controller
Route::get('/', 'HomeController@index');
Route::post('/home/submit', 'HomeController@submit');
Route::get('/home/skin/{any?}', 'HomeController@getSkin');


Route::get('dashboard/import', 'DashboardController@getImport');
/* Auth & Profile */
Route::get('user/profile','UserController@getProfile');
Route::get('user/login','UserController@getLogin');
Route::get('user/register','UserController@getRegister');
Route::get('user/logout','UserController@getLogout');
Route::get('user/reminder','UserController@getReminder');
Route::get('user/reset/{any?}','UserController@getReset');
Route::get('user/reminder','UserController@getReminder');
Route::get('user/activation','UserController@getActivation');

//edit organizational experience
Route::get('profile/organi_exp/{id}','UserController@getEditOrg');
Route::post('save-edit-organization-exp','UserController@saveEditOrgExp');
Route::get('user/deleteorg/{id}','UserController@destroyOrgExp'); 

//add organizational experience
Route::get('profile/add_organi_exp','UserController@getaddOrg');
Route::post('save-add-organization-exp','UserController@saveAddOrgExp');
Route::get('user/deleteedu/{id}','UserController@destroyEduExp'); 

//edit Educational experience
Route::get('profile/edit_edu_exp/{id}','UserController@getEditEdu');
Route::post('save-edit-edu-exp','UserController@saveEditEduExp');

//add organizational experience
Route::get('profile/add_edu_exp','UserController@getaddEdu');
Route::post('save-add-edu-exp','UserController@saveAddEduExp');

//add file attach
Route::get('image-upload-with-validation',['as'=>'fileindex','uses'=>'UserController@fileindex']);
Route::post('image-upload-with-validation',['as'=>'filestore','uses'=>'UserController@filestore']);
// Route::get('profile','UserController@fileindex');
// Route::post('store','UserController@filestore');

//add Working experience
Route::get('profile/add_work_exp','UserController@getaddWork');
Route::post('save-add-working-exp','UserController@saveAddWorkExp');
Route::get('user/delete/{id}','UserController@destroyWorkExp');

//edit Working experience
Route::get('profile/edit_work_exp/{id}','UserController@getEditWork');
Route::post('save-edit-work-exp','UserController@saveEditWorkExp');

//applicant menu
Route::get('applicant-menu','DashboardController@getApplicant');
Route::get('applicant-menu/ApplicantDetail/{id}','DashboardController@getApplicantDetail');
Route::get('applicant-menu/edubackdetail/{id}','DashboardController@getEdubackDetail');
Route::get('applicant-menu/orgexpdetail/{id}','DashboardController@getOrgexpDetail');
Route::get('applicant-menu/workexpdetail/{id}','DashboardController@getWorkexpDetail');

//add new applicant menu

//edit&update personal detail Applicant Menu
Route::get('applicant-menu/editdataapplicant/{id}','DashboardController@getEditDataApplicant');
Route::post('applicant-menu/editdataapplicant/updateapplicantdata','DashboardController@postUpdateApplicantData');

//edit educational exp Applicant Menu
Route::get('applicant-menu/edubackdetail/appmenu_edit_edu_exp/{idApplicant}','DashboardController@getEditEduAppMenu');
Route::post('save-appmenu-edit-edu-exp','DashboardController@saveEditEduExpAppMenu');

//delete educational background applicant menu
Route::get('dashboard/deleteeduappmenu/{id}','DashboardController@destroyAppMenuEdu');

//add educational exp Applicant Menu
Route::get('applicant-menu/appmenu_add_edu_exp/{idApplicantAddedu}','DashboardController@getAddEduapp');
Route::post('save-appmenu-add-edu-exp','DashboardController@saveAddEduExpAppMenu');

//add Organizational exp Applicant Menu
Route::get('applicant-menu/appmenu_add_org_exp/{idApplicantAddorg}','DashboardController@getAddorgapp');
Route::post('save-appmenu-add-org-exp','DashboardController@saveAddOrgExpAppMenu');

//edit Organizational exp Applicant Menu
Route::get('applicant-menu/orgexpdetail/appmenu_edit_org_exp/{id}','DashboardController@getEditOrgAppMenu');
Route::post('save-appmenu-edit-org-exp','DashboardController@saveEditOrgExpAppMenu');

//delete educational background applicant menu
Route::get('dashboard/deleteorgappmenu/{id}','DashboardController@destroyAppMenuOrg');

//add working exp applicant menu
Route::get('applicant-menu/appmenu_add_work_exp/{idApplicantAddwork}','DashboardController@getAddworkapp');
Route::post('save-appmenu-add-work-exp','DashboardController@saveAddworkExpAppMenu');

//edit working exp applicant menu 
Route::get('applicant-menu/workexpdetail/appmenu_edit_work_exp/{id}','DashboardController@getEditWorkAppMenu');
Route::post('save-appmenu-edit-work-exp','DashboardController@saveEditWorkExpAppMenu');

//delete educational background applicant menu
Route::get('dashboard/deleteworkappmenu/{id}','DashboardController@destroyAppMenuWork');

//add file attachment applicant menu
Route::get('applicant-menu/editfileattchment/{id}','DashboardController@getEditFileAttachmentApplicant');
Route::get('applicant-menu/editfileattachment/updatefileattachmentapplicant',['as'=>'indexfileattachmentappicant','uses'=>'DashboardController@indexfileattachmentappicant']);
Route::post('applicant-menu/editfileattchment/updatefileattachmentapplicant',['as'=>'postupdatefileattachmentapplicant','uses'=>'DashboardController@postupdatefileattachmentapplicantm']);

//delete fileattachment
Route::get('dashboard/deletefileattchmentappmenu/{id}','DashboardController@destroyAppMenuFileattach');

//delete fileattachment profile menu
Route::get('user/deletefileattchmentprofile/{id}','UserController@destroyAppMenuFileattachprofile');

//edit file attachment applicant menu

//end new applicant menu

// Social Login
Route::get('user/socialize/{any?}','UserController@socialize');
Route::get('user/autosocialize/{any?}','UserController@autosocialize');
//
Route::post('user/signin','UserController@postSignin');
Route::post('user/create','UserController@postCreate');
Route::post('user/saveprofile','UserController@postSaveprofile');
Route::post('user/saveapplicantdata','UserController@postSaveApplicantData');
Route::post('user/savepassword','UserController@postSavepassword');
Route::post('user/saveapplicantdocument','UserController@postSaveApplicantdocument');
Route::post('user/doreset/{any?}','UserController@postDoreset');
Route::post('user/request','UserController@postRequest');

/* Posts & Blogs */
Route::get('posts','HomeController@posts');
Route::get('posts/{any}','HomeController@posts');
Route::post('posts/comment','HomeController@comment');
Route::get('posts/remove/{id?}/{id2?}/{id3?}','HomeController@remove');

Route::get('post-category/{category_id}','PostcategoryController@getPostsByCategory');

// Start Routes for Notification 
Route::resource('notification','NotificationController');
Route::get('home/load','HomeController@getLoad');
Route::get('home/lang/{any}','HomeController@getLang');

Route::get('/set_theme/{any}', 'HomeController@set_theme');

/* Custom */
Route::get('job-list','JobController@getJobList');
Route::get('job-list/{id?}','JobController@getJobListParam');
Route::get('job-list/{idcat}/{id}/{slug}','JobController@getJobDetail');
Route::get('job-field','JobfieldController@getJobfieldList');
Route::get('job-lists/category/{cat}','JobController@getJobListCat');
Route::get('job-lists/category/{cat}/{sub}','JobController@getJobListCatSub');
Route::any('/search','JobController@jobSearch');
Route::get('list-prescreening-result','PrescreeningresultController@adminView');
Route::get('list-prescreening-result/{status}/{idjob}','PrescreeningstatusController@getListApplied');
Route::get('edit-prescreening-result/{status}/{idstatus}','PrescreeningstatusController@getEditApplied');
Route::get('view-prescreening-result/{idstatus}','PrescreeningstatusController@getPrescreeningView');
Route::get('prescreening/{status}/{iduser}/{idjob}','PrescreeningresultController@getResultApplicant');
Route::get('history-prescreening','PrescreeningstatusController@getPrescreeningHist');
Route::post('save-prescreening-status','PrescreeningstatusController@saveListApplied');
Route::get('search-prescreening-result', function() { return view('prescreeningresult/search-prescreening-result'); });
Route::get('search-detail-prescreening/{status}/{idjob}', function($status,$idjob) { return view('prescreeningstatus/search-detail-prescreening', ['status' => $status, 'idjob' => $idjob]); });
Route::any('result-search-pre','PrescreeningresultController@filterSearch');
Route::any('result-search-detail-pre/{status}/{idjob}','PrescreeningstatusController@filterSearch');
Route::get('contactus-ui','ContactusController@getContactUs');
Route::get('test-4','KategorisoalController@testDoc');
Route::get('mail-test','KategorisoalController@getMail');
Route::post('check-id','UserController@checkID');
Route::post('check-password','UserController@checkPassword');
Route::get('search-applicant', function() {return view('applicantmenu/search-applicant'); });
Route::any('result-search-applicant','DashboardController@filterSearchApp');
Route::get('view-comments/{idpage}','Core\PostsController@viewComments');
Route::get('status-comments/{idcomment}','Core\PostsController@statusComments');
Route::post('change-status-comments','Core\PostsController@changeStatus');
Route::post('bulk-save-prescreening','PrescreeningstatusController@bulkSaveStatus');
/*Route::get('test-5','KategorisoalController@testAppSel'); */

//edit&update personal detail Applicant Menu
Route::get('applicant-menu/editdataapplicant/{id}','DashboardController@getEditDataApplicant');
Route::post('applicant-menu/editdataapplicant/updateapplicantdata','DashboardController@postUpdateApplicantData');

//edit educational exp Applicant Menu
Route::get('applicant-menu/edubackdetail/appmenu_edit_edu_exp/{idApplicant}','DashboardController@getEditEduAppMenu');
Route::post('save-appmenu-edit-edu-exp','DashboardController@saveEditEduExpAppMenu');

//add educational exp Applicant Menu
Route::get('applicant-menu/appmenu_add_edu_exp/{idApplicantAddedu}','DashboardController@getAddEduapp');
Route::post('save-appmenu-add-edu-exp','DashboardController@saveAddEduExpAppMenu');

//edit educational exp Applicant Menu
Route::get('applicant-menu/edubackdetail/appmenu_edit_edu_exp/{idApplicant}','DashboardController@getEditEduAppMenu');
Route::post('save-appmenu-edit-edu-exp','DashboardController@saveEditEduExpAppMenu');

//add Organizational exp Applicant Menu
Route::get('applicant-menu/appmenu_add_org_exp/{idApplicantAddorg}','DashboardController@getAddorgapp');
Route::post('save-appmenu-add-org-exp','DashboardController@saveAddOrgExpAppMenu');

//edit Organizational exp Applicant Menu
Route::get('applicant-menu/orgexpdetail/appmenu_edit_org_exp/{id}','DashboardController@getEditOrgAppMenu');
Route::post('save-appmenu-edit-org-exp','DashboardController@saveEditOrgExpAppMenu');

//add working exp applicant menu
Route::get('applicant-menu/appmenu_add_work_exp/{idApplicantAddwork}','DashboardController@getAddworkapp');
Route::post('save-appmenu-add-work-exp','DashboardController@saveAddworkExpAppMenu');

//edit working exp applicant menu 
Route::get('applicant-menu/workexpdetail/appmenu_edit_work_exp/{id}','DashboardController@getEditWorkAppMenu');
Route::post('save-appmenu-edit-work-exp','DashboardController@saveEditWorkExpAppMenu');


/* Prescreening  Result*/
Route::get('prescreening-list/{idjob}/{slug}','PrescreeningController@getPrescreening');
Route::post('prescreening-result','PrescreeningresultController@saveAnswer');


/* Prescreening Question */
Route::get('prescreening-management','KategorisoalController@getCategoryPre');
Route::get('prescreening-management/group/{id}/delete','KategorisoalController@groupDelete');
Route::get('prescreening-management/question/{idgroup}/view','SoalController@getQuestion');
Route::get('prescreening-management/question/{idgroup}/create','SoalController@addQuestion');
Route::get('prescreening-management/question/{idgroup}/{id}/edit','SoalController@editQuestion');
Route::post('prescreening-management/question/delete','SoalController@deleteQuestion');
Route::post('prescreening-management/question/save','SoalController@customStore');
Route::get('prescreening-management/answer/{idsoal}/view','JawabanController@getAnswer');
Route::get('prescreening-management/answer/{idsoal}/create','JawabanController@addAnswer');
Route::get('prescreening-management/answer/{idsoal}/{id}/edit','JawabanController@editAnswer');
Route::post('prescreening-management/answer/delete','JawabanController@deleteAnswer');
Route::post('prescreening-management/answer/save','JawabanController@customStore');

/* FAQ */
Route::get('faq-ui','FaqController@getFaqUi');
Route::get('faq-applicant','FaqapplicantController@getFaqApplicant');
Route::get('faq-admin','FaqapplicantController@getFaqAdmin');

/* API */
Route::post('applicant-status/{id}','DashboardController@editApplicantStatus');
Route::post('upsert-univ','DashboardController@upsertUniv');
Route::post('upsert-profile','DashboardController@schedulerProfileIndex');
Route::post('delete-profile','DashboardController@schedulerDeleteProfile');
Route::post('api-prescreening-result','PrescreeningresultController@apiGetResultPres');
//Route::post('api-prescreening-result','PrescreeningresultController@apiGetResultPres')->middleware('cors');
//Route::post('api-prescreening-result',['middleware' => 'cors', 'uses' => 'PrescreeningresultController@apiGetResultPres']);
Route::post('api-vacancy-email','DashboardController@sendSubsVacancy');
//Route::get('unsubscribe/{id_user}/{username}/{id_candidate}','DashboardController@getUnsubscribe');
Route::get('unsubscribe/{id_user}','DashboardController@getUnsubscribe');

include('pages.php');


Route::resource('sximoapi','SximoapiController');

// Routes for  all generated Module
include('module.php');
// Custom routes  
$path = base_path().'/routes/custom/';
$lang = scandir($path);
foreach($lang as $value) {
	if($value === '.' || $value === '..') {continue;} 
	include( 'custom/'. $value );	
	
}
// End custom routes
Route::group(['middleware' => 'auth'], function () {
	Route::resource('dashboard','DashboardController');
});


Route::group(['namespace' => 'Sximo','middleware' => 'auth'], function () {
	// This is root for superadmin
		
		include('sximo.php');
		
});

Route::group(['namespace' => 'Core','middleware' => 'auth'], function () {

	include('core.php');

});