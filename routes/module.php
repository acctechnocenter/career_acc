<?php
        
// Start Routes for job 
Route::resource('job','JobController');
// End Routes for job 

                    
// Start Routes for candidateprofile 
Route::resource('candidateprofile','CandidateprofileController');
// End Routes for candidateprofile 

                    
// Start Routes for jobfield 
Route::resource('jobfield','JobfieldController');
// End Routes for jobfield 

                    
// Start Routes for slider 
Route::resource('slider','SliderController');
// End Routes for slider 

                    
// Start Routes for testimonial 
Route::resource('testimonial','TestimonialController');
// End Routes for testimonial 

                    
// Start Routes for faq 
Route::resource('faq','FaqController');
// End Routes for faq 

                    
// Start Routes for soal 
Route::resource('soal','SoalController');
// End Routes for soal 

                    
// Start Routes for jawaban 
Route::resource('jawaban','JawabanController');
// End Routes for jawaban 

                    
// Start Routes for faqapplicant 
Route::resource('faqapplicant','FaqapplicantController');
// End Routes for faqapplicant 

                    
// Start Routes for userprofile 
Route::resource('userprofile','UserprofileController');
// End Routes for userprofile 

                    
// Start Routes for postcategory 
Route::resource('postcategory','PostcategoryController');
// End Routes for postcategory 

                    
// Start Routes for prescreeningresult 
Route::resource('prescreeningresult','PrescreeningresultController');
// End Routes for prescreeningresult 

                    
// Start Routes for kategorisoal 
Route::resource('kategorisoal','KategorisoalController');
// End Routes for kategorisoal 

                    
// Start Routes for prescreeningstatus 
Route::resource('prescreeningstatus','PrescreeningstatusController');
// End Routes for prescreeningstatus 

                    
// Start Routes for placement 
Route::resource('placement','PlacementController');
// End Routes for placement 

                    
// Start Routes for contactus 
Route::resource('contactus','ContactusController');
// End Routes for contactus 

                    ?>