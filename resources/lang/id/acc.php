<?php

return array(
	//General
	"no-data" => "Tidak ada data",
	"yes" => "Ya",
	"no" => "Tidak",
	
	//Validation when apply job button clicked
	"notlogin" => "Anda harus login terlebih dahulu",
	"samejob" => "Anda telah melamar sebuah pekerjaan",
	"completeprofile" => "Anda harus melengkapi profile dan riwayat pendidikan",
	
	//URL API
"loginsf" => "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9YDQS5WtC11qCx6myL.DB0jVchJqNEQ7g_74WM.2L0o_8W74ThbpgFgl3bdAmSy9dsW1krtGXfU4P0p2j&client_secret=6543040097840973320&username=mustika.murni@acc.co.id&password=HondaPCX2018WarnaHitamWGwllloRYBZWmupLcUb9YmU2m",
	"postkategorisoal" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_PRESCREENING_QUESTION__c",
	"postapplicant" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT__c",
	"postapplicantselection" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_SELECTION__c",
	"postorgexp" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_ORGANIZATION__c",
	"posteduback" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_EDUCATION__c",
	"postworkexp" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_WORKING__c",
	"postfile" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_DOCUMENT__c",
	"mobile_postapplicant" => "https://acc-career-api.talents.id/talents/applicant",
	"mobile_postapplicantselection" => "https://acc-career-api.talents.id/talents/applicantselection",
	"mobile_postorgexp" => "https://acc-career-api.talents.id/talents/applicant/organizationexperience",
	"mobile_posteduback" => "https://acc-career-api.talents.id/talents/applicant/education",
	"mobile_postworkexp" => "https://acc-career-api.talents.id/talents/applicant/workexperience",
	"mobile_username_api" => "acc.prod",
	"mobile_password_api" => "Welcome@123",
	
	//User pass API custom
	"userapi" => "force@test.com",
	"passapi" => "jWyMDI-XKQ6Z-WuSbJF-9n29v",
	
	//Prescreening result applicant
	"viewpre-name" => "Nama",
	"viewpre-job" => "Pekerjaan",
	"viewpre-status" => "Status",
	"viewpre-soal" => "Pertanyaan",
	"viewpre-jwb" => "Jawaban",
	"viewpre-dis" =>"Diskualifikasi",
	"viewpre-skor" => "Skor",
	"viewpre-group" => "Grup Prescreening",
);