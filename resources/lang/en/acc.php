<?php

return array(
	//General
	"no-data" => "No Data",
	"yes" => "Yes",
	"no" => "No",
	
	//Validation when apply job button clicked
	"notlogin" => "You must login first",
	"samejob" => "Already applied to a job",
	"completeprofile" => "You must complete your profile and educational background",
	
	//URL API
"loginsf" => "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9YDQS5WtC11qCx6myL.DB0jVchJqNEQ7g_74WM.2L0o_8W74ThbpgFgl3bdAmSy9dsW1krtGXfU4P0p2j&client_secret=6543040097840973320&username=mustika.murni@acc.co.id&password=HondaPCX2018WarnaHitamWGwllloRYBZWmupLcUb9YmU2m",
	"postkategorisoal" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_PRESCREENING_QUESTION__c",
	"postapplicant" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT__c",
	"postapplicantselection" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_SELECTION__c",
	"postorgexp" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_ORGANIZATION__c",
	"posteduback" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_EDUCATION__c",
	"postworkexp" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_WORKING__c",
	"postfile" => "https://ap4.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_DOCUMENT__c",
	"mobile_postapplicant" => "https://acc-career-api.talents.id/talents/applicant",
	"mobile_postapplicantselection" => "https://acc-career-api.talents.id/talents/applicantselection",
	"mobile_postorgexp" => "https://acc-career-api.talents.id/talents/applicant/organizationexperience",
	"mobile_posteduback" => "https://acc-career-api.talents.id/talents/applicant/education",
	"mobile_postworkexp" => "https://acc-career-api.talents.id/talents/applicant/workexperience",
	"mobile_username_api" => "acc.prod",
	"mobile_password_api" => "Welcome@123",
	
	//User pass API custom
	"userapi" => "force@test.com",
	"passapi" => "jWyMDI-XKQ6Z-WuSbJF-9n29v",
	
	//Prescreening result applicant
	"viewpre-name" => "Name",
	"viewpre-job" => "Job",
	"viewpre-status" => "Status",
	"viewpre-soal" => "Question",
	"viewpre-jwb" => "Answer",
	"viewpre-dis" =>"Disqualified",
	"viewpre-skor" => "Score",
	"viewpre-group" => "Prescreening Group",
);