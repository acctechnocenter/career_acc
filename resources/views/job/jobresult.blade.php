<section>
    <div>
		<div>
			<img src="{!! asset('uploads/homepage/Slide-Banner-be-part-of-ACC.jpg') !!}" class="img-banner" />
       </div>
	   <div class="search-div">
         <div class="input-group row" style="margin: 2% auto !important; width: 50%;">
         	<form action="{{url('search')}}" method="POST" role="search">
         	{{ csrf_field() }}
         	<div class="col-md-10 padding-0">
             <input type="text" class="search-query form-control" placeholder="Search your job" name="q" />
             <div style="background-color: #555;">
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 110px;" name="jf">
						<option value="" selected disabled>Job Field</option>
						@if (count($jobFieldList) > 0)
							@foreach($jobFieldList as $optionField)
								<option value="{{ $optionField->id }}">{{ $optionField->jobfield }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 140px;" name="jc">
						<option value="" selected disabled>Job Category</option>
						@if (count($jobCatList) > 0)
							@foreach($jobCatList as $optionCat)
								<option value="{{ $optionCat->JobCategory }}">{{ $optionCat->JobCategory }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search" style="width: 120px;" name="loc">
						<option value="" selected disabled>Location</option>
						@if (count($locList) > 0)
							@foreach($locList as $optionLoc)
								<option value="{{ $optionLoc->id }}">{{ $optionLoc->name }}</option>
							@endforeach
						@endif
 					</select>
				</div>
             </div>
             </div>
             
             <div class="input-group-btn col-md-2 padding-0">
                 <button class="btn btn-primary search-job-btn" type="submit" style="height:68px;">
                     <span>GET YOUR JOB!</span>
                 </button>
             </div>
             </form>
         </div>
		</div>
     </div>
</section>


    <section class="section  bg-light filtercategory" style="padding-top:10px!important">
      <!-- WELCOME -->
      <div class="container filtercategory">
          &nbsp;
          &nbsp;
          @if (count($rowData) > 0)
			<p style="padding-bottom: 10px;"> Result for:  
				<?php 
					if($q){
						echo "<span class='res'>". $q . "</span>" ;
					}
					if($jf){
						echo "<span class='res'>". $jf . "</span>" ;
					}
					if($jc){
						echo "<span class='res'>". $jc . "</span>" ;
					}
					if($loc){
						echo "<span class='res'>". $loc . "</span>" ;
					}
				?>
			</p>
          <div class="row">
          	@foreach($rowData as $items)
          	<?php $filter_cat = strtolower( $items->JobCategory);?>
              <div class="col-md-12 effect {{ $filter_cat }}" style="margin: 10px 0; padding:10px 0 !important;">
                 <div class="col-md-3">
					@if($items->img_jobfield)
                      <img class="port-image" src="{!! asset('uploads/jobfield/'.$items->img_jobfield.'') !!}" style="width:255px; height:151px"/> 
					@else
						<img class="port-image" src="{!! asset('uploads/images/no-image.png') !!}" style="width:auto; height:151px;text-align:center"/> 
					@endif
                  </div>
                  <div class="col-md-9">
                  	<p style="font-size: 20px"><b><a href="{{ url('job-list/'. $items->IDJobField . '/'. $items->id . '/' . str_slug($items->JobTitleName, '-') ) }}">{{ $items->JobTitleName }}</a></b></p>
                     <span class="subtitle-job">{{ $items->JobCategory }}</span>@if($items->JobSubCategory) <span class="subtitle-job"> / {{ $items->JobSubCategory }}</span> @endif | <i class="fa fa-map-marker" style="padding-right:3px;"></i>  <span class="subtitle-job">{{ $items->placement }}</span>
                     <br><br>
                     <p style="font-size: 14px">{{ str_limit($items->JobDescription, $words = 200, $end = '...')}}</p>
                     <a class="details-link" href="{{ url('job-list/'. $items->IDJobField . '/'. $items->id . '/' . str_slug($items->JobTitleName, '-') ) }}">Details <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
              </div>
             @endforeach
          </div>
          &nbsp;
          @else
    		<p>No Data</p>
    	  @endif
          </div>
      <!-- /WELCOME -->
      <div class="col-md-12 custom-pagination">
			{!! $rowData->links() !!}
      </div>
    </section>
