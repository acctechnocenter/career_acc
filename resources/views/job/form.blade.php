@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'job?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Job</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Job Title" class=" control-label col-md-4 text-left"> Job Title <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='JobTitleName' id='JobTitleName' value='{{ $row['JobTitleName'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created Date" class=" control-label col-md-4 text-left"> Created Date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('CreatedDate', $row['CreatedDate'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Start Date" class=" control-label col-md-4 text-left"> Start Date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('StartDate', $row['StartDate'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="End Date" class=" control-label col-md-4 text-left"> End Date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('EndDate', $row['EndDate'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ID Job Field" class=" control-label col-md-4 text-left"> ID Job Field <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='IDJobField' id='IDJobField' value='{{ $row['IDJobField'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Job Description" class=" control-label col-md-4 text-left"> Job Description <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='JobDescription' rows='5' id='JobDescription' class='form-control input-sm '  
				           >{{ $row['JobDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Job Category" class=" control-label col-md-4 text-left"> Job Category <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $JobCategory = explode(',',$row['JobCategory']);
					$JobCategory_opt = array( 'Freshgraduate' => 'Freshgraduate' ,  'Experience' => 'Experience' ,  'Internship' => 'Internship' , ); ?>
					<select name='JobCategory' rows='5'   class='select2 '  > 
						<?php 
						foreach($JobCategory_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['JobCategory'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="LastModifiedDate" class=" control-label col-md-4 text-left"> LastModifiedDate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('LastModifiedDate', $row['LastModifiedDate'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Location" class=" control-label col-md-4 text-left"> Location <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Location' id='Location' value='{{ $row['Location'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="GPA" class=" control-label col-md-4 text-left"> GPA <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='GPA' id='GPA' value='{{ $row['GPA'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Degree" class=" control-label col-md-4 text-left"> Degree <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Degree' id='Degree' value='{{ $row['Degree'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="LinkVideo" class=" control-label col-md-4 text-left"> LinkVideo <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='LinkVideo' id='LinkVideo' value='{{ $row['LinkVideo'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="MinWorkExp" class=" control-label col-md-4 text-left"> MinWorkExp <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='MinWorkExp' id='MinWorkExp' value='{{ $row['MinWorkExp'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Requirements" class=" control-label col-md-4 text-left"> Requirements <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='JobRequirements' rows='5' id='JobRequirements' class='form-control input-sm '  
				           >{{ $row['JobRequirements'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Max Age" class=" control-label col-md-4 text-left"> Max Age <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='MaxAge' id='MaxAge' value='{{ $row['MaxAge'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="IdExternal" class=" control-label col-md-4 text-left"> IdExternal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='IdExternal' id='IdExternal' value='{{ $row['IdExternal'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Id Kategori Soal" class=" control-label col-md-4 text-left"> Id Kategori Soal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='id_kategori_soal' rows='5' id='id_kategori_soal' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $StatusJob = explode(',',$row['StatusJob']);
					$StatusJob_opt = array( 'active' => 'Active' ,  'inactive' => 'Inactive' , ); ?>
					<select name='StatusJob' rows='5'   class='select2 '  > 
						<?php 
						foreach($StatusJob_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['StatusJob'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Sub Category" class=" control-label col-md-4 text-left"> Sub Category <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='JobSubCategory' rows='5' id='JobSubCategory' class='form-control input-sm '  
				           >{{ $row['JobSubCategory'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type" class=" control-label col-md-4 text-left"> Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='JobStatusType' rows='5' id='JobStatusType' class='form-control input-sm '  
				           >{{ $row['JobStatusType'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		
		$("#id_kategori_soal").jCombo("{!! url('job/comboselect?filter=acc_kategori_soal:id:name') !!}",
		{  selected_value : '{{ $row["id_kategori_soal"] }}' });
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("job/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop