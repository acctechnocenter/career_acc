<section class="section" style="padding-top:40px!important">
		@if(count($soalan) > 0)
        <div class="container">
        <br>
        <h3>Prescreening Question for : <b>{{ $soalan[0]->job_title }}</b></h3><br>

                <form class="form">
                <input id="job" name="id_job" type="hidden" value="{{ $soalan[0]->id_job }}"/>
				<input id="urlback" name="urlback" type="hidden" value="{{ 'job-list/'.$idjobcat->idjobfield.'/'. $soalan[0]->id_job.'/'.str_slug($idjobcat->namejobfield, '-')}}"/>
            <ol>
                @foreach($soalan as $question) 

                <li>
                    <p class="soal">{{ $question->soal }}</p>
						@if($question->type == 1)
							@foreach($jawaban as $answer)
								@if($question->id_soal == $answer->id_soal)
								<div class="group-jwb">
									<input name="{{ $answer->id_soal }}" type="checkbox" value="{{ $answer->id.'#'.$answer->skor.'#'.$answer->disqualified }}" class="jawaban"/>
									<span class="span-jwb">{{ $answer->jawaban }}</span>
								</div>
								@endif
							@endforeach
                         @elseif($question->type == 2)
							@foreach($jawaban as $answer)
								@if($question->id_soal == $answer->id_soal)
								<div class="group-jwb">
									<input name="{{ $answer->id_soal }}" type="radio" value="{{ $answer->id.'#'.$answer->skor.'#'.$answer->disqualified }}" class="jawaban" required/>
									<span class="span-jwb"> {{ $answer->jawaban }}</span>
								</div>
                                @endif
							@endforeach
						@elseif($question->type == 3)
								<div class="group-jwb">
									<textarea rows="4" cols="50" name="{{ $question->id_soal }}" class="essay"></textarea>
								</div>
                        @endif
                        
                        <br>
                </li> 
                
                @endforeach
            </ol>    

				<!-- <button type="button" id="submitpre" class="btn btn-primary submitpre-btn" data-toggle="modal" data-target="#confirmpre">
					SUBMIT
				</button> -->
				
				<button type="button" id="submitpre" class="btn btn-primary submitpre-btn" onclick="validate()">SUBMIT</button>
				
				<div class="modal fade" id="confirmpre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
					  </div>
					  <div class="modal-body">
						Apakah Anda yakin untuk melakukan submit ?
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="button" class="btn btn-primary">Submit</button>
					  </div>
					</div>
				  </div>
				</div>
                </form>
				<br>
               	<div  id="success-alert" class="alert alert-success" style="display:none;">
					Anda telah berhasil melakukan submit prescreening ini. Harap cek email Anda.
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="error-alert" class="alert alert-danger" style="display:none;">
					Terjadi kesalahan pada sistem. Silakan coba lagi atau hubungi administrator.
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
               	<div  id="warning-alert" class="alert alert-warning" style="display:none;">
					Terdapat soal yang belum dijawab
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
       </div>
		@else
			<div class="container">
				<h2 style="margin-top:40px;">Sorry, question not available.</h2>
			</div>
		@endif
</section>

<style>
.soal {
	margin: 5px 0 5px 5px;
	font-size: 16px;
	font-weight: bold;
}

.span-jwb{
	font-size: 16px;
}

.group-jwb{
	margin: 5px;
}

.submitpre-btn{
	width: 139px;
	height: 35px;
	border-radius: 5px;
	background-color: #0089d1;
	border-color: #0089d1
}
</style>


    <script>
	
	function validate(){
		var flag = true;
			$(':radio').each(function () {
				name = $(this).attr('name');
				if (flag && !$(':radio[name="' + name + '"]:checked').length) {	
					flag = false;
					//return alert(name + ' group not checked');
				}
			});
			
			if(flag){
				$("#confirmpre").modal('show');
			} else {
				$('#warning-alert').fadeIn();
			}
	}
	
	function closee(){
		$('.alert').fadeOut();
	}
	
     $(document).ready(function(){

        $("#button").on("click",function(){

                                $('#submitpre').prop("disabled", true);
				$("#confirmpre").modal('hide');
                var res;
                var job;
				var baseUrl = {!! json_encode(url('/')) !!}
				var urlBack = $('#urlback').val();
                jsonData = [];
				essayData = [];
                job = $("#job").val();
				
				$(".essay").each(function(){
						if($(this).val() != ""){
							res = $(this).attr('name') + '#' + $(this).val();                  
							essayData.push(res);
						}
					});

					$(".jawaban:checked").each(function(){
						res = $(this).attr('name') + '#' + $(this).val();                  
						jsonData.push(res);
					});
                
				if(jsonData.length == 0){
					jsonDataTemp = [];
					temp = [];
					$(".jawaban").each(function(){
						temp = $(this).val().split('#');
						res = $(this).attr('name') + '#' + temp[0] + '#' + 0 + '#' + 0;
						jsonData.push(res);
					});
				}
				
					$.ajax({
						url     : "{{ url('prescreening-result') }}",
						method  : "POST",
						data    : {"hasil" : jsonData, "idJob" : job, "essay": essayData } ,
						success : function(){
							$('#submitpre').prop("disabled", true);
							$('#success-alert').fadeIn();
							setTimeout(function(){
								window.location = baseUrl + '/' + urlBack;
							}, 5000);
						}, 
						error : function(){
							$('#error-alert').fadeIn();
                                                        $('#submitpre').prop("disabled", false);
						}
					});
					console.log(essayData);
            }); 

    });
    </script>

	