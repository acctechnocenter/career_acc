

		 {!! Form::open(array('url'=>'prescreening', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Prescreening</legend>
									
									  <div class="form-group  " >
										<label for="Soal" class=" control-label col-md-4 text-left"> Soal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='id_soal' rows='5' id='id_soal' class='select2 '   ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Jawaban" class=" control-label col-md-4 text-left"> Jawaban <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='jawaban' rows='5' id='jawaban' class='form-control input-sm '  
				           >{{ $row['jawaban'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Is Benar" class=" control-label col-md-4 text-left"> Is Benar <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $is_benar = explode(',',$row['is_benar']);
					$is_benar_opt = array( '0' => 'false' ,  '1' => 'true' ,  '2' => 'disqualified' , ); ?>
					<select name='is_benar' rows='5'   class='select2 '  > 
						<?php 
						foreach($is_benar_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['is_benar'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Skor" class=" control-label col-md-4 text-left"> Skor <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='skor' id='skor' value='{{ $row['skor'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type" class=" control-label col-md-4 text-left"> Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $type = explode(',',$row['type']);
					$type_opt = array( '1' => 'Dropdown' ,  '2' => 'Checkbox' , ); ?>
					<select name='type' rows='5'   class='select2 '  > 
						<?php 
						foreach($type_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['type'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='createdOn' rows='5' id='createdOn' class='form-control input-sm '  
				           >{{ $row['createdOn'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#id_soal").jCombo("{!! url('prescreening/comboselect?filter=acc_soal:id:soal') !!}",
		{  selected_value : '{{ $row["id_soal"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
