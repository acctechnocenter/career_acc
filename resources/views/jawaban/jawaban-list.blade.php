@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<?php
$userId = Auth::user()->id;
$is_user= AccHelpers::is_user($userId);
 ?>
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> {{ $pageTitle }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
		@if(!$is_user)
			<div class="sbox-title">
				<h1> All Records</h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('prescreening-management/answer/'.$soal->id.'/view') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<!-- Toolbar Top -->
			<div class="row">
				<div class="col-md-4"> 	
					<a href="{{ url('prescreening-management/answer/'.$soal->id.'/create') }}" class="btn btn-default btn-sm"  
						title="{{ __('core.btn_create') }}"><i class=" fa fa-plus "></i> Create New Answer </a>
					<a href="{{ url('prescreening-management/question/'.$soal->id_kategori_soal.'/view') }}" class="btn btn-default btn-sm"  
						title="Back"><i class=" fa fa-arrow-left "></i> Back</a>
				</div>  
			</div>					
			<!-- End Toolbar Top -->
			<!-- Table Grid -->
			
			<div class="row">
				<div class="col-md-12">
				<br/>
					<h5>Group: {{ $soal->name }}</h5>
					<h3>{{ $soal->soal}} </h3>
				</div>
			</div>
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center">Answer</th>
						<th align="center">Disqualified</th>
						<th align="center">Score</th>
						<th align="center">Action</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->jawaban }}</td>
							@if($row->disqualified == 1)
							<td>Yes</td>
							@elseif ($row->disqualified == 2)
							<td>No</td>
							@endif
							<td>{{ $row->skor }}</td>
							<td>  
								<a href="{{ url('prescreening-management/answer/'.$soal->id.'/'.$row->id.'/edit') }}"><button class="btn btn-primary btn-xs" type="button" title="Edit Answer"><i class="fa fa-edit"></i> </button></a>  
								<button class="btn btn-primary btn-xs" type="button" title="Remove"  onclick="confirmDelete({{$soal->id}},{{$row->id}})"><i class="fa fa-times"></i> </button> 
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->
			

			</div>
		</div>
		@else
			<div>Permission not allowed</div>
		@endif
	</div>
</div>

@stop

<script type="text/javascript">

	function confirmDelete(idSoal, id){
		if(confirm('Are you sure you want to delete this?')){
			$.ajax({
                url: "{{ url('prescreening-management/answer/delete') }}",
                method: "POST",
                data: {"idSoal" : idSoal, "id": id },
				success : function(){
						alert('Deleted successfully');
						window.location.reload();
						}, 
				error : function(){
						alert('There was a problem. Please contact administrator');
						window.location.reload();
						}
            });
		}
	}

</script>