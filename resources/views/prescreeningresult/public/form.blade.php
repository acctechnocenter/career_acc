

		 {!! Form::open(array('url'=>'prescreeningresult', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Prescreening Result</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="User" class=" control-label col-md-4 text-left"> User <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_user' id='id_user' value='{{ $row['id_user'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Soal" class=" control-label col-md-4 text-left"> Soal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_soal' id='id_soal' value='{{ $row['id_soal'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Jawaban" class=" control-label col-md-4 text-left"> Jawaban <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_jawaban' id='id_jawaban' value='{{ $row['id_jawaban'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created Date" class=" control-label col-md-4 text-left"> Created Date <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_date', $row['created_date'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Skor" class=" control-label col-md-4 text-left"> Skor <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='skor' rows='5' id='skor' class='form-control input-sm '  
				           >{{ $row['skor'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Job" class=" control-label col-md-4 text-left"> Job <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_job' id='id_job' value='{{ $row['id_job'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='createdOn' rows='5' id='createdOn' class='form-control input-sm '  
				           >{{ $row['createdOn'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
