<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id User', (isset($fields['id_user']['language'])? $fields['id_user']['language'] : array())) }}</td>
						<td>{{ $row->id_user}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Firstname', (isset($fields['firstname']['language'])? $fields['firstname']['language'] : array())) }}</td>
						<td>{{ $row->firstname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lastname', (isset($fields['lastname']['language'])? $fields['lastname']['language'] : array())) }}</td>
						<td>{{ $row->lastname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) }}</td>
						<td>{{ $row->title}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gender', (isset($fields['gender']['language'])? $fields['gender']['language'] : array())) }}</td>
						<td>{{ $row->gender}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Dateofbirth', (isset($fields['dateofbirth']['language'])? $fields['dateofbirth']['language'] : array())) }}</td>
						<td>{{ $row->dateofbirth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Provinceofbirth', (isset($fields['provinceofbirth']['language'])? $fields['provinceofbirth']['language'] : array())) }}</td>
						<td>{{ $row->provinceofbirth}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Birthplace', (isset($fields['birthplace']['language'])? $fields['birthplace']['language'] : array())) }}</td>
						<td>{{ $row->birthplace}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nomorktp', (isset($fields['nomorktp']['language'])? $fields['nomorktp']['language'] : array())) }}</td>
						<td>{{ $row->nomorktp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Homephone', (isset($fields['homephone']['language'])? $fields['homephone']['language'] : array())) }}</td>
						<td>{{ $row->homephone}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mobilephone', (isset($fields['mobilephone']['language'])? $fields['mobilephone']['language'] : array())) }}</td>
						<td>{{ $row->mobilephone}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Maritalstatus', (isset($fields['maritalstatus']['language'])? $fields['maritalstatus']['language'] : array())) }}</td>
						<td>{{ $row->maritalstatus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Religion', (isset($fields['religion']['language'])? $fields['religion']['language'] : array())) }}</td>
						<td>{{ $row->religion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lastestducation', (isset($fields['lastestducation']['language'])? $fields['lastestducation']['language'] : array())) }}</td>
						<td>{{ $row->lastestducation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Universityorschool', (isset($fields['universityorschool']['language'])? $fields['universityorschool']['language'] : array())) }}</td>
						<td>{{ $row->universityorschool}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Faculty', (isset($fields['faculty']['language'])? $fields['faculty']['language'] : array())) }}</td>
						<td>{{ $row->faculty}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Major', (isset($fields['major']['language'])? $fields['major']['language'] : array())) }}</td>
						<td>{{ $row->major}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gpa', (isset($fields['gpa']['language'])? $fields['gpa']['language'] : array())) }}</td>
						<td>{{ $row->gpa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Startdate', (isset($fields['startdate']['language'])? $fields['startdate']['language'] : array())) }}</td>
						<td>{{ $row->startdate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Endate', (isset($fields['endate']['language'])? $fields['endate']['language'] : array())) }}</td>
						<td>{{ $row->endate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Country', (isset($fields['country']['language'])? $fields['country']['language'] : array())) }}</td>
						<td>{{ $row->country}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Address', (isset($fields['address']['language'])? $fields['address']['language'] : array())) }}</td>
						<td>{{ $row->address}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kecamatan', (isset($fields['kecamatan']['language'])? $fields['kecamatan']['language'] : array())) }}</td>
						<td>{{ $row->kecamatan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kotaorkabupaten', (isset($fields['kotaorkabupaten']['language'])? $fields['kotaorkabupaten']['language'] : array())) }}</td>
						<td>{{ $row->kotaorkabupaten}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Provinsi', (isset($fields['provinsi']['language'])? $fields['provinsi']['language'] : array())) }}</td>
						<td>{{ $row->provinsi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kodepos', (isset($fields['kodepos']['language'])? $fields['kodepos']['language'] : array())) }}</td>
						<td>{{ $row->kodepos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Organizationname', (isset($fields['organizationname']['language'])? $fields['organizationname']['language'] : array())) }}</td>
						<td>{{ $row->organizationname}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Organizationscope', (isset($fields['organizationscope']['language'])? $fields['organizationscope']['language'] : array())) }}</td>
						<td>{{ $row->organizationscope}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Organization Experience Period', (isset($fields['organization_experience_period']['language'])? $fields['organization_experience_period']['language'] : array())) }}</td>
						<td>{{ $row->organization_experience_period}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rolefunction', (isset($fields['rolefunction']['language'])? $fields['rolefunction']['language'] : array())) }}</td>
						<td>{{ $row->rolefunction}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Roleorposition', (isset($fields['roleorposition']['language'])? $fields['roleorposition']['language'] : array())) }}</td>
						<td>{{ $row->roleorposition}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Company', (isset($fields['company']['language'])? $fields['company']['language'] : array())) }}</td>
						<td>{{ $row->company}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Workingexperienceperiod', (isset($fields['workingexperienceperiod']['language'])? $fields['workingexperienceperiod']['language'] : array())) }}</td>
						<td>{{ $row->workingexperienceperiod}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Position', (isset($fields['position']['language'])? $fields['position']['language'] : array())) }}</td>
						<td>{{ $row->position}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['category']['language'])? $fields['category']['language'] : array())) }}</td>
						<td>{{ $row->category}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status Working Experience', (isset($fields['status_working_experience']['language'])? $fields['status_working_experience']['language'] : array())) }}</td>
						<td>{{ $row->status_working_experience}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jobdescription', (isset($fields['jobdescription']['language'])? $fields['jobdescription']['language'] : array())) }}</td>
						<td>{{ $row->jobdescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Salary', (isset($fields['salary']['language'])? $fields['salary']['language'] : array())) }}</td>
						<td>{{ $row->salary}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Reasonofleaving', (isset($fields['Reasonofleaving']['language'])? $fields['Reasonofleaving']['language'] : array())) }}</td>
						<td>{{ $row->Reasonofleaving}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Careerpreference', (isset($fields['Careerpreference']['language'])? $fields['Careerpreference']['language'] : array())) }}</td>
						<td>{{ $row->Careerpreference}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Expectedsalary', (isset($fields['expectedsalary']['language'])? $fields['expectedsalary']['language'] : array())) }}</td>
						<td>{{ $row->expectedsalary}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Additionalinformation', (isset($fields['additionalinformation']['language'])? $fields['additionalinformation']['language'] : array())) }}</td>
						<td>{{ $row->additionalinformation}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Cv', (isset($fields['cv']['language'])? $fields['cv']['language'] : array())) }}</td>
						<td>{{ $row->cv}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	