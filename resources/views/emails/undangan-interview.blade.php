<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Selamat! Karena Anda telah LOLOS proses sebelumnya untuk posisi <b><i>{{ $job }} {{ $placement }}</i></b>. Untuk itu kami akan mengundang Anda kembali untuk mengikuti Proses {{$name}} yang akan diselenggarakan pada :</p>
										<table>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Hari, Tanggal</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $scheduled_date }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Waktu</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $times }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Tempat</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $place }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">PIC</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $pic }}</td>
											</tr>
										</table>
										<p>Mohon datang 30 menit lebih awal sebelum Proses {{$name}}  dimulai.<br/>
										Jika Anda berniat untuk mengubah jadwal {{$name}} ini, mohon menghubungi career@acc.co.id atau telepon di nomor 021-7885 9000  Ext. 1551/1502/1550. </p>
										<p>Mohon berhati-hati untuk penipuan proses recruitment dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutment akan dikirimkan melalui email dengan domain “@acc.co.id”. </p>
										<p>Terima kasih sekali lagi atas ketertarikan Anda bekerja di ACC. <i>See you on {{ $scheduled_date }}!</i></p>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Congratulation! You have PASSED our previous session for <b><i>{{ $job }} {{ $placement }}</i></b>. Therefore we would like to invite you again for {{$name}} Session which will be held on :</p>
										<table>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Day, Date</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $scheduled_date }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Time</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $times }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">Place</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $place }}</td>
											</tr>
											<tr>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">PIC</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">:</td>
												<td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">{{ $pic }}</td>
											</tr>
										</table>
										<p>Please come 30 minutes earlier before we start the {{$name}} Session.<br/>
										If you are about to reschedule this process, please contact career@acc.co.id or call 021-7885 9000  Ext. 1551/1502/1550.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Once again thank you for your interest working in ACC. <i>See you on {{ $scheduled_date }}!</i></p>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>