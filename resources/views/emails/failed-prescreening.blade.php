<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Terima kasih atas ketertarikan Anda bergabung di Astra Credit Companies (ACC) sebagai <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>Mohon maaf, kami memberitahukan bahwa posisi <b><i>{{ $job }} {{ $placement }}</i></b> yang Anda lamar telah terisi dan saat ini kami sedang tidak memiliki posisi lain yang cocok dengan kualifikasi Anda. Namun, kami akan tetap menyimpan data diri Anda, sehingga jika ada posisi lain yang sekiranya cocok untuk Anda, maka kami akan memberitahu Anda kembali.</p>
										<p>Mohon berhati-hati untuk penipuan proses recruitment dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutment akan dikirimkan melalui email dengan domain “@acc.co.id”. </p>
										<p>Terima kasih sekali lagi atas ketertarikan Anda bekerja di ACC. <i>We wish you all the best for your career journey!</i></p>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Thank you for your interest in joining Astra Credit Companies (ACC) as <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>Unfortunately we inform you that <b><i>{{ $job }} {{ $placement }}</i></b> that you applied has already been fulfilled and at present we have no opportunities that match with your qualification. However, we will save your profile therefore if there are any other position that qualified with you, we will gladly inform you back.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Once again thank you for your interest working in ACC. <i>We wish you all the best for your career journey!</i></p>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>