<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Selamat!  Karena Anda telah sampai di tahap terakhir proses recruitment untuk posisi <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>Dalam tahap akhir kali ini, kami mengundang Anda untuk melakukan Medical Check Up di {{ $place }} pada {{ $scheduled_date }}. Mohon berpuasa selama 8-12 jam sebelum melakukan Medical Check Up dan tidak boleh minum selain air putih. Bagi para wanita yang sedang datang bulan tidak diperkenankan untuk melakukan Medical Check Up dan harap reschedule jadwal Medical Check Up nya kepada tim Recruitment ACC.</p>
										<p>Kami pun mengundang Anda untuk mengumpulkan kelengkapan dokumen calon karyawan di sistem kami www.career.acc.co.id. Berikut adalah dokumen-dokumen yang harus dipersiapkan :</p>
										<ol>
											<li>Copy Ijazah dan Transkip Nilai *</li>
											<li>Copy Sertifikat Kursus / Training</li>
											<li>Surat keterangan catatan kepolisian (SKCK) *</li>
											<li>Copy KTP *</li>
											<li>Copy SIM</li>
											<li>Copy Paspor </li>
											<li>Copy Akte Nikah (apabila sudah menikah)</li>
											<li>Copy Kartu Keluarga yang terbaru (NIK harus sama dengan KTP) *</li>
											<li>Copy Kartu NPWP *</li>
											<li>Copy Bank Permata *</li>
											<li>Pas Photo (3x4 latar belakang orange) *</li>
											<li>Copy Akte Kelahiran sendiri* dan akte kelahiran anak (apabila sudah mempunyai anak)</li>
											<li>Copy Kartu BPJS  (apabila sudah memiliki kartu BPJS, WAJIB untuk upload copy kartu BPJS nya)</li>
										</ol>
										<p>Perlu diperhatikan bahwa berkas yang diberi tanda bintang (*) merupakan berkas wajib yang harus Anda upload ke dalam sistem kami. Untuk informasi mekanisme upload berkas dapat Anda baca di SINI.</p>
										<p>Jika ada pertanyaan lebih lanjut, dapat langsung menghubungi kami di 021-7885 9000 Ext. 1551/1502/1550.</p>
										<p>Mohon berhati-hati untuk penipuan proses recruitment dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutment akan dikirimkan melalui email dengan domain “@acc.co.id”. </p>
										<p>Terima kasih</p>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Congratulations! You have been on the last stage of recruitment process for <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>On this stage, you are invited to do a Medical Check Up at {{$place}} on {{ $scheduled_date }}. Please do fasting for 8-12 hours before you do the Medical Check Up. You are not allowed to drink any other beverages besides mineral water. For ladies who are on their menstruation period are not allowed to do this medical check up and we suggest you to reschedule to our Recruitment Team.</p>
										<p>We also ask you to fill your documentation by uploading some documents on our system www.career.acc.co.id. Here are some documents that you need to prepare :</p>
										<ol>
											<li>Copy of Ijazah dan Transkip Nilai *</li>
											<li>Copy of Training / Course Certificate</li>
											<li>Surat keterangan catatan kepolisian (SKCK) *</li>
											<li>Copy of ID (KTP) *</li>
											<li>Copy of Driving License (SIM)</li>
											<li>Copy of Passport</li>
											<li>Copy of Marriage Certificate (If you are married)</li>
											<li>Copy of Kartu Keluarga  (NIK should be similar to your ID) *</li>
											<li>Copy of NPWP *</li>
											<li>Copy of Permata Bank Statement *</li>
											<li>Photo (3x4 with Orange Background) *</li>
											<li>Copy of Birth Certificate* and your children’s birth certificate (apabila sudah mempunyai anak)</li>
											<li>Copy of BPJS (If you have it before, you MUST upload to the system)</li>
										</ol>
										<p>We remind you that all of the documents above with (*) marks are mandatory to be uploaded to the system. You can read further information of uploading mechanism in HERE.</p>
										<p>If you have any questions regarding to this process, please contact career@acc.co.id or call 021-7885 9000  Ext. 1551/1502/1550.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Thank you.</p>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>