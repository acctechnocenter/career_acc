<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p><b>SELAMAT!</b></p>
										<p>Karena Anda telah menyelesaikan seluruh Proses Recruitment untuk posisi <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>Dengan email ini kami menginformasikan bahwa Anda sudah bisa bergabung di ACC {{ $placement }} per {{ $scheduled_date }} dan langsung menemui {{ $pic }} yang akan menjadi user Anda.</p>
										<p>Mohon jika Anda telah memiliki rekening payroll dari Bank Permata, maka Anda harus mendaftarkan nomor rekening ke sistem Talent maksimal sampai tanggal {{ $date }}, dan kemudian fotokopi halaman depan buku tabungan Anda dan serahkan kepada PIC Payroll (Ibu Lucy / Kesti). Karyawan baru pun harus mengembalikan PKWT kerja kepada PIC Payroll maksimal H+7 setelah tanggal join date. Apabila tidak mengembalikan PKWT, maka Anda tidak akan dapat diproses untuk proses penggajian.</p>
										<p>Mohon berhati-hati untuk penipuan proses rekrutmen dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutmen akan dikirimkan melalui email dengan domain “@acc.co.id”.</p>
										<p>Terima kasih.</p>
										<p>Sekali lagi, selamat!<br/>
										Selamat datang di keluarga ACC!</p>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p><b>CONGRATULATIONS!</b></p>
										<p>You have passed all of the Recruitment Process for <b><i>{{ $job }} {{ $placement }}</i></b>.</p>
										<p>By this email, we gladly inform you that you are able to join in ACC {{ $placement }} on {{ $scheduled_date }} and you can directly meet {{ $pic }} as your user.</p>
										<p>If you have already had Permata Bank Payroll Account, please enroll your account on our Talent System no later than {{ $date }} and then send your copy of Permata Bank Statement to our Payroll Staffs (Ibu Lucy / Kesti). We also inform you to send back PKWT to our Payroll Staffs no later than after 7 days of your join date. If you don’t send back the PKWT, your salary would not be processed.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Once again, congratulation!<br/>
										Welcome to ACC family.</p>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>