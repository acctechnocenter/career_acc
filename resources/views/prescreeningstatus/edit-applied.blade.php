@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url('list-prescreening-result') }}"> Prescreening Result </a></li>
		<li><a href="{{url('list-prescreening-result/'.$statusRes.'/'.$rowData->id_job) }}">Prescreening Result : {{ $rowData->job_title }} </a></li>
		<li class="active"> Edit  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
{!! Form::open(array('url'=>'save-prescreening-status', 'class'=>'form-horizontal validated' )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('list-prescreening-result/'.$statusRes.'/'.$rowData->id_job) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<!-- <button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button> -->
				<button type="submit" name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.sb_save') }}" onclick="this.disabled=true;this.form.submit();" ><i class="fa  fa-check"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Prescreening Status</legend>	
							{!! Form::hidden('id', $rowData->id_pre_status) !!}
							{!! Form::hidden('id_job', $rowData->id_job) !!}
							{!! Form::hidden('id_user', $rowData->id_user) !!}
							{!!  Form::hidden('statusRes', $statusRes) !!}
									  <div class="form-group  " >
										<label for="Applicants" class=" control-label col-md-4 text-left"> Applicants </label>
										<div class="col-md-6">
										  <p>{{ $rowData->first_name }} {{ $rowData->last_name }}</p>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Job" class=" control-label col-md-4 text-left"> Job </label>
										<div class="col-md-6">
										  <p>{{ $rowData->job_title }}</p>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Skor" class=" control-label col-md-4 text-left"> Skor </label>
										<div class="col-md-6">
										  <p>{{ $rowData->skor_akhir }}</p>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 	
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					<?php
					$type_opt = array( 'pending' => 'Pending' ,  'disqualified' => 'Disqualified' , 'accepted' => 'Accepted', 'failed' => 'Failed', ); ?>
					<select name='type' rows='5'   class='select2 '  > 
						<?php 
						foreach($type_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($rowData->status_prescreening == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 									  													
						</fieldset>
			</div>
			
		
		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop