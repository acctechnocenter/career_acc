@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<?php $arrStat = array('accepted','failed'); ?>
<section class="page-header row">
	<h2> {{ $pageTitle }} : {{ $jobDetail->JobTitleName }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url('list-prescreening-result') }}"> Prescreening Result </a></li>
		<li class="active"> {{ $pageTitle }} : {{ $jobDetail->JobTitleName }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
			<div class="sbox-title">
			<div class="ajaxLoading"></div>
				<h1> All Records <small> </small></h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('list-prescreening-result/'.$statusRes.'/'. $jobDetail->id) }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
						<!-- <a href="{{ url('sximo/module/config/'.$pageModule) }}" class="tips btn btn-sm  " title=" {{ __('core.btn_config') }}" ><i class="fa  fa-ellipsis-v"></i></a> -->
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<!-- Toolbar Top -->
			<div class="row">
				<div class="col-md-4">
					<a href="{{ url('list-prescreening-result') }}" class="btn btn-default btn-sm"  
						title="Back"><i class=" fa fa-arrow-left "></i> Back</a>
					@if(!in_array($statusRes,$arrStat))
					<div class="btn-group">
						<button type="button" id="bulkButton" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk Change Status </button>
				        <ul class="dropdown-menu">
							<li><a class="bulkStat" id="accepted">Accepted</a></li>	
							<li><a class="bulkStat" id="failed">Failed</a></li>
							<li><a class="bulkStat" id="disqualified">Disqualified</a></li>	
							<li><a class="bulkStat" id="pending">Pending</a></li>		          
				        </ul>
				    </div>   
					@endif
				</div>    
				<div class="col-md-1 pull-right">
					<div class="input-group">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default btn-sm " 
					        onclick="SximoModalTest('{{url('search-detail-prescreening/'.$statusRes.'/'.$jobDetail->id)}}','Advance Search'); " ><i class="fa fa-filter"></i> Filter </button>
					      </div><!-- /btn-group -->
			<!-- <input type="text" class="form-control input-sm onsearch" data-target="{{ url($moduleJob) }}" aria-label="..." placeholder=" Type And Hit Enter "> -->
					    </div>
				</div>  
			</div>			
			<!-- End Toolbar Top -->

			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						@if(!in_array($statusRes,$arrStat))
						<th  style="width: 3% !important;"></th>
						@endif
						<th align="center">Applicants</th>
						<th align="center">Applied Date</th>
						<th align="center">Skor </th>
						<th align="center">Status</th>
						<th align="center">Action</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							@if(!in_array($statusRes,$arrStat))
							<td ><input type="checkbox" class="ids" name="ids[]" value="{{ $row->id_status}}" id="state-{{ $row->id_status }}" />  </td>
							@endif
							<td><a class="link-applicant" href="{{ url('applicant-menu/ApplicantDetail/'.$row->applicants) }}" target="_blank" style="text-decoration: underline;" title="View Applicant Detail">{{ $row->first_name }} {{ $row->last_name }}</a></td>
							<td>{{ $row->apply_date }}</td>
							<td>{{ $row->skor }}</td>
							<td>{{ $row->status }}</td>
							<td>
							 	<div class="dropdown">
								  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> Action </button>
								  <ul class="dropdown-menu">
								  @if(!in_array($statusRes,$arrStat))
									<li><a  href="{{ url('edit-prescreening-result/'.$statusRes.'/'.$row->id_status) }}" class="tips"> Change Status</a></li>
								 @endif
									<li><a  href="{{ url('prescreening/'.$statusRes.'/'.$row->applicants.'/'.$row->id_job) }}"  class="tips"> View Prescreening Result</a></li>
								  </ul>
								</div>
							</td>	
						</tr>
					@endforeach
				@else
						<tr>
							<td colspan="5">No data</td>
						</tr>
				@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->


			</div>
		</div>
	</div>
</div>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <style>
 .dataTables_filter {
    display: initial; 
}
</style>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
		$('#bulkButton').prop('disabled', true);
    $('#prescreeningresultTable').DataTable();
});

$("input[name='ids[]']").change(function () {
	  var minAllowed = 2;
	  var maxAllowed = 5;
	   var cnt = $("input[name='ids[]']:checked").length;
	      if (cnt > maxAllowed || cnt < minAllowed)
	      {
	         $('#bulkButton').prop('disabled', true);
			 if(cnt > maxAllowed){
				 alert('Only allowed '+maxAllowed+' result to process');
			 }
	     } else {
			 $('#bulkButton').prop('disabled', false);
		 }
	  });


$(".bulkStat").click(function(){	
	$('#bulkButton').prop('disabled', true);
	var selected = new Array();
	var stat = $(this).attr('id');
	$('input[name="ids[]"]:checked').each(function() {
		selected.push(this.value);
	});
	
	if(confirm("Are you sure change the status?")){
	$('.ajaxLoading').show();
	$.ajax({
		url     : "{{ url('bulk-save-prescreening') }}",
		method  : "POST",
		data    : {"hasil" : selected, "stat" : stat} ,
					success : function(){
						$('.ajaxLoading').hide();
						location.reload(true);
						}, 
						error : function(){
						$('.ajaxLoading').hide();
						alert('Error');
						}
		});
	}	
});
	
	function SximoModalTest( url , title, link )
		{
			
			$('#sximo-modal-content').html(' ....Loading content , please wait ...');
			$('.modal-title').html(title);
			$('#sximo-modal-content').load(url,function(){
			});
			$('#sximo-modal').modal('show');	
		}		
		
</script>

@stop