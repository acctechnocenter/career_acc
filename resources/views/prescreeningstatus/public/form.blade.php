

		 {!! Form::open(array('url'=>'prescreeningstatus', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Prescreening Status</legend>
									
									  <div class="form-group  " >
										<label for="Job" class=" control-label col-md-4 text-left"> Job <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_job' id='id_job' value='{{ $row['id_job'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Applicants" class=" control-label col-md-4 text-left"> Applicants <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id_user' id='id_user' value='{{ $row['id_user'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Skor" class=" control-label col-md-4 text-left"> Skor <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='skor_akhir' id='skor_akhir' value='{{ $row['skor_akhir'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $status_prescreening = explode(',',$row['status_prescreening']);
					$status_prescreening_opt = array( 'pending' => 'Pending' ,  'accepted' => 'Accepted' ,  'disqualified' => 'Disqualified' , ); ?>
					<select name='status_prescreening' rows='5'   class='select2 '  > 
						<?php 
						foreach($status_prescreening_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['status_prescreening'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
