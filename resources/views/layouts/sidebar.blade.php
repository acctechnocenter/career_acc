<?php $sidebar = SiteHelpers::menus('sidebar');
if(!\Auth::check()){
	return redirect('user/login')->with('status', 'error')->with('message','You are not login');}
$userId = Auth::user()->id;
$check= AccHelpers::acc_validate($userId);
$is_user= AccHelpers::is_user($userId);
?>
<div id="sidebar-navigation">
    <div class="logo">    
         <a href="<?php if(!$is_user){echo url('dashboard') ;}?>">           
            @if(file_exists(public_path().'/uploads/images/'.config('sximo.cnf_logo') ) && config('sximo.cnf_logo') !='')
                <img src="{{ asset('frontend/default/images/logo-acc.png') }}" alt="{{ config('cnf_appname') }}" style="width: 110px;height: 46px;"/>
            @else
            {{ config('sximo.cnf_appname')}} 
            @endif  
        </a>    
    </div>
    <div class="sidebar-collapse">
    <nav role="navigation" class="navbar-default ">
       <ul id="sidemenu" class="nav expanded-menu">
			<li>
				<a data-toggle="tooltip" title="Profile" data-placement="right" href="{{ url('user/profile') }}">
					<i class="fa fa-user"></i> 
					<span class="nav-label">Profile</span>
				</a>
			</li>
        @foreach ($sidebar as $menu)
            

             <li @if(Request::segment(1) == $menu['module']) class="active" @endif>

            @if($menu['module'] =='separator')
            <li class="separator"> <span> {{$menu['menu_name']}} </span></li>
                
            @else
                <a data-toggle="tooltip" title="{{  $menu['menu_name'] }}" data-placement="right"
                    @if($menu['menu_type'] =='external')
                        href="{{ $menu['url'] }}" 
                    @else
                        href="{{ URL::to($menu['module'])}}" 
                    @endif              
                
                 @if(count($menu['childs']) > 0 ) class="expand level-closed" @endif>
                    <i class="{{$menu['menu_icons']}}"></i> 
                    <span class="nav-label">                    
                        {{ (isset($menu['menu_lang']['title'][session('lang')]) ? $menu['menu_lang']['title'][session('lang')] : $menu['menu_name']) }}
                    </span> 
                    @if(count($menu['childs']))<span class="fa arrow"></span> @endif    
                </a> 
                @endif  
                @if(count($menu['childs']) > 0)
                    <ul class="nav nav-second-level">
                        @foreach ($menu['childs'] as $menu2)
                         <li @if(Request::segment(1) == $menu2['module']) class="active" @endif>
                            <a  
                                @if($menu2['menu_type'] =='external')
                                    href="{{ $menu2['url']}}" 
                                @else
                                    href="{{ URL::to($menu2['module'])}}"  
                                @endif                                  
                            >
                            
                            <i class="{{$menu2['menu_icons']}}"></i>
                           {{ (isset($menu2['menu_lang']['title'][session('lang')]) ? $menu2['menu_lang']['title'][session('lang')] : $menu2['menu_name']) }}
                            </a> 
                            @if(count($menu2['childs']) > 0)
                            <ul class="nav nav-third-level">
                                @foreach($menu2['childs'] as $menu3)
                                    <li @if(Request::segment(1) == $menu3['module']) class="active" @endif>
                                        <a 
                                            @if($menu['menu_type'] =='external')
                                                href="{{ $menu3['url'] }}" 
                                            @else
                                                href="{{ URL::to($menu3['module'])}}" 
                                            @endif                                      
                                        
                                        >
                                       
                                        <i class="{{$menu3['menu_icons']}}"></i> 
                                        {{ (isset($menu3['menu_lang']['title'][session('lang')]) ? $menu3['menu_lang']['title'][session('lang')] : $menu3['menu_name']) }}                                         
                                            
                                        </a>
                                    </li>   
                                @endforeach
                            </ul>
                            @endif                          
                        </li>                           
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
		@if($check)
		<li>
			<a data-toggle="tooltip" title="Prescreening Result" data-placement="right" href="{{ url('list-prescreening-result') }}">
				<i class="fa fa-desktop"></i> 
				<span class="nav-label">Prescreening Result</span>
			</a>
		</li>
		<li>
			<a data-toggle="tooltip" title="Prescreening Questions" data-placement="right" href="{{ url('prescreening-management') }}">
				<i class="fa fa-desktop"></i> 
				<span class="nav-label">Prescreening Questions</span>
			</a>
		</li>
		<li>
			<a data-toggle="tooltip" title="Applicants" data-placement="right" href="{{ url('applicant-menu') }}">
				<i class="fa fa-address-book-o"></i> 
				<span class="nav-label">Applicants</span>
			</a>
		</li>
		<li>
			<a data-toggle="tooltip" title="News" data-placement="right" href="{{ url('core/posts') }}">
				<i class="fa fa-file"></i> 
				<span class="nav-label">News</span>
			</a>
		</li>
		@endif
		<li>
			<a data-toggle="tooltip" title="Applied Job" data-placement="right" href="{{ url('history-prescreening') }}">
				<i class="fa fa-desktop"></i> 
				<span class="nav-label">Applied Job</span>
			</a>
		</li>
    </ul>   
    </nav>
    </div>
</div>