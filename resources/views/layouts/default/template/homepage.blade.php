<section id="slider" class="slider-parallax" >
  
   <div id="home-slider" class="owl-carousel owl-slider" style="background-color: #f0f3f3;">
      @foreach ($sliders as $slider)
      <a href="{{$slider->link}}"><div class="item">
	<img src="{{ asset('uploads/slider/'.$slider->image) }}" />
      </div></a>
       @endforeach
    </div>
</section>
  <section>
    <div>
	   <div class="search-div">
         <div class="input-group row" style="margin: 2% auto !important; width: 50%;">
         	<form action="{{url('search')}}" method="POST" role="search">
         	{{ csrf_field() }}
         	<div class="col-md-10 padding-0">
             <input type="text" class="search-query form-control" placeholder="Search your job" name="q" />
             <div style="background-color: #555;">
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 110px;" name="jf">
						<option value="" selected disabled>Job Field</option>
						@if (count($jobFieldList) > 0)
							@foreach($jobFieldList as $optionField)
								<option value="{{ $optionField->id }}">{{ $optionField->jobfield }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 140px;" name="jc">
						<option value="" selected disabled>Job Category</option>
						@if (count($jobCatList) > 0)
							@foreach($jobCatList as $optionCat)
								<option value="{{ $optionCat->JobCategory }}">{{ $optionCat->JobCategory }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search" style="width: 120px;" name="loc">
						<option value="" selected disabled>Location</option>
						@if (count($locList) > 0)
							@foreach($locList as $optionLoc)
								<option value="{{ $optionLoc->id }}">{{ $optionLoc->name }}</option>
							@endforeach
						@endif
 					</select>
				</div>
             </div>
             </div>
             
             <div class="input-group-btn col-md-2 padding-0" style="text-align: center;">
                 <button class="btn btn-primary search-job-btn" type="submit" style="height:68px;">
                     <span>GET YOUR JOB!</span>
                 </button>
             </div>
             </form>
         </div>
		</div>
     </div>
</section>
  
  <section class="section  bg-light filtercategory" style="padding-top:10px!important; background-color:#fff!important;    padding-bottom: 40px;">
    <!-- WELCOME -->
      <div class="container filtercategory">
        <br>
          &nbsp;
          &nbsp;
          <div>
          	<h4 style="font-weight: bold; text-align:center;">JOB FIELDS</h4>
              <p class="col-md-12" style="margin-bottom: 20px!important;">
                <a href="{{ url('job-list') }}" class="btn btn-default">More Jobs >></a>
              </p>
          </div>

          <br/>
          @if (count($rowData) >= 1)
		@foreach($rowData->chunk(4) as $chunk)
          <div class="row">
          	@foreach($chunk as $items)
              <div class="col-md-3 filter photo" style="margin: 10px 0; padding-top:10px !important;">
              <a  class="link-effect-winston" href="{{ url('job-list/'. $items->id)}}">
              <figure class="effect-winston">
              		@if($items->img_jobfield)
              		<img class="port-image" src="{!! asset('uploads/jobfield/'.$items->img_jobfield.'') !!}"/>
					@else
					<img class="port-image" src="{!!  asset('uploads/jobs/1518592336-14524170.jpg') !!}"/>
					@endif
              		@if(strlen($items->jobfield) < 30)
              			<p style="font-size: 16px"><b>{{ $items->jobfield }}</b></p>
						@else
						<p style="font-size: 14px"><b>{{ $items->jobfield }}</b></p>
						@endif
              </figure>
              </a>
              </div>
             @endforeach
          </div>
		  @endforeach
          &nbsp;
          @else
    		<p>No Data</p>
    	  @endif
          </div>
      <!-- /WELCOME -->
  </section>
  
<section>
	<div><p class="title-bagianblog">LATEST NEWS</p></div>
 <div class="section bagianblog">    

<table border="1">	
        @foreach (array_slice($newskotak->toArray(), 0, 7) as $newshome)
            		<div class="col-md-3 imagetitlecolor" style="padding-left: 0px;padding-right: 0px; height:285px;">
            		 <div class="nobottomborder" style="margin-bottom: 15px;">
            		 <a class="link-news" href="{{ url('posts/'.$newshome->alias) }}">
            			 <img src="{{ asset('uploads/images/'.$newshome->image) }}" alt="{{ config('cnf_appname') }}" style="width: 482px;
                          height: 210px;"/>
                          <h5 class="link-news-title" ><strong>{{$newshome->title}}</strong></h5>
                          </a>
                     </div>
                    </div>          
        @endforeach
        <div class="col-md-3" style="padding-left: 0px;padding-right: 0px; height:285px!important; background-color:#0d9481!important">
            <div class="newsone">
              <h4 style="text-align: left;font-weight:bold;color: white;">STAY TUNE WITH US!</h4>
              <h5 style="text-align: left;color: white;">Get of all our news and </h5>
              <h5 style="text-align: left;color: white;">information from Career</h5>
              <h5 style="text-align: left;color: white;">in ACC</h5>
			  <a href="{{ url('posts') }}" style="color: #ffffff;">
              <button class="btn btn-danger seemore-btn" type="button" >See More News</button></a>
            </div>
          </div> 
        
</table>
</div>       
</section>

<!-- TESTIMONIALS -->
<section style="padding-bottom: 50px; clear:both; background-color: #f7f9fa;">
	<div class="container" >
		<h4 style="font-weight: bold; text-align:center;margin-top: 30px;margin-bottom: 30px;">TESTIMONIALS</h4>
      <div class="row">
        <div class="col-sm-12">
          <div id="customers-testimonials" class="owl-carousel">

            <!--TESTIMONIAL 1 -->
            @foreach ($testimonials as $testi)
            <div class="item">
              <div class="shadow-effect">
                <img class="img-responsive gambar" src="{{ asset('uploads/testimonial/'.$testi->photo) }}" alt="">
                <div class="item-details cst-testi-items">
									<h3>{{$testi->name}}</h3>
									<h5>{{$testi->department}}</h5>
									<p>"{{$testi->description}}"</p>
								</div>
              </div>
            </div>
            @endforeach
            <!--END OF TESTIMONIAL 1 -->

          </div>
        </div>
      </div>
      </div>
    </section>
    <!-- END OF TESTIMONIALS -->
<!--end-->
<section>
		<div class="row" style="margin:0;">
			<div class="col-md-12" style="padding:0;">
				<img src="uploads/config/footer.png" alt="Flowers in Chania" style="background-size: contain; width:100%">
			</div>
      	</div>
</section>



    <script>
    $(document).ready(function(){

        $(".filter-button").click(function(){
            var value = $(this).attr('data-filter');

            if(value == "all")
            {
                $('.filter').show('1000');
            }
            else
            {
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');

            }
        });

    });

    jQuery(document).ready(function($) {
    	"use strict";
    	$('#customers-testimonials').owlCarousel( {
    			loop: true,
    			center: true,
    			items: 3,
    			margin: 30,
    			autoplay: true,
    			dots:true,
    	    nav:true,
    			autoplayTimeout: 8500,
    			smartSpeed: 450,
    	  	navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    			responsive: {
    				0: {
    					items: 1
    				},
    				768: {
    					items: 2
    				},
    				1170: {
    					items: 3
    				}
    			}
    		});
    	});
    </script>