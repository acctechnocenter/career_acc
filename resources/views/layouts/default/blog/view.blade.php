<section id="blog" style="padding:20px 0;">
      <!-- Container Starts -->
      <div class="container">  

       
        <!-- Row Starts -->
        <div class="row">  
            <div class="col-md-9">
                <div class="" style="margin: 0px 0 20px;">
                <h1 class=" animated" style="visibility: visible;">
                    {{ $posts->title}}
                </h1>   
                    <!-- penambahan gambar -->
                    @if(file_exists('./uploads/images/'.$posts->image) && $posts->image !='' )
                    <div align="middle">
                        <br />
                              <img src="{{ asset('uploads/images/'.$posts->image) }}" alt="" class="img-responisve" style="width: 100%;
                    height: auto;">
                    </div>
                    @endif
                <div class="section-tool text-left ">
                    <br>
                    <i class="fa fa-eye "></i>  <span>  Views (<b> {{ $posts->views }} </b>)  </span>   
                    <i class="fa fa-user "></i>  <span>  {{ ucwords($posts->username) }}  </span>   
                    <i class="icon-calendar3"></i>  <span> {{ date("M j, Y " , strtotime($posts->created)) }} </span> 
                    <i class="fa fa-comment-o "></i>   <span>  {{ $posts->comments }} comment(s)  </span> 
                </div>

                <div>
                  @if(file_exists('./uploads/images/posts/'.$posts->image) && $posts->image !='' )
                  <img src="{{ asset('uploads/images/posts/'.$posts->image) }}" alt="" class="img-fluid img-responsive">
                  @endif
                </div>                                  
                  {!! PostHelpers::formatContent($posts->note) !!}  
                </div>  

                
                <h4 class="blog-item-comment-title"><i class="icon-comment"></i> Comments </h4>
                @foreach($comments as $comm)
                <div class="blog-item-comments">
                   
                    <div class="avatar">
                    <?php if( file_exists( './uploads/users/'.$comm->avatar) && $comm->avatar !='') { ?>
                        <img src="{{ asset('uploads/users').'/'.$comm->avatar }} " border="0" width="60" class="img-circle" />
                    <?php  } else { ?> 
                        <img alt="" src="http://www.gravatar.com/avatar/{{ md5($comm->email) }}" width="60" class="img-circle" />
                    <?php } ?> 
                    </div>
                    <div class="content">
                         <div class="info" >
                            {{ $comm->comment_name }} | 
                            {{ date("M j, Y " , strtotime($comm->posted)) }}
                        </div>
                        {!! PostHelpers::formatContent($comm->comments) !!}
                        <?php /* <div class="tools">
                            @if(Session::get('gid') == '1' OR $comm->userID == Session::get('uid')) 
                            <a href="{{ url('posts/remove/'.$posts->pageID.'/'. $posts->alias.'/'.$comm->commentID) }}" class="text-danger remove"><i class="fa fa-minus-circle"></i> Remove  </a>
                            @endif
                        </div> */ ?>
                    </div> 
                </div>
                @endforeach
                <div class="blog-item-comments">
                   
                    <div class="avatar">
                        {!! SiteHelpers::avatar('60') !!}    
                    </div>
                    <div class="content">
                        <h4> Leave Comment </h4>
                         <form method="post"  action="{{ url('posts/comment') }}" parsley-validate novalidate class="form">
                        {{ csrf_field() }}
						@if(Session::has('message'))
						{!! Session::get('message') !!}
						@endif
						<ul class="parsley-error-list">
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
							@if(!Auth::check())
							<input class="form-control" type="text" placeholder="Your name" required name="name" /><br/>
							<input class="form-control" type="email" placeholder="Email" required name="email" /><br/>
							@else
								<?php $userInfo = AccHelpers::getUserComment(\Auth::user()->id);?>
								<input class="form-control" type="hidden" placeholder="Your name" required name="name" value="{{$userInfo->first_name}} {{$userInfo->last_name}}"  /><br/>
								<input class="form-control" type="hidden" placeholder="Email" required name="email" value="{{ $userInfo->email}}"  /><br/>
							@endif
                            <textarea rows="5" placeholder="Leave comments here ...." class="form-control " required name="comments"></textarea><br />
							<div class="form-group has-feedback  animated fadeInLeft delayp1">
									<label class="text-left"> Are u human ? </label>	
									<div class="g-recaptcha" data-sitekey="{{config('sximo.cnf_recaptchapublickey')}}"></div>
									<div style="clear:both;"></div>
								</div>
                            <button type="submit" class="btn btn-primary "> Submit Comment </button>    
                            <input type="hidden" name="pageID" value="{{ $posts->pageID }}" />    
                            <input type="hidden" name="alias" value="{{ $posts->alias }}" />                      
                        </form>
                    </div> 
                </div>

               


            </div>


          <div class="col-md-3">
            <h3> Categories </h3>
            <ul class="nav nav-list">
            <?php $categories = PostHelpers::cloudtags();
            foreach($categories as $cat) { ?>
                <!-- menghilangkan angka -->
              <li><a href="{{  url('posts?label='.$cat['tags']) }}"> {{ ucwords($cat['tags']) }}</a> </li>
            <?php } ?>
            </ul>

          </div>      
         </div> 
          

        </div><!-- Row Ends -->

      </div><!-- Container Ends -->
    </section>

    <script type="text/javascript">
        $(function(){
            $('.remove').on('click',function(){
                if(confirm('Remove comment ?'))
                {
                    return true;
                }
                return false;
            })
        })
    </script>