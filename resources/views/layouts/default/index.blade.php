<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ config('sximo.cnf_appname') }}</title>
    <link rel="shortcut icon" href="{{ asset('fav-icon.png')}}" type="image/x-icon">
    <!-- CSS Files -->
    <link href="{{ asset('frontend/default/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('frontend/default/style.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('sximo5/fonts/icomoon.css') }}" rel="stylesheet" />
    <link href="{{ asset('sximo5/fonts/awesome/css/font-awesome.css') }}" rel="stylesheet" />
	<link href="{{ asset('frontend/default/addons/jquery.smartmenus.bootstrap.css') }}" rel="stylesheet">
 <link href="{{ asset('sximo5/js/plugins/sweetalert/assets/css/sweetalert.css') }}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project --> 
     <script src="{{ asset('frontend/default/js/app.js') }}"></script>
       <script type="text/javascript" src="{{ asset('sximo5/js/plugins/sweetalert/src/sweetalert.min.js') }}"></script>
	 <script src='https://www.google.com/recaptcha/api.js'></script>
      



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script>
    <![endif]-->
    <style>
      .swal-text {
    font-size: 16px;
    position: relative;
    float: none;
    line-height: normal;
    vertical-align: top;
    text-align: center;
    display: inline-block;
    margin: 0;
    padding: 0 10px;
    font-weight: 400;
    color: rgba(0,0,0,.64);
    max-width: calc(100% - 20px);
    overflow-wrap: break-word;
    box-sizing: border-box;
}
    </style>
  </head>
  <body class="index-page sidebar-collapse">  

    <div id="header">
    <!-- Navbar -->
    <nav class="navbar navbar-default navbar-fixed-top bg-white" role="navigation">
        <div class="container full-cont top-header">
    	  <a class="navbar-brand" href="{{ url('') }}">
            <img src="{{ asset('frontend/default/images/logo-acc.png') }}" alt="{{ config('cnf_appname') }}" />
          </a>
    	</div>
      <div class="container full-cont bg-blue bottom-header">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div> 
         <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           @include('layouts.default.navigation')
        </div>
      </div>
    </nav>       
    <!-- End Navbar -->
  </div>    

    <!-- Main Content Begin Here -->
    <section id="main-content">
        @include($pages)
    </section></section>
    <!-- Main Content Begin Here -->


    <!-- Footer Section -->
    <footer>
      <!-- Container Starts -->
      <div class="container">
      </div><!-- Container Ends -->
      
      <!-- Copyright -->
      <div id="copyright">
        <div class="container">
          <div class="row text-left">            
              <p class="copyright-text col-md-6 footer-left">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
              </p>
              <p class="copyright-text col-md-6 text-right footer-right" style="font-size:25px;">
              <a href="https://www.facebook.com/ACC-Young-Community-1388582758107992/?ref=br_rs" target="_blank"><i class="fa fa-facebook-square fa-lg" style="padding-left:5px; color:#2e4da7!important"></i></a>
              <a href="https://www.instagram.com/lensacc/" target="_blank"><i class="fa fa-instagram fa-lg" style="padding-left:5px; color:#a438b1!important"></i></a>
              <a href="https://twitter.com/acc_community" target="_blank"><i class="fa fa-twitter fa-lg" style="padding-left:5px; color:#00aced!important"></i></a>
              <a href="https://www.linkedin.com/company/astra-credit-companies-acc-" target="_blank"><i class="fa fa-linkedin fa-lg" style="padding-left:5px; color:#0077b5!important"></i></a>
              <a href="https://www.youtube.com/channel/UC11uTes_-p_gN1csiz8CE1w" target="_blank"><i class="fa fa-youtube-play fa-lg" style="padding-left:5px; color:#fb0007!important"></i></a>
              
              </p>
            
          </div>
        </div>
      </div>
      <!-- Copyright  End-->
      
    </footer>
    <!-- Footer Section End-->

    <!-- JavaScript & jQuery Plugins -->
    <!-- jQuery Load -->
    
    <script src="{{ asset('frontend/default/js/script.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/default/js/jquery.smartmenus.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/default/addons/jquery.smartmenus.bootstrap.min.js') }}"></script>
  

	
<!--Start of Tawk.to Script-->
<script type="text/javascript">

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b8e1a6aafc2c34e96e82fc2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

  </body>
</html>