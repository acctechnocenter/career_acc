<?php  $menus = SiteHelpers::menus('top') ;
if(Auth::check()){
$userId = Auth::user()->id;
$is_user= AccHelpers::is_user($userId);
$full_profile= AccHelpers::flag_profile($userId);}
?>
<ul class="nav navbar-nav white-text custom-ul-header">
    <li><a href="{{ url('') }}"> Home</a></li>
    <li><a class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;"> Jobs  <span class="caret"></span></a>
		<ul class="dropdown-menu dropdown-menu-right">
		<li><a href="{{ url('/job-lists/category/experience') }}">Experience</a></li>
		<li>
			<a href="#">Fresh Graduate  <span class="caret" style="margin-left:2px;"></span></a>
			<ul class="dropdown-menu">
				<li><a href="{{ url('/job-lists/category/fresh-graduate/ldp-program') }}">LDP Program</a></li>
				<li><a href="{{ url('/job-lists/category/fresh-graduate/internship') }}">Internship</a></li>
				<li><a href="{{ url('/job-lists/category/fresh-graduate/staff') }}">Staff</a></li>				
			</ul>
		</li>
		</ul>
	</li>
    <li><a href="{{ url('faq-applicant') }}"> Faq</a></li>
    <li><a href="{{ url('contactus-ui') }}"> Contact Us</a></li>
    @foreach ($menus as $menu)
        @if($menu['module'] =='separator')
        <li class="divider"></li>        
        @else
            <li><!-- HOME -->
                <a 
                @if($menu['menu_type'] =='external')
                    href="{{ URL::to($menu['url'])}}" 
                @else
                    href="{{ URL::to($menu['module'])}}" 
                @endif
             
                 @if(count($menu['childs']) > 0 ) class="dropdown-toggle" data-toggle="dropdown" @endif>
                    <i class="{{$menu['menu_icons']}}"></i>                 
                    @if(config('sximo.cnf_multilang') ==1 && isset($menu['menu_lang']['title'][session('lang')]) && $menu['menu_lang']['title'][session('lang')]!='')
                        {{ $menu['menu_lang']['title'][session('lang')] }}
                    @else
                        {{$menu['menu_name']}}
                    @endif             
                    @if(count($menu['childs']) > 0 )
                     <span class="caret"></span>
                    @endif  
                </a> 
                @if(count($menu['childs']) > 0)
                <ul class="dropdown-menu dropdown-menu-right">
                @foreach ($menu['childs'] as $menu2)
                    @if($menu2['module'] =='separator')
                        <li class="divider"> </li>        
                    @else
                    <li class="
                        @if(count($menu2['childs']) > 0) dropdown-submenu @endif
                        @if(Request::is($menu2['module'])) active @endif">
                        <a 
                            @if($menu2['menu_type'] =='external')
                                href="{{ url($menu2['url'])}}" 
                            @else
                                href="{{ url($menu2['module'])}}" 
                            @endif
                                        
                        >
                            <i class="{{ $menu2['menu_icons'] }}"></i> 
                            @if(config('sximo.cnf_multilang') ==1 && isset($menu2['menu_lang']['title'][session('lang')]))
                                {{ $menu2['menu_lang']['title'][session('lang')] }}
                            @else
                                {{$menu2['menu_name']}}
                            @endif                        
                        </a>
                    @endif
                 @endforeach     
                </ul>
                @endif
            </li>
        @endif
    @endforeach 
    <li class="dropdown">
	@if(Auth::check())
		<?php $name= \DB::table('tb_users')->where('id', Auth::user()->id)->value('first_name'); ?>
        <a href="javascript://ajax" class="dropdown-toggle" data-toggle="dropdown" style="text-transform:none;">@if($full_profile)<i class="fa fa-exclamation" style="color:#ff4d4d;"></i>@endif Hi, {{ $name }} <span class="caret"></span> </a>
         <ul class="dropdown-menu ">
			@if(!$is_user)
            <li><a href="{{ url('dashboard') }}">Dashboard </a></li>
            <li class="divider"></li>
			@endif
           <li><a href="{{ url('user/profile?view=frontend') }}">@if($full_profile)<i class="fa fa-exclamation" style="color:#ff4d4d;"></i>@endif {{ __('core.m_profile') }} </a></li>
           
           <li><a href="{{ url('user/logout') }}"> {{ __('core.m_logout') }} </a></li>
		 </ul>
           @else
           <a href="javascript://ajax" class="dropdown-toggle" data-toggle="dropdown">My Account <span class="caret"></span> </a>
         <ul class="dropdown-menu ">
            <li><a href="{{ url('user/login') }}" onclick="SximoModal(this.href , '{{ __('core.signin') }}'); return false;"><i class="fa fa-lock"></i> {{ __('core.signin') }} </a></li>
            <li><a href="{{ url('user/register') }}" onclick="SximoModal(this.href , '{{ __('core.signup') }}'); return false;"><i class="fa fa-user-plus"></i> {{ __('core.signup') }} </a></li>
		 </ul> 
           @endif  
    </li>    
</ul>   

@if(Auth::check())
   @if($full_profile)
	<script type="text/javascript">
		//swal("Update Your profile", "Please complete your Personal Details and Educational Background.", "warning");
   swal({
title: "Update Your profile!",
text: "Please complete your Personal Details and Educational Background.",
icon: "warning"
}).then(function() {
// Redirect the user
window.location.href = "/user/profile?view=frontend";
});
	</script>
  @endif
@endif