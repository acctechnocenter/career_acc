

		 {!! Form::open(array('url'=>'faqapplicant', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> faqapplicant</legend>
									
									  <div class="form-group  " >
										<label for="CreatedDate" class=" control-label col-md-4 text-left"> CreatedDate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('CreatedDate', $row['CreatedDate'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ApplicantFaqQuestion" class=" control-label col-md-4 text-left"> ApplicantFaqQuestion <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='ApplicantFaqQuestion' rows='5' id='ApplicantFaqQuestion' class='form-control input-sm '  
				           >{{ $row['ApplicantFaqQuestion'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ApplicantFaqAnswer" class=" control-label col-md-4 text-left"> ApplicantFaqAnswer <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='ApplicantFaqAnswer' rows='5' id='ApplicantFaqAnswer' class='form-control input-sm '  
				           >{{ $row['ApplicantFaqAnswer'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Image" class=" control-label col-md-4 text-left"> Image <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='file' name='Image' id='Image' class='inputfile  @if($row['Image'] =='') class='required' @endif '  />

							<label for='Image'><i class='fa fa-upload'></i> Choose a file</label>
							<div class='Image_preview'></div>
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['Image'],'') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Category" class=" control-label col-md-4 text-left"> Category <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $Category = explode(',',$row['Category']);
					$Category_opt = array( 'Admin' => 'Admin' ,  'Applicant' => 'Applicant' , ); ?>
					<select name='Category' rows='5' required  class='select2 '  > 
						<?php 
						foreach($Category_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['Category'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
