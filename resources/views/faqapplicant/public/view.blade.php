<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['Id']['language'])? $fields['Id']['language'] : array())) }}</td>
						<td>{{ $row->Id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedDate', (isset($fields['CreatedDate']['language'])? $fields['CreatedDate']['language'] : array())) }}</td>
						<td>{{ $row->CreatedDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ApplicantFaqQuestion', (isset($fields['ApplicantFaqQuestion']['language'])? $fields['ApplicantFaqQuestion']['language'] : array())) }}</td>
						<td>{{ $row->ApplicantFaqQuestion}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ApplicantFaqAnswer', (isset($fields['ApplicantFaqAnswer']['language'])? $fields['ApplicantFaqAnswer']['language'] : array())) }}</td>
						<td>{{ $row->ApplicantFaqAnswer}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Image', (isset($fields['Image']['language'])? $fields['Image']['language'] : array())) }}</td>
						<td>{{ $row->Image}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['Category']['language'])? $fields['Category']['language'] : array())) }}</td>
						<td>{{ $row->Category}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	