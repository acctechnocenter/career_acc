<section id="blog">
    <div id="faq-slider">
       <div id="faqphoto" style="background: url('{!! asset('uploads/images/Website Banner-FAQ.jpg') !!}');  background-size: cover; background-size: 100% 100%; height:441px;">
         <div class="wrap">
         </div>
       </div>
     </div>
</section>
<section style="margin-top: 30px;margin-bottom: 30px;background-color: #f7f9fa;">
<body>

 <div class="acc-kontainer">
 @if (count($rowData) >= 1)
 <div class="accTabs">
 @foreach($rowData as $items)
      <a class="accLink" style="text-decoration:none">
        <div class="tab-title faqtitle cd-faq-trigger twocolorish panel-heading accordion-toggle" id="titleapplicant">{{ $items->ApplicantFaqQuestion }}</div>
        <div class="tab-slide Faqanswer " style="color: #ffffff; background-color: #0072bb; display:none;">{{ $items->ApplicantFaqAnswer }}</div>
      </a>
      @endforeach
 </div>
 &nbsp;
          @else
    		<p>No Data</p>
    	  @endif
 
 </div>
 
 <style>
 #main-content{
 background-color: #f7f9fa;
 }
 .panel-heading {
  padding: 0;
	border:0;
}
.panel-title>a, .panel-title>a:active{
	display:block;
	padding:15px;
  color:#555;
  font-size:16px;
  font-weight:bold;
	text-transform:uppercase;
	letter-spacing:1px;
  word-spacing:3px;
	text-decoration:none;
}
.panel-heading  a:before {
   font-family: 'Glyphicons Halflings';
   content: "\e114";
   float: right;
   transition: all 0.5s;
}
.panel-heading.active a:before {
	-webkit-transform: rotate(180deg);
	-moz-transform: rotate(180deg);
	transform: rotate(180deg);
} 
 div.twocolorish {
    background-color: #e2e3e7;
    border-left: 20px solid #fa9829;
}
 
 .acc-kontainer {
  width: 70%;
  margin: auto;
}

 .Faqanswer{
 height: auto;
  color: #fff;
  font-size: 16px;
  padding: 20px;
  transition: 0s;
  margin-right: 10px;
  margin-left: 10px;

 }
 
.faqtitle{
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #e2e3e7;
    color: black;
    font-size:20px!important;
    padding-left: 15px;
    padding-right: 15px;
    margin: 0.6em 0 0em!important;
    border-radius: 6px;
}

  .cd-faq-trigger::before, .cd-faq-trigger::after {
    /* arrow icon on the right */
    position: absolute;
    right: 24px;
    top: 50%;
    height: 2px;
    width: 13px;
    background: #cfdca0;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition-property: -webkit-transform;
    -moz-transition-property: -moz-transform;
    transition-property: transform;
    -webkit-transition-duration: 0.2s;
    -moz-transition-duration: 0.2s;
    transition-duration: 0.2s;
  }
  @media only screen and (max-width: 500px) {
  .cd-faq-trigger{
    position: relative;
    display: block;
    margin: 1.6em 0 0em!important;
    line-height: 1.2;
    }
  #faqphoto{
    height: 241px!important;
    width: 360px;  
  }
}

 </style>
 
<script>
/**
 * EFECTO PARA FLECHAS EN ACORDEON
 */
$("a.accLink").click(function () {
    var $that = $(this),
        $children = $that.children('div.tab-slide');

    // If expanded tab was clicked, collapse it.
    // >> FIXED LOGIC ERROR >> if ($children.hasClass("slided")) {
    if ($that.hasClass("slided")) {
        $children.slideUp("slow", function () {
            $that.removeClass("slided");
        });
    } else {
        // Collapse all expanded tabs
        // FIXED LOGIC ERROR >> $("div.slided").slideUp("slow", function() {
        $(".slided").find("div.tab-slide").slideUp("slow", function () {
            $(".slided").removeClass("slided");
        });
        // Expand clicked tab
        $children.slideDown("slow", function () {
            $that.addClass("slided");
        });
    }
});
</script>
</body>
</section>

<section class="section  bg-light filtercategory" style="padding-top:10px!important">

<!-- <div class="col-md-12">
      	<ul class="pagination pull-right">
      	  <li><a href="#">First</a></li>
		  <li><a href="#">1</a></li>
		  <li><a href="#">2</a></li>
		  <li><a href="#">3</a></li>
		  <li><a href="#">4</a></li>
		  <li><a href="#">5</a></li>
		  <li><a href="#">Next</a></li>
		</ul>
      </div> -->
</section>
