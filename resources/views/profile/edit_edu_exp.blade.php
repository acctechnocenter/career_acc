@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> Edit Educational Background </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'save-edit-edu-exp', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('user/profile/') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
<form name="myForm">
  <div style="background-color: white; width: 921px;" >
  	{!! Form::hidden('id_edit', $rowData->id_edu_back) !!}
    <div class="form-group  " >
    		<label for="lasteducation" class=" control-label col-md-4">Last Education</label>
    		<div class="col-md-8">
    			<?php $lasteducation = explode(',',$rowData->lasteducation);
    			$lasteducation_opt = array( 'SMU' => 'Senior High School (SMU)' ,  'D1' => 'Diploma 1 (D1)' , 'D2' => 'Diploma 2 (D2)' , 'D3' => 'Asociate Degree 3 (D3)' ,  'D4' => 'Diploma 4 (D4)' , 'S1' => 'Bachelor Degree (S1)' , 'S2' => 'Master Degree (S2)' , 'S3' => 'Doctor (S3)', ); ?>
    					<select name='lasteducation'  rows='5' required  class='select2 ' onchange="yesnoCheck(this);"  > 
    						<?php 
    						foreach($lasteducation_opt as $key=>$val)
    						{
    						    echo "<option  value ='$key' ".($rowData->lasteducation == $key ? " selected='selected' " : '' ).">$val</option>"; 						
    						}						
    			?></select> 
    		</div> 
    	</div> 
	<div class="form-group  " >
		<label for="universityorschool" class=" control-label col-md-4">University or School<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<select name='universityorschool' id='universityorschool' rows='5'  required  class='select2 ' onchange="yesnoCheck(this);" > 
    						<?php				
    						foreach($universityorschoolname as $item)
    						{
    						    echo "<option  value ='$item->id' ".($rowData->name == $item->name ? " selected='selected' " : '' ).">".$item->name."</option>"; 						
    						}						
    			?></select>
<div id="ifYes" style="display: none;" class="form-group  "> 
    			    <br> 
                    <div class="col-md-8" style="width:610px!important;">
                                        <label for="otherunivorschool">University or School anda tidak ada dalam daftar? silahkan daftarkan namanya dikolom di bawah </label>
        			<input  type='text' name='otherunivorschool' id='otherunivorschool'    value="{{$rowData->otherunivorschool}}"
        										  class='form-control input-sm ' /> 
        			</div> 
                </div>										  
			</div> 
	  </div>
    			
	  <div class="form-group  " >
		<label for="faculty" class=" control-label col-md-4">Faculty</label>
			<div class="col-md-8">
			<input  type='text' name='faculty' id='faculty' value="{{ $rowData->faculty }}" required=""  data-parsley-faculty     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="major" class=" control-label col-md-4">Major</label>
			<div class="col-md-8">
			<input  type='text' name='major' id='major' value="{{ $rowData->major }}" required=""  data-parsley-major     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="gpa" class=" control-label col-md-3" style="width: 320px;">Gpa</label>
			<div class="col-md-3">
			<input  type='number' name='gpa' id='gpa' step='0.01' min="0" max="4" value="{{ $rowData->gpa }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="startdate" class=" control-label col-md-3" style="width: 321px;">Start date</label>
			<div class="col-md-3">			
			<input  type='text' name='startdate' id='startdate' value="{{ $rowData->startdate }}" required=""  data-parsley-startdate     
										  class='form-control input-sm ' />
			</div>
			<label for="endate" class=" control-label col-md-3" style="width:91px">End Date</label>
			<div class="col-md-3">
			<input  type='text' name='endate' id='endate' value="{{ $rowData->endate }}" required=""  data-parsley-endate     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>	 
	 </div>
	 </form>
	 </div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">  
		 
   $(document).ready(function() {
 if($('#universityorschool').val() == 300000019559645){
		   $('#ifYes').show();
	   }
	     
	   $(function() {
		   $('#universityorschool').change(function(){
		     $('#ifYes').hide();

		   $('#otherunivorschool').attr('value', ''); //ga muncul di input cuma masih ada datanya di datbase
		     
		   });	   
		   
		 });

	   $("form[name=myForm]").parsley();
	   
	 window.Parsley.addValidator('faculty', {
	   validateString: function(value) {
	     var patt = new RegExp("^[A-Za-z -]+$");
	     return patt.test(value);
	   },
	   messages: {
	     en: 'Faculty Must Contain only letters.'
	   }
	 });

	 window.Parsley.addValidator('major', {
		   validateString: function(value) {
		     var patt = new RegExp("^[A-Za-z -]+$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Major Must Contain only letters.'
		   }
		 });

	 window.Parsley.addValidator('startdate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Start Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });

	 window.Parsley.addValidator('endate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'End Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });
	 
	 });
   
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
function yesnoCheck(that) {
        if (that.value == "300000019559645") {
            document.getElementById("ifYes").style.display = "block";
        } else {
            document.getElementById("ifYes").style.display = "none";
        }
    }
	</script>		 
@stop