@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> Add Educational Experience </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'save-add-edu-exp', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('user/profile/') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <form name="myForm">
  <div style="background-color: white; width: 921px;" >
<?php /*  {!! Form::hidden('$id_add_edu', $rowData->id_edu_back) !!} */?>
  {!! Form::hidden('id_user', \Session::get('uid')) !!}

    <div class="form-group  " >
    		<label for="lasteducation" class=" control-label col-md-4">{{ Lang::get('core.lasteducation') }}</label>
    		<div class="col-md-8">
    			<?php
    			$lasteducation_opt = array( 'SMU' => 'Senior High School (SMU)' ,  'D1' => 'Diploma 1 (D1)' , 'D2' => 'Diploma 2 (D2)' , 'D3' => 'Asociate Degree 3 (D3)' ,  'D4' => 'Diploma 4 (D4)' , 'S1' => 'Bachelor Degree (S1)' , 'S2' => 'Master Degree (S2)' , 'S3' => 'Doctor (S3)', ); ?>
    					<select name='lasteducation' rows='5' required  class='select2 '  > 
    						<?php 
    						foreach($lasteducation_opt as $key=>$val)
    						{
    						    echo "<option  value =".$key.">".$val."</option>"; 	
    						}						
    			?></select> 
    		</div> 
    	</div> 
 	<div class="form-group  " >
		<label for="universityorschool" class=" control-label col-md-4">{{ Lang::get('core.universityorschool') }}</label>
			<div class="col-md-8">
			<select name='universityorschool' rows='5' required  class='select2 ' onchange="yesnoCheck(this);" > 
    						<?php     				
    						
    						foreach($universityorschool as $item)
    						{
    						    echo "<option  value =".$item->id.">".$item->name."</option>"; 						
    						}						
    			?></select> 
<div id="ifYes" style="display: none;" class="form-group  "> 
    			    <br> 
                    <div class="col-md-8" style="width:610px!important;">
                                        <label for="otherunivorschool">University or School anda tidak ada dalam daftar? silahkan daftarkan namanya dikolom di bawah </label>
        			<input  type='text' name='otherunivorschool' id='otherunivorschool'    
        										  class='form-control input-sm ' /> 
        			</div> 
                </div>
										  
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="faculty" class=" control-label col-md-4">{{ Lang::get('core.faculty') }}</label>
			<div class="col-md-8">
			<input  type='text' name='faculty' id='faculty' required=""  data-parsley-faculty value=""    
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="major" class=" control-label col-md-4">{{ Lang::get('core.major') }}</label>
			<div class="col-md-8">
			<input  type='text' name='major' id='major' required=""  data-parsley-major value=""     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="gpa" class=" control-label col-md-3" style="width: 320px;">{{ Lang::get('core.gpa') }}</label>
			<div class="col-md-3">
			<input  type='number' name='gpa' id='gpa' step='0.01' min="0" max="4" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="startdate" class=" control-label col-md-3" style="width: 321px;">{{ Lang::get('core.startdate') }}</label>
			<div class="col-md-3">			
			<input type="number" name='startdate' id='startdate' value="" required=""  data-parsley-startdate    
										  class='form-control input-sm ' placeholder="Year"/>
			</div>
			<label for="endate" class=" control-label col-md-3" style="width:91px">{{ Lang::get('core.endate') }}</label>
			<div class="col-md-3">
			<input type="number" name='endate' id='endate' value=""  required=""  data-parsley-endate
										  class='form-control input-sm ' placeholder="Year"  /> 
			</div> 
	  </div>	   
	 </div>
	 </form>
	 </div>
		
		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		

	
		 
   <script type="text/javascript">   
   $(document).ready(function() {
	   $("form[name=myForm]").parsley();
	   
	 window.Parsley.addValidator('faculty', {
	   validateString: function(value) {
	     var patt = new RegExp("^[A-Za-z -]+$");
	     return patt.test(value);
	   },
	   messages: {
	     en: 'Faculty Must Contain only letters.'
	   }
	 });

	 window.Parsley.addValidator('major', {
		   validateString: function(value) {
		     var patt = new RegExp("^[A-Za-z -]+$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Major Must Contain only letters.'
		   }
		 });


	 window.Parsley.addValidator('startdate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Start Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });

	 window.Parsley.addValidator('endate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'End Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });
	 
	 });
	 
      
   
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});	
		
	});
	function yesnoCheck(that) {
        if (that.value == "300000019559645") {
            document.getElementById("ifYes").style.display = "block";
        } else {
            document.getElementById("ifYes").style.display = "none";
        }
    }
	</script>		 
@stop