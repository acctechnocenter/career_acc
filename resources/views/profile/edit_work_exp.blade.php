@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> Edit Working Experience </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'save-edit-work-exp', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('user/profile/') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
<!-- 				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button> -->
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
  	{!! Form::hidden('id_edit', $rowData->id_wor_exp ) !!}
	  <div class="form-group  " >
		<label for="company" class=" control-label col-md-4">{{ Lang::get('core.company') }}</label>
			<div class="col-md-8">
			<input  type='text' name='company' id='company' value="{{ $rowData->company }}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="workingexperienceperiodstartdate" class=" control-label col-md-3" style="width: 321px;">{{ Lang::get('core.workingexperienceperiodstartdate') }}</label>
			<div class="col-md-3">			
			<input  type='date' name='workingexperienceperiodstartdate' id='workingexperienceperiodstartdate' value="{{ $rowData->workingexperienceperiodstartdate }}"      
										  class='form-control input-sm ' /> 
			</div>
			<label for="workingexperienceperiodenddate" class=" control-label col-md-3" style="width:98px">{{ Lang::get('core.workingexperienceperiodenddate') }}</label>
			<div class="col-md-3">
			<input  type='date' name='workingexperienceperiodenddate' id='workingexperienceperiodenddate' value="{{ $rowData->workingexperienceperiodenddate }}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  
	  <div class="form-group  " >
		<label for="position" class=" control-label col-md-4">{{ Lang::get('core.position') }}</label>
			<div class="col-md-8">
			<input  type='text' name='position' id='position' value="{{ $rowData->position }}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="category" class=" control-label col-md-4">{{ Lang::get('core.category') }}</label>
			<div class="col-md-8">
				<?php $category = explode(',',$rowData->category);
				$category_opt = array( 'Marketing/Sales' => 'Marketing/Sales' ,  'Operation' => 'Operation' ,  'Human Resource' => 'Human Resource' ,  'Procurement/Purchasing/GA ' => 'Procurement/Purchasing/GA' ,  'Information Technology' => 'Information Technology' ,  'Legal/Litigation' => 'Legal/Litigation' , ); ?>
					<select name='category' rows='5'   class='select2 '  > 
						<?php 
						foreach($category_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($rowData->category == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>	
			<div class="form-group  " >
		<label for="status_working_experience" class=" control-label col-md-4">{{ Lang::get('core.status_working_experience') }}</label>
			<div class="col-md-8">
				<?php $status_working_experience = explode(',',$rowData->status_working_experience);
				$status_working_experience_opt = array( 'Fulltime' => 'Fulltime' ,  'Freelance' => 'Freelance' ,  'Internship' => 'Internship' , ); ?>
					<select name='status_working_experience' rows='5'  class='select2 '  > 
						<?php 
						foreach($status_working_experience_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($rowData->status_working_experience == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>	
		<div class="form-group  " >
		<label for="jobdescription" class=" control-label col-md-4">{{ Lang::get('core.jobdescription') }}</label>
			<div class="col-md-8">
			<textarea class="field" name="jobdescription" cols="111" rows="5" style="width:605px" >{{ $rowData->jobdescription }}</textarea> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="salary" class=" control-label col-md-4">{{ Lang::get('core.salary') }}</label>
			<div class="col-md-8">
			<input  type='number' name='salary' id='salary' value="{{ $rowData->salary }}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="Reasonofleaving" class=" control-label col-md-4">{{ Lang::get('core.Reasonofleaving') }}</label>
			<div class="col-md-8">
			<textarea class="field" name="Reasonofleaving" cols="111" rows="5" style="width:605px">{{ $rowData->Reasonofleaving }}</textarea> 
			</div> 
	  </div>
	 </div>
	 </div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop