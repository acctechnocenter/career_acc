@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> Add Organizational Experience </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'save-add-organization-exp', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('user/profile/') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
  {!! Form::hidden('id_user', \Session::get('uid')) !!}
	  <div class="form-group  " >
		<label for="organizationname" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;">{{ Lang::get('core.organizationname') }}</label>
			<div class="col-md-8">
			<input  type='text' name='organizationname' id='organizationname' value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="organizationscope" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;"> {{Lang::get('core.organizationscope') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">										  
					<?php 
					$organizationscope_opt = array( 'International' => 'International' ,  'National' => 'National' ,'Region' => 'Region' ,  'University' => 'University' ,'Faculty/Major' => 'Faculty/Major' ,  'Others' => 'Others' ,); ?>
					<select name='organizationscope' rows='5'  class='select2 '  > 
						<?php 
						foreach($organizationscope_opt as $key=>$val)
						{
						    echo "<option  value =".$key.">".$val."</option>"; 	
						}						
						?></select> 
			</div> 
		</div> 
		<div class="form-group  " >
    		<label for="organization_experience_startdate" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;">{{ Lang::get('core.organization_experience_startdate') }}</label>
    			<div class="col-md-8">			
    			<input  type='text' placeholder="Year" name='organization_experience_startdate' id='organization_experience_startdate' value=""   required=""  data-parsley-startdate   
    										  class='form-control input-sm ' /> 
    			</div>
		</div>
		<div class="form-group  " >
			<label for="organization_experience_endate" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;">{{ Lang::get('core.organization_experience_endate') }}</label>
			<div class="col-md-8">
			<input  type='text' placeholder="Year" name='organization_experience_endate' id='organization_experience_endate' value=""  required=""  data-parsley-endate    
										  class='form-control input-sm ' /> 
			</div> 
		</div>
	  <div class="form-group  " >
		<label for="rolefunction" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;">{{ Lang::get('core.rolefunction') }}</label>
			<div class="col-md-8">
			<input  type='text' name='rolefunction' id='rolefunction' value=""     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="roleorposition" class=" control-label col-md-4" style="text-align: left; margin-top:5px; margin-bottom:5px;">{{ Lang::get('core.roleorposition') }}</label>
			<div class="col-md-8">
			<input  type='text' name='roleorposition' id='roleorposition' value=""      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>	 
	 </div>
	</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
   $(document).ready(function() {
	   $("form[name=myForm]").parsley();
	   
	 window.Parsley.addValidator('startdate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Start Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });

	 window.Parsley.addValidator('endate', {
		   validateString: function(value) {
		     var patt = new RegExp("^([0-9]{4})?$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'End Date Must Contain only Number with limit 4 character, ex: 1995,1989,1945.'
		   }
		 });
	 
	 });
   
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop