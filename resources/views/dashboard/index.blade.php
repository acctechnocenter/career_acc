@extends('layouts.app')


@section('content')
<div class="page-content row"> 
<div class="page-content-wrapper m-t">
<?php $name= \DB::table('tb_users')->where('id', Auth::user()->id)->value('first_name'); ?>
<h2>Welcome, {{ $name }}<h2>

 <div class="row">
	<div class="col-md-6" style="background-color:#fff;">
		<h4>5 most pending comments in news  </h4>
			<div class="table-responsive" style=" padding-right: 0; padding-left: 0;">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>News</th>
							<th>Pending</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php $i = 1;?>
					@if(count($rowData) > 0)
						@foreach ($rowData as $row)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $row->title }}</td>
								<td>{{ $row->pending }}</td>
								<td><a href="{{ url('view-comments/'.$row->pageID) }}"><i class="fa fa-arrow-right"></i></a></td>
							</tr>
						@endforeach
					@else	
						<tr>
							<td colspan="4">No data</td>
						</tr>
					@endif
					</tbody>
				</table>
			</div>
	<p style="font-size:14px; padding-bottom: 5px;">You have {{ $countPending }} pending comments in all news. <a href="{{url('core/posts') }}" style="text-decoration:underline;">See all</a></p>
	</div>
 </div>


</div>
</div>  
                     
@stop