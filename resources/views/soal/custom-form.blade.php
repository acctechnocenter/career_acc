@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }}</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'prescreening-management/question/save', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('prescreening-management/question/'.$id_kategori_soal.'/view') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.sb_apply') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.sb_save') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Soal</legend>
				{!! Form::hidden('id', $row['id']) !!}
				{!! Form::hidden('id_kategori_soal',$id_kategori_soal) !!}
									  <div class="form-group  " >
										<label for="Soal" class=" control-label col-md-4 text-left"> Soal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='soal' rows='5' id='soal' class='form-control input-sm '  required
				           >{{ $row['soal'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 										
									  <div class="form-group  " >
										<label for="Type" class=" control-label col-md-4 text-left"> Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $type = explode(',',$row['type']);
					$type_opt = array( '2' => 'Multiple Choice' , '1' => 'Checkbox' , '3' =>'Essay',  ); ?>
					<select name='type' rows='5'   class='select2 '  > 
						<?php 
						foreach($type_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['type'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									    <div class="form-group  " >
										<label for="order_number" class=" control-label col-md-4 text-left"> Order</label>
										<div class="col-md-6">
										<input  type='text' name='order_number' id='order_number' pattern="[0-9]*"  value="{{ $row['order_number'] }}" 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
		 
@stop