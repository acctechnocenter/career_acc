@extends('layouts.app')

@section('content')
<div class="page-content row">
	<div class="page-content-wrapper m-t">
		<div class="sbox">
			<div class="sbox-title">
				<h1> Applicant Menu<small> </small></h1>
				<div class="sbox-tools">
						<a href="{{ url('applicant-menu') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
				</div>				
			</div>	
			<div class="sbox-content">
						<div class="row">
				<div class="col-md-4"> 	
					<h5> All Records </h5>
				</div>  
			<div class="col-md-1 pull-right">
					<div class="input-group">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default btn-sm " 
					        onclick="SximoModalTest('Advance Search'); " ><i class="fa fa-filter"></i> Filter </button>
					      </div>
					    </div>
			</div> 
			</div>
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover ">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center">First Name</th>
						<th align="center">Last Name</th>
						<th align="center">Email</th>
						<th align="center">Date Of Birth</th>
						<th align="center">City Or Regency</th>
						<th align="center">Active</th>
						<th align="center">Action</th>					
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->first_name }}</td>
							<td>{{ $row->last_name }}</td>
							<td>{{ $row->email }}</td>
							<td>{{ $row->dateofbirth }}</td>
							<td>{{ $row->cityorregency }}</td>
							<td>
								@if($row->active == 1)
								<span class="label label-primary"> Active</span>
								@elseif($row->active == 0)
								<span class="label label-danger"> Not Active</span>
								@elseif($row->active == 2)
								<span class="label label-warning"> Banned</span>
								@endif
							</td>
							<td>
								<a href="{{url('applicant-menu/ApplicantDetail/'.$row->idApplicant)}}"><button class="btn btn-primary btn-xs" type="button" title="View Applicant"><i class="fa fa-folder-open-o" style="color: white"></i></button></a>  
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->appends(request()->input())->links() !!}
			</div>
			<!-- End Table Grid -->

			</div>
		</div>
		
	</div>
</div>

   <script type="text/javascript">
	
		function SximoModalTest(title)
		{
			$('#sximo-modal-content').html(' ....Loading content , please wait ...');
			$('.modal-title').html(title);
			$('#sximo-modal-content').load('search-applicant',function(){
			});
			$('#sximo-modal').modal('show');	
		}		
		
	</script>	

@stop