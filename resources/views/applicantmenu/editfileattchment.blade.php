@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('applicant-menu/ApplicantDetail/'.$test_id) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<h5>Update File Attachment Applicant</h5>
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
            	<!-- applicant document -->
  <div class="tab-pane  m-t">
  	{!! Form::open(array('route' => 'postupdatefileattachmentapplicant','files'=>true)) !!} 
<!-- 	{!! Form::open(array('url'=>'applicant-menu/editfileattchment/updatefileattachmentapplicant/', 'class'=>'form-horizontal ')) !!}  -->
	{!! Form::hidden('id_user', $applicantData->id_user) !!} 
	{!! Form::hidden('test_id', $applicantData->id_user) !!}	   
	@if (count($applicantData) > 0)
	 {!! Form::hidden('id', $applicantData->id) !!} 
	@endif
	{!! Form::hidden('id_user', \Session::get('uid')) !!}
<?php /* ?>	{!! Form::hidden('id_inaddinfo', $interestaddinfo->id_interest_addinfo) !!} <?php */ ?>
	<!-- start Personal Details -->
   @if (count($applicantData) > 0)	
	<!-- End Personal Details -->  
    <div class="col-md-12 form-group">
	<h6 style="color: green">Please upload a valid image or file. Size of image or file should not be more than 2MB.</h6>
			<br> 
		<?php $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];?>
		<div class="table-responsive">
		<table class="table table-striped">
			<tbody>
		@foreach($jenisFile as $jns)
		<?php $label = strtoupper(str_replace("_"," ",$jns)); ?>
			<tr>
				<td width='29%' class='label-view'>
					@if(count($fileattachData) > 0)
								@foreach($fileattachData as $file)
									@if($file->type_file == $jns)
									<i></i>
									@endif								
								@endforeach
				@endif
				{{ $label }}

				@if($label == "IJAZAH")
				<span class="asterix"> * </span>
				@endif
				@if($label == "SKCK")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KTP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KARTU KELUARGA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NPWP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NOMOR REKENING PERMATA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "FOTO LATAR BELAKANG ORANGE")
				<span class="asterix"> * </span>
				@endif
				@if($label == "AKTE KELAHIRAN DIRI SENDIRI")
				<span class="asterix"> * </span>
				@endif
			</td>
                <td>  
							@if(count($fileattachData) > 0)
								@foreach($fileattachData as $file)
									@if($file->type_file == $jns)
									&nbsp;
									&nbsp;
									&nbsp;
									<a type="button" onclick="SximoConfirmDelete('{{ url('dashboard/deletefileattchmentappmenu/'.$file->id_file) }}')">										
									<i></i>
									</a>
									@endif
								@endforeach
							@endif		
                </td>   
				<td>
                  	 <input type="file" name={{ $jns }} id={{ $jns }} class="inputfile"/>
                </td>   
				</tr>
		@endforeach
			</tbody>
		</table>
        </div>
       <br>
       <div class="col-md-12 imgdiv" style="text-align: center">
            <button type="submit" class="btn btn-primary">Submit All</button>
       </div>
       
  </div>
	@else
	
	<div class="col-md-12 form-group">
	<h6 style="color: green">Please upload a valid image or file. Size of image or file should not be more than 2MB.</h6>
			<br> 
		<?php $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];?>
		<div class="table-responsive">
		<table class="table table-striped">
			<tbody>
		@foreach($jenisFile as $jns)
		<?php $label = strtoupper(str_replace("_"," ",$jns)); ?>
			<tr>
				<td width='20%' class='label-view'>{{ $label }} 
				@if($label == "IJAZAH")
				<span class="asterix"> * </span>
				@endif
				@if($label == "SKCK")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KTP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KARTU KELUARGA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NPWP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NOMOR REKENING PERMATA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "FOTO LATAR BELAKANG ORANGE")
				<span class="asterix"> * </span>
				@endif
				@if($label == "AKTE KELAHIRAN DIRI SENDIRI")
				<span class="asterix"> * </span>
				@endif
				</td>				
                <td>  
							@if(count($fileattachData) > 0)
								@foreach($fileattachData as $file)
									@if($file->type_file == $jns)
									<a href={{ $file->link_file }} title="Download file" ><i class="fa fa-download"></i></a>
									<i class="fa fa-check-square-o" style="color: green;"></i>
									@endif									
								@endforeach
							@endif		
                </td>   
				<td>
                  	 <input type="file" name={{ $jns }} id={{ $jns }} class="inputfile"/>
                </td>   
				</tr>
		@endforeach
			</tbody>
		</table>
        </div>
       <br>
       <div class="col-md-12 imgdiv" style="text-align: center">
            <button type="submit" class="btn btn-primary">Submit All</button>
       </div>
       
  </div>
	
	@endif 
              
                
		<div class="form-group  " >
		<div class="col-md-12" style="margin-left: 77px; margin-right: 26px;">
        <!-- <input type="checkbox" name="terms" value="agreed" id="terms" onclick="if(!this.form.terms.checked){alert('You must agree to the terms first.');return false}" required /> Here by I delare that all the information above are true and correct. I am responsible for any untrue statement.
        -->
        </div>
        </div>

	{!! Form::close() !!}	
  </div>
	 </div>
		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});

	//tambahan untuk salaryupto yang gaada datanya

// 	document.getElementById("expectedsalaryfrom_view").disabled = true;
// 	document.getElementById("expectedsalaryupto_view").disabled = true;
// 	document.getElementById("sendsalary").style.display = "none";

	$("#clicker").click(function(){	
		document.getElementById("expectedsalaryfrom_view").disabled = false;
		document.getElementById("expectedsalaryupto_view").disabled = false;
		document.getElementById("sendsalary").style.display = "block";
		document.getElementById("showsalary").style.display = "none";
		document.getElementById("save").style.display = "none"; // savenya di ilangin	
		
	});

	$("#save").click(function(){	
		document.getElementById("expectedsalaryfrom_view").disabled = true;
		document.getElementById("expectedsalaryupto_view").disabled = true;
		document.getElementById("showsalary").style.display = "block";
		document.getElementById("sendsalary").style.display = "none";

	});
	//tambahan untuk salaryupto yang ada datanya
	
	</script>		 
@stop