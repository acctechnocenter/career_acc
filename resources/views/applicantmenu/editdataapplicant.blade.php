@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('applicant-menu/ApplicantDetail/'.$id) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<h5>Update Applicant Detail</h5>
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
            	<!-- applicant document -->
  <div class="tab-pane  m-t">
	{!! Form::open(array('url'=>'applicant-menu/editdataapplicant/updateapplicantdata/', 'class'=>'form-horizontal ')) !!}    
	@if (count($applicantData) > 0)
	 {!! Form::hidden('id', $applicantData->id) !!} 
	@endif
	{!! Form::hidden('id_user', \Session::get('uid')) !!}
<?php /* ?>	{!! Form::hidden('id_inaddinfo', $interestaddinfo->id_interest_addinfo) !!} <?php */ ?>
	
<h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;" >Personal Details</h5>

    <!-- start Personal Details -->
   @if (count($applicantData) > 0)
	<div class="form-group  " >
		<label for="title_profile" class=" control-label col-md-4"> {{ Lang::get('core.title_profile')}} <span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php $title_profile = explode(',',$applicantData->title_profile);
				$title_opt = array( 'Mr.' => 'Mr.' ,  'Mrs.' => 'Mrs.' ,  'Ms.' => 'Ms.' ,); ?>
 					<select name='title_profile' rows='5' required  class='select2 ' id="title_profile"  > 
						<?php 
						foreach($title_opt as $key=>$val)
					{
					    echo "<option  value ='$key' ".($applicantData->title_profile == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?>
					</select>  					
				</div> 
	</div>
	<div class="form-group  " >
		<label for="firstname" class=" control-label col-md-4">{{ Lang::get('core.firstname') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='firstname' id='firstname' value="{{ $applicantData->first_name }}" pattern="^[A-Za-z -]+$" title="First Name harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="lastname" class=" control-label col-md-4">{{ Lang::get('core.lastname') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='lastname' id='lastname' value="{{ $applicantData->last_name }}" pattern="^[A-Za-z -]+$" title="Last Name harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	<div class="form-group">
		<label for="gender" class=" control-label col-md-4">{{ Lang::get('core.gender') }} <span class="asterix"> * </span></label>
		<div class="col-md-8">
		<input type="radio" name="gender" value="male" required <?php if(in_array('male', $explode_gender)){
            echo "checked"; }?>> Male<br>
  		<input type="radio" name="gender" value="female" required <?php if(in_array('female', $explode_gender)){
            echo "checked"; }?>> Female<br>
		<!--<input name="gender" type= id="gender" class="form-control input-sm" required value="{{ $applicantData->gender }}" />-->		 
		 </div> 
	  </div>
	  <div class="form-group  " >
		<label for="dateofbirth" class=" control-label col-md-4">{{ Lang::get('core.dateofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='date' name='dateofbirth' id='dateofbirth' value="{{ $applicantData->dateofbirth }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="provinceofbirth" class=" control-label col-md-4">{{ Lang::get('core.provinceofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='provinceofbirth' id='provinceofbirth' value="{{ $applicantData->provinceofbirth }}" pattern="^[A-Za-z -]+$" title="Province of Birth harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityofbirth" class=" control-label col-md-4">{{ Lang::get('core.cityofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityofbirth' id='cityofbirth' pattern="^[A-Za-z -]+$" title="City Of Birth harus Huruf semua" value="{{ $applicantData->cityofbirth }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>  
	  <div class="form-group  " >
		<label for="identitycardnumber" class=" control-label col-md-4">{{ Lang::get('core.identitycardnumber') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='identitycardnumber' id='identitycardnumber' value="{{ $applicantData->identitycardnumber }}" pattern="[0-9]{16}" title="Identity Card Number harus Huruf Angka dengan panjang 16 karakter" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="homephone" class=" control-label col-md-4">{{ Lang::get('core.homephone') }} (optional)</label>
			<div class="col-md-8">
			<input  type='text' name='homephone' id='homephone' pattern="[0-9]*" title="Homephone harus Angka" value="{{ $applicantData->homephone }}"     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="mobilephone" class=" control-label col-md-4">{{ Lang::get('core.mobilephone') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='mobilephone' id='mobilephone' pattern="[0-9]*" title="Mobile Phone harus Angka" value="{{ $applicantData->mobilephone }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="npwp" class=" control-label col-md-4">{{ Lang::get('core.npwp') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='npwp' id='npwp' pattern="[0-9]{15}" title="NPWP harus Angka dengan panjang 15 karakter" value="{{ $applicantData->npwp}}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	 <div class="form-group  " >
		<label for="maritalstatus" class=" control-label col-md-4">{{ Lang::get('core.maritalstatus') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php $maritalstatus = explode(',',$applicantData->maritalstatus);
			     $maritalstatus_opt = array( 'Single' => 'Single' ,  'Married' => 'Married' ,  'Divorced' => 'Divorced' , 'Widowed' => 'Widowed' , ); ?>
					<select name='maritalstatus' id="maritalstatus" rows='5' required  class='select2 ' onchange="yesnoCheck(this);" > 
						<?php 	
						foreach($maritalstatus_opt as $key=>$val)
						{
						            echo "<option  value ='$key' ".($applicantData->maritalstatus == $key ? " selected='selected' " : '' ).">$val</option>"; 
						}						
			?></select> 		
		</div> 
	</div>
	<!-- tambahin married date -->
	    	<div id="ifYes" style="display: none;" class="form-group  "> 
	    	<label for="marrieddate" class=" control-label col-md-4">{{ Lang::get('core.marrieddate') }} <span class="asterix"> * </span></label>
              <div class="col-md-8">                 
        			<input  type='date' name='marrieddate' id='marrieddate' value="{{$applicantData->marrieddate}}" 
        			        			    class='form-control input-sm ' />
              </div> 
                </div>	
	
	
	<div class="form-group  " >
		<label for="religion" class=" control-label col-md-4">{{ Lang::get('core.religion') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php $religion = explode(',',$applicantData->religion);
			$religion_opt = array( 'Moslem' => 'Moslem' ,  'Buddha' => 'Buddha' ,  'Catholic' => 'Catholic' , 'Christian' => 'Christian' , 'Konghucu' => 'Konghucu' ,  'Hindu' => 'Hindu' , ); ?>
					<select name='religion' rows='5' required  class='select2 '  > 
						<?php 
						foreach($religion_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($applicantData->religion == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	
	<!-- End Personal Details -->  
 
	
	@else
<div class="form-group  " >
		<label for="title_profile" class=" control-label col-md-4"> {{ Lang::get('core.title_profile')}} <span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php 
				$title_opt = array( 'Mr.' => 'Mr.' ,  'Mrs.' => 'Mrs.' ,  'Ms.' => 'Ms.' ,); ?>
 					<select name='title_profile' rows='5' required  class='select2 ' id="title_profile" > 
						<?php 
						foreach($title_opt as $key=>$val)
					   {
					    echo "<option  value ='$key' 
                                ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?>
					</select>  					
				</div> 
	</div>
	
	<div class="form-group  " >
		<label for="firstname" class=" control-label col-md-4">{{ Lang::get('core.firstname') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='firstname' id='firstname' value="" pattern="^[A-Za-z -]+$" title="First Name harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="lastname" class=" control-label col-md-4">{{ Lang::get('core.lastname') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='lastname' id='lastname' value="" pattern="^[A-Za-z -]+$" title="last Name harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	
	<div class="form-group">
		<label for="gender" class=" control-label col-md-4">{{ Lang::get('core.gender') }} <span class="asterix"> * </span></label>
		<div class="col-md-8">
		<input type="radio" name="gender" value="male"> Male<br>
  		<input type="radio" name="gender" value="female"> Female<br>		 
		 </div> 
	  </div>
	  	  
	  
	  <div class="form-group  " >
		<label for="dateofbirth" class=" control-label col-md-4">{{ Lang::get('core.dateofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='date' name='dateofbirth' id='dateofbirth' value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="provinceofbirth" class=" control-label col-md-4">{{ Lang::get('core.provinceofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='provinceofbirth' id='provinceofbirth' pattern="^[A-Za-z -]+$" title="Add Province Of Birth Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityofbirth" class=" control-label col-md-4">{{ Lang::get('core.cityofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityofbirth' id='cityofbirth' pattern="^[A-Za-z -]+$" title="Add City Of Birth Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="identitycardnumber" class=" control-label col-md-4">{{ Lang::get('core.identitycardnumber') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='identitycardnumber' id='identitycardnumber' pattern="[0-9]{16}" title="Identity Card Number harus Huruf Angka dengan panjang 16 karakter" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="homephone" class=" control-label col-md-4">{{ Lang::get('core.homephone') }} (optional)</label>
			<div class="col-md-8">
			<input  type='text' name='homephone' id='homephone' pattern="[0-9]*" title="Home Phone harus Angka" value=""     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="mobilephone" class=" control-label col-md-4">{{ Lang::get('core.mobilephone') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='mobilephone' id='mobilephone' pattern="[0-9]*" title="Mobile Phone harus Angka" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	   <div class="form-group  " >
		<label for="npwp" class=" control-label col-md-4">{{ Lang::get('core.npwp') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='npwp' id='npwp' pattern="[0-9]{15}" title="NPWP harus Huruf Angka dengan panjang 15 karakter" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	 <div class="form-group  " >
		<label for="maritalstatus" class=" control-label col-md-4">{{ Lang::get('core.maritalstatus') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php
			     $maritalstatus_opt = array( 'Single' => 'Single' ,  'Married' => 'Married' ,  'Divorced' => 'Divorced' , 'Widowed' => 'Widowed' , ); ?>
					<select name='maritalstatus' id="maritalstatus" rows='5' required  class='select2 ' onchange="yesnoCheck(this);" > 
						<?php 
						foreach($maritalstatus_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	
	<!-- tambahan untuk add -->
			    <div class="form-group  " >
    			  <label for="marrieddate" class=" control-label col-md-4">{{ Lang::get('core.marrieddate') }} <span class="asterix"> * </span></label>
    			    <div class="col-md-8">
        			    <input  type='date' name='marrieddate' id='marrieddate' value=""
        			    class='form-control input-sm ' />
			   		 </div>
			    </div>
	<!-- tambahan untuk add -->
	
	 <div class="form-group  " >
		<label for="religion" class=" control-label col-md-4">{{ Lang::get('core.religion') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php
			$religion_opt = array( 'Moslem' => 'Moslem' ,  'Buddha' => 'Buddha' ,  'Catholic' => 'Catholic' , 'Christian' => 'Christian' , 'Konghucu' => 'Konghucu' ,  'Hindu' => 'Hindu' , ); ?>
					<select name='religion' rows='5' required  class='select2 '  > 
						<?php 
						foreach($religion_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	
	@endif
	

	
	
	
	
	
	
	
	
	
	    <!-- start home address -->
	  <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Home Address</h5>
	  	  @if (count($applicantData) > 0)
	  	  <div class="form-group  " >
		<label for="country" class=" control-label col-md-4">{{ Lang::get('core.country') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='country' id='country' value="Indonesia" required     
										  class='form-control input-sm ' disabled /> 
			</div> 
	  </div>
	  <!--value country indonesia, disabled-->
	  <div class="form-group  " >
		<label for="address" class=" control-label col-md-4" >{{ Lang::get('core.address') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='address' id='address' value="{{ $applicantData->address }}" required     
										  class='form-control input-sm ' /> 
			<!-- <textarea class="field" name="notes" cols="111" rows="5" style="width:605px" value="{{ $applicantData->address }}"></textarea> -->
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="subdisctrict" class=" control-label col-md-4">{{ Lang::get('core.subdisctrict') }} (Kecamatan)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='subdisctrict' id='subdisctrict' pattern="^[A-Za-z -]+$" title="Sub Disctrict Must Contain only letters." value="{{ $applicantData->subdisctrict }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityorregency" class=" control-label col-md-4">{{ Lang::get('core.cityorregency') }} (Kota / Kabupaten)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityorregency' id='cityorregency' pattern="^[A-Za-z -]+$" title="City or Regency Must Contain only letters." value="{{ $applicantData->cityorregency }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="province" class=" control-label col-md-4">{{ Lang::get('core.province') }} (Provinsi)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='province' id='province' pattern="^[A-Za-z -]+$" title="Province Must Contain only letters." value="{{ $applicantData->province }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="postalcode" class=" control-label col-md-4">{{ Lang::get('core.postalcode') }} (Kode POS) <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='postalcode' id='postalcode' pattern="[0-9]{5}" title="Postal Code Harus angka dengan 5 karakter" value="{{ $applicantData->postalcode }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  	  @else
	  	  
	  	  <div class="form-group  " >
		<label for="country" class=" control-label col-md-4">{{ Lang::get('core.country') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='country' id='country' value="Indonesia" required     
										  class='form-control input-sm ' disabled /> 
			</div> 
	  </div>
	  <!--value country indonesia, disabled-->
	  <div class="form-group  " >
		<label for="address" class=" control-label col-md-4" >{{ Lang::get('core.address') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='address' id='address' value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="subdisctrict" class=" control-label col-md-4">{{ Lang::get('core.subdisctrict') }} (Kecamatan)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='subdisctrict' id='subdisctrict' pattern="^[A-Za-z -]+$" title="Sub Disctrict Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityorregency" class=" control-label col-md-4">{{ Lang::get('core.cityorregency') }} (Kota / Kabupaten)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityorregency' id='cityorregency' pattern="^[A-Za-z -]+$" title="City or Regency Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="province" class=" control-label col-md-4">{{ Lang::get('core.province') }} (Provinsi)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='province' id='province' pattern="^[A-Za-z -]+$" title="Province Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="postalcode" class=" control-label col-md-4">{{ Lang::get('core.postalcode') }} (Kode POS) <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='postalcode' id='postalcode'  pattern="[0-9]{5}" title="Add Postal Code Must Contain only Number with 5 charaters" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  	  
	  	  @endif
	  	  

        
        <!-- your interest -->      
        
        	<h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Your Interests</h5>
    <h5 style="margin-left: 40px; padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px; font-style:italic;">Career Preference (Thick Max. 5 preferences)</h5>   	          
    	         

     <div class="form-group" >    
    	<div class="col-md-8" id="yourinterests" style="width: 461px; margin-left: 77px; margin-right: 26px;">
		@if(count($jobfield) >= 0)
			@foreach($jobfield as $cat)
				   <div>
						<input type="checkbox" name="careerpreference[]" value={{ $cat->id }} class="cps_options" <?php if(in_array($cat->id, $explode_career)){
            echo "checked"; }?> >
						<label for={{ $cat->id }}>{{ $cat->jobfield }}</label>
					</div>
			@endforeach
		@endif
        </div>   
        </div>   
          
        <!-- your interest -->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
	  
	  <!-- expected salary --> 
	  @if (count($applicantData) > 0)
	   <div class="form-group " class="col-md-12" >
	   
	   <!-- edit format number -->
	   <div class="row" id="showsalary" class="col-md-5">
		   <label for="expectedsalaryfrom_view" class=" control-label col-md-2" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-2">					
			<input  type='text' name='expectedsalaryfrom_view' id='expectedsalaryfrom_view' value="{{ number_format($applicantData->expectedsalaryfrom,2) }}" required     
										  class='form-control input-sm ' /> 
			</div>
			
			<label for="expectedsalaryupto_view" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
				<input  type='text' name='expectedsalaryupto_view' id='expectedsalaryupto_view' value="{{ number_format($applicantData->expectedsalaryupto, 2) }}" required     
										  class='form-control input-sm ' /> 
			</div>
			<td><a class="fa fa-pencil" id='clicker'></a></td> 
			</div>
							
			
			<div class="row" id="sendsalary" class="col-md-5">
			    			<label for="expectedsalaryfrom" class=" control-label col-md-2" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-2">					
    			<input  type='text' name='expectedsalaryfrom' id='expectedsalaryfrom' pattern="[0-9]*" title="Expected Salary From harus Angka tanpa Koma dan titik" value="{{$applicantData->expectedsalaryfrom}}" required     
    										  class='form-control input-sm ' /> 
			</div>	
			    			<label for="expectedsalaryupto" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
    			<input  type='text' name='expectedsalaryupto' id='expectedsalaryupto ' pattern="[0-9]*" title="Expected Salary Up To harus Angka tanpa Koma dan titik" value="{{$applicantData->expectedsalaryupto}}" required     
    										  class='form-control input-sm ' /> 
			</div> 
			<td><a class="fa fa-floppy-o" id='save'></a></td>
			</div>	
			<!-- edit format number -->	
			
	  </div>   
	  @else
	   <div class="form-group  " >
		<label for="expectedsalaryfrom" class=" control-label col-md-3" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">			
			<input  type='number' name='expectedsalaryfrom' id='expectedsalaryfrom' pattern="[0-9]*"  title="Expected Salary From harus Angka tanpa Koma dan titik" value="" required     
										  class='form-control input-sm ' /> 
			</div>			
			<label for="expectedsalaryupto" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
			<input  type='number' name='expectedsalaryupto' id='expectedsalaryupto' pattern="[0-9]*"  title="Expected Salary Up To harus Angka tanpa Koma dan titik" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>   
	  
	  @endif
	  <!-- expected salary -->
	  
	  
	  <!-- additional information -->
	  <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
        color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Additional Information</h5>
	  @if (count($applicantData) > 0)
	  
	  <div class="form-group  " >
		<label for="optyfrom" class=" control-label col-md-4">{{ Lang::get('core.optyfrom') }}<span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php $optyfrom = explode(',',$applicantData->optyfrom);
				$optyfrom_opt = array( 'Campus/University Career Center' => 'Campus/University Career Center' ,  'Company/Brand Website' => 'Company/Brand Website' ,  'Print Advertisement' => 'Print Advertisement' , 'Friends/Relatives' => 'Friends / Relatives' ,  'ACC Employee' => 'ACC Employee' , 'Others' => 'Others' ,); ?>
					<select name='optyfrom' rows='5' required  class='select2 '  > 
						<?php 
						foreach($optyfrom_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($applicantData->optyfrom == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>
	  
	  @else
    	<div class="form-group  " >
    		<label for="optyfrom" class=" control-label col-md-4">{{ Lang::get('core.optyfrom') }}<span class="asterix"> * </span></label>
    			<div class="col-md-8">
    				<?php 
    				$optyfrom_opt = array( 'Campus/University Career Center' => 'Campus/University Career Center' ,  'Company/Brand Website' => 'Company/Brand Website' ,  'Print Advertisement' => 'Print Advertisement' , 'Friends/Relatives' => 'Friends / Relatives' ,  'ACC Employee' => 'ACC Employee' , 'Others' => 'Others' ,); ?>
    					<select name='optyfrom' rows='5' required  class='select2 '  > 
    						<?php 
    						foreach($optyfrom_opt as $key=>$val)
    						{
    						    echo "<option  value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 
    						}						
    						?></select> 
    				</div> 
    			</div>
	  @endif
	  
	  <!-- additional information --> 
        
		<div class="form-group  " >
		<div class="col-md-12" style="margin-left: 77px; margin-right: 26px;">
        <!-- <input type="checkbox" name="terms" value="agreed" id="terms" onclick="if(!this.form.terms.checked){alert('You must agree to the terms first.');return false}" required /> Here by I delare that all the information above are true and correct. I am responsible for any untrue statement.
        -->
        </div>
        </div>
	  
	<div class="form-group">
		<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
		<div class="col-md-8 example">
			<button class="btn btn-danger " type="submit" id="submit" onclick="flushsession()"> {{ Lang::get('core.sb_savechanges') }} </button>
		 </div> 
	  </div>   
 
	{!! Form::close() !!}	
  </div>
  <!--backup applicant document-->  
        
			<!-- <div class="table-responsive" style="padding-bottom: 70px;">-->		
		    <!-- <table class="table table-striped" style="width: 931px;">-->
		    

            	
            	
	 
	 <!-- </div>-->
	 </div>
<!-- 	 </table> -->
			
			

		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});

	//tambahan untuk salaryupto yang gaada datanya

	document.getElementById("expectedsalaryfrom_view").disabled = true;
	document.getElementById("expectedsalaryupto_view").disabled = true;
	document.getElementById("sendsalary").style.display = "none";

	$("#clicker").click(function(){	
		document.getElementById("expectedsalaryfrom_view").disabled = false;
		document.getElementById("expectedsalaryupto_view").disabled = false;
		document.getElementById("sendsalary").style.display = "block";
		document.getElementById("showsalary").style.display = "none";
		document.getElementById("save").style.display = "none"; // savenya di ilangin	
		
	});

	$("#save").click(function(){	
		document.getElementById("expectedsalaryfrom_view").disabled = true;
		document.getElementById("expectedsalaryupto_view").disabled = true;
		document.getElementById("showsalary").style.display = "block";
		document.getElementById("sendsalary").style.display = "none";

	});
	//tambahan untuk salaryupto yang ada datanya
	
	</script>		 
@stop