@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('applicant-menu/ApplicantDetail/'.$edu_exp) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<h5>Applicant Detail</h5>
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
               <div class="class-md-12" style="text-align: left"> 
               	<a href="{{ url('applicant-menu/appmenu_add_edu_exp/'.$edu_exp)}}">
					<button class="btn btn-primary btn-xs" type="button" title="View Answer">
					<i class="fa fa-file-text-o" style="color: white; font-style: normal; font-size:14px;"> Add Form</i>
					</button></a>
			&nbsp;<br>
			</div>
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped" style="width: 100%;">
                <thead>
                       <tr>
                               <th>Last Education</th>
                               <th>University or School</th>
							   <th>Other</th>
                               <th>Faculty</th>
                               <th>Major</th>
                               <th>Gpa</th>
                               <th>From</th>
                               <th>Until</th>
				<th colspan="2" style="text-align: center">Action<th>
                       </tr>
               </thead>
		@if (count($rowData) >= 1)
               <tbody>
               @foreach($rowData as $items_edu)
                       <tr>
                               <td>{{$items_edu->lasteducation}}</td>
                               <td>{{$items_edu->name}}</td>
							   <td>{{$items_edu->otherunivorschool}}</td>
                               <td>{{$items_edu->faculty}}</td>
                               <td>{{$items_edu->major}}</td>
                               <td>{{$items_edu->gpa}}</td>
                               <td>{{$items_edu->startdate}}</td>
                               <td>{{$items_edu->endate}}</td>  
				                               <td>
                               <a href="{{url('applicant-menu/edubackdetail/appmenu_edit_edu_exp/'.$items_edu->id_edu_back)}}">
									<button class="btn btn-primary btn-xs" type="button" title="View Answer" style="background-color:#43ad0d; border-color:#43ad0d">
									<i class="fa fa-pencil-square-o" style="color: white; font-style: normal; font-size:12px;"> Edit</i>
								</button></a></td>  
												<td><a type="button" onclick="SximoConfirmDelete('{{ url('dashboard/deleteeduappmenu/'.$items_edu->id_edu_back) }}')">
									<button class="btn btn-primary btn-xs" type="button" title="Delete Record" style="background-color:#e44521; border-color:#e44521">
									<i class="fa fa-trash" style="color: white;font-size:12px!important;"><i style="font-family: Segoe UI; font-style:normal;"> Delete</i></i>
								</button></a></td>                          
                       </tr>
               @endforeach
               </tbody>
                @else
		                  <a href="{{ url('applicant-menu/appmenu_add_edu_exp/'.$edu_exp)}}">
					<!--<button class="btn btn-primary btn-xs" type="button" title="View Answer">-->
					<!--<i class="fa fa-file-text-o" style="color: white; font-style: normal; font-size:14px;"> Add Form </i>-->
					<!--</button>-->
</a>
					&nbsp;
					<div></div>
					<br>
			   <tbody>
				<tr><td colspan="7">No Data</td></tr>
				</tbody>
    	  @endif
			<!-- End Table Grid -->

	  
	 
	 </div>
	 </div>
			
			

		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop