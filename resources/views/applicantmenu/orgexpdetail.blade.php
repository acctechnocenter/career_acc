@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
						<a href="{{ url('applicant-menu/ApplicantDetail/'.$org_exp) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<h5>View Organizational Experience</h5>
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
  <div class="class-md-12" style="text-align: left"> 
               	<a href="{{ url('applicant-menu/appmenu_add_org_exp/'.$org_exp)}}">
    					<button class="btn btn-primary btn-xs" type="button" title="View Answer">
    						<i class="fa fa-file-text-o" style="color: white; font-style: normal; font-size:14px;"> Add Form </i>
    					</button>
					</a>
			&nbsp;<br>
			</div>
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped" style="width: 931px;">
			<thead>
                       <tr>
                               <th>Name</th>
                               <th>Scope</th>
                               <th>Startdate</th>
                               <th>Endate</th>
                               <th>Role Function</th>
                               <th>Role or Position</th>
				<th colspan="2" style="text-align: center">Action</th>
                       </tr>
               </thead>
               @if (count($rowData) >= 1)
               <tbody>
               @foreach($rowData as $items)
                       <tr>
                               <td>{{$items->organizationname}}</td>
                               <td>{{$items->organizationscope}}</td>
                               <td>{{$items->organization_experience_startdate}}</td>
                               <td>{{$items->organization_experience_endate}}</td>
                               <td>{{$items->rolefunction}}</td>
                               <td>{{$items->roleorposition}}</td>
				<td><a href="{{url('applicant-menu/orgexpdetail/appmenu_edit_org_exp/'.$items->id_org_exp)}}">
									<button class="btn btn-primary btn-xs" type="button" title="View Answer" style="background-color:#43ad0d; border-color:#43ad0d">
									<i class="fa fa-pencil-square-o" style="color: white; font-style: normal; font-size:12px;"> Edit</i>
								</button></a></td>
		<td><a type="button" onclick="SximoConfirmDelete('{{ url('dashboard/deleteorgappmenu/'.$items->id_org_exp) }}')">
									<button class="btn btn-primary btn-xs" type="button" title="Delete Record" style="background-color:#e44521; border-color:#e44521">
									<i class="fa fa-trash" style="color: white;font-size:12px!important;"><i style="font-family: Segoe UI; font-style:normal;"> Delete</i></i>
								</button></a></td>
                       </tr>
               @endforeach
               </tbody>
                @else
			<a href="{{ url('applicant-menu/appmenu_add_org_exp/'.$org_exp)}}">
    					<!--<button class="btn btn-primary btn-xs" type="button" title="View Answer">-->
    						<!--<i class="fa fa-file-text-o" style="color: white; font-style: normal; font-size:14px;"> Add Form </i>-->
    					<!--</button>-->
					</a>
					&nbsp;
					<div></div>
					<br>
			   <tbody>
				<tr><td colspan="6">No Data</td></tr>
				</tbody>
    	  @endif
			<!-- End Table Grid -->

	  
	 
	 </div>
	 </div>		

		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop