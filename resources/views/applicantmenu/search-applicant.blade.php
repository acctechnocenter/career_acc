<div>
{!! Form::open(array('url'=>'result-search-applicant', 'class'=>'form-horizontal validated' )) !!}
	<div class="form-group  " >
		<label for="appfirstname" class="control-label col-md-4 text-left"> First Name</label>
		<div class="col-md-6">
			<input  type='text' name='appfirstname' id='appfirstname' class='form-control input-sm ' /> 
		</div> 
		<div class="col-md-2">
	
		 </div>
	 </div>
	<div class="form-group  " >
		<label for="applastname" class="control-label col-md-4 text-left"> Last Name</label>
		<div class="col-md-6">
			<input  type='text' name='applastname' id='applastname' class='form-control input-sm ' /> 
		</div> 
		<div class="col-md-2">
	
		 </div>
	 </div>
	<div class="form-group  " >
		<label for="appemail" class="control-label col-md-4 text-left"> Email</label>
		<div class="col-md-6">
			<input  type='text' name='appemail' id='appemail' class='form-control input-sm ' /> 
		</div> 
		<div class="col-md-2">
	
		 </div>
	 </div>
	 {!! Form::submit('Search', ['class' => 'btn btn-sm btn-primary']) !!} 
{!! Form::close() !!}
</div>