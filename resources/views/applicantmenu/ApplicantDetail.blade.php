@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('applicant-menu') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<h5>Applicant Detail</h5>
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
			@if(count($rowData) > 0)
					@foreach ($rowData as $row)
			<div class="class-md-12" style="text-align: right"> 
				<div class="class-md-6">
				<td><a href="{{url('applicant-menu/editdataapplicant/'.$row->idApplicant)}}">
				<button class="btn btn-primary btn-xs" type="button">
				<i class="fa fa-folder-open-o" style="color: white; font-size:12px;"  ><i style="font-family: Segoe UI; font-style:normal;"> Edit Personal Detail</i></i></button></a></td> </button>
				</div>
			</div>
			@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped">
				<tbody>	
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
					<h4>Personal Detail</h4>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Status</strong></td>
						<td>
							@if($row->active == 1)
								<span class="label label-primary"> Active</span>
							@elseif($row->active == 0)
								<span class="label label-danger"> Not Active</span>
							@elseif($row->active == 2)
								<span class="label label-warning"> Banned</span>
							@endif
						</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>First Name</strong></td>
						<td>{{ $row->first_name }}</td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Last Name</strong></td>
						<td>{{ $row->last_name }}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Gender</strong></td>
						<td>{{ $row->gender }}</td>
						
					</tr>					
					<tr>
						<td width='30%' class='label-view text-left'><strong>Date of Birth</strong></td>
						<td>{{ $row->dateofbirth }}</td>
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Province of Birth</strong></td>
						<td>{{ $row->provinceofbirth }}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>City of Birth</strong></td>
						<td>{{ $row->cityofbirth }}</td>
					</tr>
					
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Identity Card Number</strong></td>
						<td>{{ $row->identitycardnumber }}</td>
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Home Phone</strong></td>
						<td>{{ $row->homephone }}</td>
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Mobile Phone</strong></td>
						<td>{{ $row->mobilephone }}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>NPWP</strong></td>
						<td>{{ $row->npwp }}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Marital Status</strong></td>
						<td>{{ $row->maritalstatus }}</td>
					</tr>
					@if(!empty($row->marrieddate))
						<tr>
							<td width='30%' class='label-view text-left'><strong>Married Date</strong></td>
							<td>{{ $row->marrieddate }}</td>
						</tr>
					@endif
					<tr>
						<td width='30%' class='label-view text-left'><strong>Religion</strong></td>
						<td>{{ $row->religion }}</td>
					</tr>
					<tr>
						<td style="border: none"><h5 style="font-size: 18px;">Home Address</h5></td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Country</strong></td>
						<td>{{ $row->country }}</td>
					</tr>
					
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Address</strong></td>
						<td>{{ $row->address }}</td>
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-left'><strong>Sub-district (Kecamatan)</strong></td>
						<td>{{ $row->subdisctrict }}</td>
					</tr>				
					<tr>
						<td width='30%' class='label-view text-left'><strong>City or Regency (Kota / Kabupaten)</strong></td>
						<td>{{ $row->cityorregency }}</td>
						
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Province (Provinsi)</strong></td>
						<td>{{ $row->province }}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Postal Code (Kode POS)</strong></td>
						<td>{{ $row->postalcode }}</td>
					</tr>
					
					<tr>
						<td style="border: none"><h5 style="font-size: 18px;">Your Interests</h5></td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Career Preference</strong></td>
						<?php $careerpref = AccHelpers::pref_career($row->Careerpreference);?>
						<td>{{ $careerpref }}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Expected Salary</strong></td>
						<td>{{ number_format($row->expectedsalaryfrom,0) }}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>Expected Salary Up To</strong></td>
						<td>{{ number_format($row->expectedsalaryupto,0) }}</td>
					</tr>
					<tr>
						<td style="border: none"><h5 style="font-size: 18px;">Additional Information</h5></td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-left'><strong>How did you here about this opportunity?</strong></td>
						<td>{{ $row->optyfrom }}</td>
					</tr>
					
									@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				
					</tbody>	
		    </table>

		    <!-- file attachment -->
              <div class="row">
                <div class="col-md-12">
            		<div class="table-responsive">
            		<table class="table table-striped">
            		<h4>Background</h4>
            			<tbody>
            			<tr>
						<td width='30%' class='label-view text-left'><strong>Educational Background</strong></td>
						<td><a href="{{url('applicant-menu/edubackdetail/'.$row->idApplicant)}}"><button class="btn btn-primary btn-xs" type="button" title="View Detail" style="background-color: #62cb31; border-color:#62cb31">
						<i class="fa fa-folder-open-o" style="color: white"><i style="font-family: Segoe UI; font-style:normal;"> View Detail</i></i></button></a></td>
    					</tr>
    					<tr>
    						<td width='30%' class='label-view text-left'><strong>Organizational Experiences</strong></td>
    						<td><a href="{{url('applicant-menu/orgexpdetail/'.$row->idApplicant)}}"><button class="btn btn-primary btn-xs" type="button" title="View Detail" style="background-color: #62cb31; border-color:#62cb31">
    						<i class="fa fa-folder-open-o" style="color: white"><i style="font-family: Segoe UI; font-style:normal;"> View Detail</i></i></button></a></td>
    					</tr>
    					<tr>
    						<td width='30%' class='label-view text-left'><strong>Working Experiences</strong></td>
    						<td><a href="{{url('applicant-menu/workexpdetail/'.$row->idApplicant)}}"><button class="btn btn-primary btn-xs" type="button" title="View Detail" style="background-color: #62cb31; border-color:#62cb31">
    						<i class="fa fa-folder-open-o" style="color: white"><i style="font-family: Segoe UI; font-style:normal;"> View Detail</i></i></button></a></td>
    					</tr>
            			</tbody>
            		</table>
                    </div>
                   <br>
                   
              </div>
              </div>
              <!-- file attachment -->
		    
		   <!-- file attachment -->
  <div class="row" id="fileattachment">   
    <div class="col-md-12">
            <div class="class-md-12" style="text-align: right"> 
				<div class="class-md-6">
    				<td><a href="{{url('applicant-menu/editfileattchment/'.$row->idApplicant)}}">
    				<button class="btn btn-primary btn-xs" type="button">
    				<i class="fa fa-folder-open-o" style="color: white; font-size:12px;"><i style="font-family: Segoe UI; font-style:normal;"> Upload File Attachment</i></i></button></a></td> </button>
				</div>
			</div> 
			
		<?php $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training',
		    'surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga',
		    'akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3',
		    'npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];?>
		<div class="table-responsive">
		<h4>File Attachment</h4>
		<table class="table table-striped">
			<tbody>
		@foreach($jenisFile as $jns)											
		<?php $label = strtoupper(str_replace("_"," ",$jns)); ?>
			<tr>
				<td width='20%' class='label-view'>
				{{ $label }}
				</td>
                <td>  
							@if(count($fileattachData) > 0)
								@foreach($fileattachData as $filed)								                		  
									@if($filed->type_file == $jns)
										@if(!empty($filed->link_file))									
									<i class="fa fa-check-square-o" style="color: green;font-size: 21px;"></i>
									&nbsp;
									&nbsp;
									&nbsp;
									<a href={{ $filed->link_file }} title="Download file" ><i class="fa fa-download"></i></a>
									&nbsp;
									&nbsp;
									&nbsp;
									<a type="button" onclick="SximoConfirmDelete('{{ url('dashboard/deletefileattchmentappmenu/'.$filed->id_file) }}')">
									<i class="fa fa-trash" style="color: red;font-size:12px!important;"></i>
									</a>
										@endif	
									@endif
								@endforeach
							@endif		
                </td>   
				</tr>
		@endforeach
			</tbody>
		</table>
        </div>
       <br>
       
  </div>
  </div>
  <!-- file attachment -->

			<input type="hidden" name="action_task" value="" />
			
			</div>
			<!-- End Table Grid -->

	  
	 
	 </div>
	 </div>
			
			

		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop