@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> Edit Working Experience </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'save-appmenu-edit-work-exp', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('applicant-menu/workexpdetail/'.$rowData->id_user) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
<!-- 				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button> -->
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="container" >
  <div style="background-color: white; width: 921px;" >
  	{!! Form::hidden('id_edit', $rowData->id_wor_exp ) !!}
	{!! Form::hidden('id_user', $rowData->id_user) !!} 
	  <div class="form-group  " >
		<label for="company" class=" control-label col-md-3">{{ Lang::get('core.company') }}</label>
			<div class="col-md-9">
			<input  type='text' name='company' id='company' value="{{ $rowData->company }}"      
										  class='form-control input-sm ' required=""  data-parsley-company /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="workingexperienceperiodstartdate" class=" control-label col-md-3" style="width: 238px;">{{ Lang::get('core.workingexperienceperiodstartdate') }}</label>
			<div class="col-md-3">			
			<input  type='date' name='workingexperienceperiodstartdate' id='workingexperienceperiodstartdate' value="{{ $rowData->workingexperienceperiodstartdate }}"      
										  class='form-control input-sm ' required /> 
			</div>
			<label for="workingexperienceperiodenddate" class=" control-label col-md-3" style="width:98px">{{ Lang::get('core.workingexperienceperiodenddate') }}</label>
			<div class="col-md-3">
			<input  type='date' name='workingexperienceperiodenddate' id='workingexperienceperiodenddate' value="{{ $rowData->workingexperienceperiodenddate }}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  
	  <div class="form-group  " >
		<label for="position" class=" control-label col-md-3">{{ Lang::get('core.position') }}</label>
			<div class="col-md-9">
			<input  type='text' name='position' id='position' value="{{ $rowData->position }}"      
										  class='form-control input-sm ' required=""  data-parsley-position/> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="category" class=" control-label col-md-3">{{ Lang::get('core.category') }}</label>
			<div class="col-md-9">
				<?php $category = explode(',',$rowData->category);
				$category_opt = array( 'Marketing/Sales' => 'Marketing/Sales' ,  'Operation' => 'Operation' ,  'Human Resource' => 'Human Resource' ,  'Procurement/Purchasing/GA ' => 'Procurement/Purchasing/GA' ,  'Information Technology' => 'Information Technology' ,  'Legal/Litigation' => 'Legal/Litigation' , ); ?>
					<select name='category' rows='5'   class='select2 ' required > 
						<?php 
						foreach($category_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($rowData->category == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>	
			<div class="form-group  " >
		<label for="status_working_experience" class=" control-label col-md-3">{{ Lang::get('core.status_working_experience') }}</label>
			<div class="col-md-9">
				<?php $status_working_experience = explode(',',$rowData->status_working_experience);
				$status_working_experience_opt = array( 'Fulltime' => 'Fulltime' ,  'Freelance' => 'Freelance' ,  'Internship' => 'Internship' , ); ?>
					<select name='status_working_experience' rows='5'  class='select2 ' required > 
						<?php 
						foreach($status_working_experience_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($rowData->status_working_experience == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>	
		<div class="form-group  " >
		<label for="jobdescription" class=" control-label col-md-3">{{ Lang::get('core.jobdescription') }}</label>
			<div class="col-md-9">
			<textarea class="field" name="jobdescription" cols="111" rows="5" style="width:605px" required >{{ $rowData->jobdescription }}</textarea> 
			</div> 
	  </div>
	  <div class="form-group  " >
	  <div class="row" id="showsalary" class="col-md-12">
		<label for="salary_view" class=" control-label col-md-3" style="width: 253px;">{{ Lang::get('core.salary') }}</label>
			<div class="col-md-8">
			<input  type='text' name='salary_view' id='salary_view' value="{{ number_format($rowData->salary, 2) }}"      
										  class='form-control input-sm ' style="width:583px" /> 
			</div> 	
			<td class="col-md-1"><a class="fa fa-pencil" id='clicker'></a></td> 		
	  </div>
	  <div class="row" id="sendsalary" class="col-md-12">
		<label for="salary" class=" control-label col-md-3" style="width: 253px;">{{ Lang::get('core.salary') }}</label>
			<div class="col-md-8">
			<input  type='number' name='salary' id='salary' value="{{ $rowData->salary }}"      
										  class='form-control input-sm ' required /> 
			</div> 			
	  </div>
	  <td class="col-md-1"><a class="fa fa-floppy-o" id='save'></a></td>
	  </div>
	  
	  <div class="form-group  " >
		<label for="Reasonofleaving" class=" control-label col-md-3">{{ Lang::get('core.Reasonofleaving') }}</label>
			<div class="col-md-9">
			<textarea class="field" name="Reasonofleaving" cols="111" rows="5" style="width:605px">{{ $rowData->Reasonofleaving }}</textarea> 
			</div> 
	  </div>
	 </div>
	 </div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">

   $(document).ready(function() {
	   $("form[name=myForm]").parsley();
	   
	   window.Parsley.addValidator('company', {
		   validateString: function(value) {
		     var patt = new RegExp("^[A-Za-z -]+$");
		     return patt.test(value);
		   },
		   messages: {
		     en: 'Company Must Contain only letters.'
		   }
		 });

	   window.Parsley.addValidator('position', {
		   validateString: function(value) {
		     var pst = new RegExp("^[A-Za-z -]+$");
		     return pst.test(value);
		   },
		   messages: {
		     en: 'Position Must Contain only letters.'
		   }
		 });		 
	 });
   
   document.getElementById("salary_view").disabled = true;
   document.getElementById("sendsalary").style.display = "none";
   document.getElementById("save").style.display = "none";

   $("#clicker").click(function(){	
 	    document.getElementById("salary_view").disabled = true;
		document.getElementById("sendsalary").style.display = "block";
		document.getElementById("showsalary").style.display = "none";
		document.getElementById("save").style.display = "none"; // savenya di ilangin	
		
	});

   $("#save").click(function(){	
		document.getElementById("salary_view").disabled = true;
		document.getElementById("sendsalary").style.display = "none";
	});
	
   
	$(document).ready(function() {  

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("prescreeningresult/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop