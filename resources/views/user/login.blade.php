<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
@extends('layouts.login')

@section('content')
	

		<div class="text-center">
		
		<img src="{{ asset('frontend/default/images/logo-acc.png') }}" alt="{{ config('cnf_appname') }}" style="width: 180px;height: 65px;"/>
		<h3 style="color: #86c5e8; margin-bottom: 20px;">Sign In</h3>
        <!--<img src="{{ asset('uploads/images/backend-logo-light.png') }}" alt="{{ config('sximo.cnf_appname') }}"  />-->
	    </div>
    <!--<p class="text-center m-t"> {{ config('sximo.cnf_appdesc') }}  </p>-->
		
		
		
			<div class="ajaxLoading"></div>
			<p class="message alert alert-danger " style="display:none;"></p>	
	 
		    	@if(Session::has('status'))
		    		@if(session('status') =='success')
		    			<p class="alert alert-success">
							{!! Session::get('message') !!}
						</p>
					@else
						<p class="alert alert-danger">
							{!! Session::get('message') !!}
						</p>
					@endif		
				@endif

			<ul class="parsley-error-list">
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>		
			

		<div class="tab-content">
			<div class="tab-pane active m-t" id="tab-sign-in">
	 		{!! Form::open(array('url'=>'user/signin', 'class'=>'form-vertical','id'=>'LoginAjax' , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				
            <div class="form-group has-feedback animated fadeInLeft delayp1">
              <div class="input-group">               
               <input type="text" name="email" placeholder="{{ Lang::get('Username / Email Address') }}" class="form-control" required="email" style="height: 35.979166px;" />
               <div class="input-group-addon">
            	<i class="fa fa-envelope"></i> 
               </div>
              </div>
             </div> 
             
             <div class="form-group has-feedback  animated fadeInRight delayp1">
              <div class="input-group">               
               <input type="password" name="password" placeholder="{{ Lang::get('core.password') }}" class="form-control" required="true" />
                  
               <div class="input-group-addon">
            	<i class="fa fa-lock" style="font-size: 15px;"></i> 
               </div>
              </div>
             </div> 
							
				<select name="cars" style=" width: 319px; height: 31.979166px;border-top-color: #cccccc;border-right-color: #cccccc; border-left-color: #cccccc;border-bottom-color: #cccccc;display:none;" class="form-group animated fadeInRight delayp1">
					<option value="english">English</option>
                    <option value="indonesia">Indonesia</option>                    
                  </select>
                     @php
                      $redirect = !empty($_GET['redirect']) ? $_GET['redirect'] : '';
                    @endphp
                  <input type="hidden" name="redirect" value="{!! $redirect !!}" />

				<div class="form-group has-feedback  animated fadeInRight delayp1" style="margin-top: 0px; margin-bottom: 0px;">
					<label> Remember  ?	</label>
					<input type="checkbox" name="remember" value="1" />
				</div>


				@if(config('sximo.cnf_recaptcha') =='true') 
				<div class="form-group has-feedback  animated fadeInLeft delayp1">
					<label class="text-left"> Are u human ? </label>	
					<div class="g-recaptcha" data-sitekey="{{config('sximo.cnf_recaptchapublickey')}}"></div>
					
					<div class="clr"></div>
				</div>	
			 	@endif	

				<div class="form-group animated fadeInRight delayp1">
					<button type="submit" class="btn btn-primary btn-block col-md-12 text-center" style="float:right; background-color: #f28d0a!important; border-color: #f28d0a !important;width: 94px;"> Sign In </button>
				</div>			 	




<!-- 				<div class="form-group   animated fadeInLeft delayp1" >					        -->
<!-- 						<p class="">						 -->
<!-- 							<a href="javascript:void(0)" class="forgot"> @lang('core.forgotpassword') ? </a> |  -->
<!-- 							<a href="{{ url('user/register')}}"> @lang('core.registernew') </a> -->
<!-- 						</p>					 -->
<!-- 				</div>	 -->
				<div class="animated fadeInUp delayp1">
			<div class="form-group  ">
				@if($socialize['google']['client_id'] !='' || $socialize['twitter']['client_id'] !='' || $socialize['facebook'] ['client_id'] !='') 
				
				<p class="text-muted text-center"><b> {{ Lang::get('core.loginsocial') }} </b>	  </p>
				
				<div style="padding:15px 0;">
					@if($socialize['facebook']['client_id'] !='') 
					<a href="{{ url('user/socialize/facebook')}}" class="btn btn-success"><i class="icon-facebook"></i> Facebook </a>
					@endif
					@if($socialize['google']['client_id'] !='') 
					<a href="{{ url('user/socialize/google')}}" class="btn btn-danger"><i class="icon-google"></i> Google </a>
					@endif
					@if($socialize['twitter']['client_id'] !='') 
					<a href="{{ url('user/socialize/twitter')}}" class="btn btn-info"><i class="icon-twitter"></i> Twitter </a>
					@endif
				</div>
				@endif
			</div>			

				  <p style="padding:5px 0" class="text-center">
<!-- 				  <a href="{{ url('')}}"> {{ Lang::get('core.backtosite') }} </a>   -->
			   	</p>
			   	</div>	
			   </form>			
			</div>	
		

		<div class="tab-pane  m-t" id="tab-forgot" style="display: none">	

			
			{!! Form::open(array('url'=>'user/request', 'class'=>'form-vertical', 'parsley-validate'=>'','novalidate'=>' ')) !!}
			   <div class="form-group has-feedback">
			   <div class="">
					<label>{{ Lang::get('core.enteremailforgot') }}</label>
					<input type="text" name="credit_email" placeholder="{{ Lang::get('core.email') }}" class="form-control" required/>
				</div> 	
				</div>
				<div class="form-group has-feedback">    
					<a href="javascript:;" class="forgot btn btn-warning"> Cancel </a>     
			      <button type="submit" class="btn btn-default pull-right"> {{ Lang::get('core.sb_submit') }} </button>        
			  </div>
			  
			  <div class="clr"></div>

			  
			</form>		
		</div>
		
	</div>
	<div>
			<p style="padding: 10px 10px 10px 10px; margin-bottom:0px; background-color:white;" class="text-center">
			<a href="{{ url('')}}"> {{ Lang::get('core.backtosite') }} </a>  
		</div> 
	<div>
        	<p class="" style="text-align: center;padding-bottom: 20px; padding-top: 20px;background-color: grey; margin-bottom:0px;
                color: white;">						
							<a href="javascript:void(0)" class="forgot" style="color:white"> @lang('core.forgotpassword') ? </a> | 
							<a href="{{ url('user/register')}}" style="color:white"> @lang('core.registernew') </a>
			</p>	
        </div>
 



<script type="text/javascript">
	$(document).ready(function(){

		$('.forgot').on('click',function(){
			$('#tab-forgot').toggle();
			$('#tab-sign-in').toggle();
		})
		var form = $('#LoginAjax'); 
		form.parsley();
		form.submit(function(){
			
			if(form.parsley().isValid()){			
				var options = { 
					dataType:      'json', 
					beforeSubmit :  showRequest,
					success:       showResponse  
				}  
				$(this).ajaxSubmit(options); 
				return false;
							
			} else {
				return false;
			}		
		
		});

	});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		window.location.href = data.url;	
		$('.ajaxLoading').hide();
	} else {
		$('.message').html(data.message)	
		$('.ajaxLoading').hide();
		$('.message').show(data.message)	
		return false;
	}	
}	
</script>

@stop