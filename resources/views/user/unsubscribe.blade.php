<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ $title }} | {{ config('sximo.cnf_appname') }}</title>
   <link rel="shortcut icon" href="{{ asset('favicon.ico')}}" type="image/x-icon">
    <!-- CSS Files -->
    <link href="{{ asset('frontend/default/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('frontend/default/style.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('sximo5/fonts/icomoon.css') }}" rel="stylesheet" />
    <link href="{{ asset('sximo5/fonts/awesome/css/font-awesome.css') }}" rel="stylesheet" />
	<link href="{{ asset('frontend/default/addons/jquery.smartmenus.bootstrap.css') }}" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project --> 
     <script src="{{ asset('frontend/default/js/app.js') }}"></script>
	 <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>
<div class="unsubscribe">
	<img src="{{ asset('frontend/default/images/logo-acc.png') }}" />
	<div class="content-unsubscribe">
		<h1>You are now unsubscribed</h1>
	</div>
</div>

<script src="{{ asset('frontend/default/js/script.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/default/js/jquery.smartmenus.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/default/addons/jquery.smartmenus.bootstrap.min.js') }}"></script>
</body>
</html>