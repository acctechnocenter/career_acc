<?php 
return [
'cnf_appname' 			=> 'ACC Career',
'cnf_appdesc' 			=> 'Web Career ACC',
'cnf_comname' 			=> 'Astra Credit Companies',
'cnf_email' 			  => 'info@mycompanyname.com',
'cnf_metakey' 			=> 'career, acc, web',
'cnf_metadesc' 		  => 'Write description for your site',
'cnf_group' 			=> '3',
'cnf_activation' 		=> 'confirmation',
'cnf_multilang' 		=> '0',
'cnf_lang' 			=> 'en',
'cnf_regist' 			=> 'true',
'cnf_front' 			=> 'true',
'cnf_recaptcha' 		=> 'false',
'cnf_theme' 			=> 'default',
'cnf_backend' 			=> 'minimal',
'cnf_recaptchapublickey' => '6Le4xHEUAAAAADJPOP5eKoklkiPSpRFpvd-pGr0N',
'cnf_recaptchaprivatekey' => '6Le4xHEUAAAAACFtds7k44oqqy5CxObCIs0c8LtA',
'cnf_mode' 			=> 'production',
'cnf_logo' 			=> 'backend-logo.xlsx',
'cnf_allowip' 			=> '',
'cnf_restrictip' 		=> '192.116.134 , 194.111.606.21',
'cnf_mail' 			=> 'phpmail',
'cnf_maps' 			=> 'AIzaSyAayRr-0yZ1ejGGX6l--XDVbHWU85wPj9U',
'cnf_date' 			=> 'Y-m-d',
'limit_login_time' => '5',
'limit_login_attempts' => '3',
];
