<?php

use Illuminate\Database\Seeder;

class MenuQuestionModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $module = \DB::table('tb_menu')->select('module')->where('module','=','questionlist')->first();
        if($module === null){
            \DB::table('tb_menu')->insert([
                'parent_id' => 0,
                'module' => 'questionlist', 
                'url' => NULL, 
                'menu_name' => 'Question List', 
                'menu_type' => 'internal', 
                'role_id' => NULL, 
                'deep' => NULL, 
                'ordering' => NULL, 
                'position' => 'sidebar', 
                'menu_icons' => 'fa fa-question-circle-o', 
                'active' => '1', 
                'access_data' => '{"1":"1","2":"1","3":"0"}', 
                'allow_guest' => NULL, 
                'menu_lang' => NULL,
            ]);
        }
    }
}
