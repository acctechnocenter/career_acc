<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MenuQuestionModuleSeeder::class);
        $this->call(UserProfileModuleSeeder::class);
        $this->call(JobModuleSeeder::class);
        $this->call(CandidateProfileModuleSeeder::class);  
        $this->call(JobfieldModuleSeeder::class);  
        $this->call(TestimonialModuleSeeder::class);  
        $this->call(SliderModuleSeeder::class);  
        $this->call(PrescreeningResultModuleSeeder::class);
		$this->call(PrescreeningStatusModuleSeeder::class);
		$this->call(SoalModuleSeeder::class);
		$this->call(JawabanModuleSeeder::class);
		$this->call(KategoriSoalModuleSeeder::class);
    }
}
