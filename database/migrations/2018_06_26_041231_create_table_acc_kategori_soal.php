<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccKategoriSoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('acc_kategori_soal', function (Blueprint $table) {
    		$table->increments('id');
			$table->string('name');
			$table->string('description');
    		$table->string('entry_by');
    		$table->dateTime('createdOn');
			$table->string('status');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_kategori_soal');
    }
}
