<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccJobfieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('acc_jobfield', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('jobfield');
    		$table->string('img_jobfield');
    		$table->string('entry_by');
    		$table->datetime('createdOn');
			$table->string('status_jobfield');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('acc_jobfield');
    }
}
