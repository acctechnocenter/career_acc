<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccWorkingExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('acc_working_experience', function (Blueprint $table) {
            
            $table->increments('id_wor_exp');
            $table->integer('id_user');            
            $table->string('company',50);
            $table->date('workingexperienceperiodstartdate',50);
            $table->date('workingexperienceperiodenddate',50);
            $table->string('position',50);
            $table->enum('category',['Marketing/sales','Operation','Human Resource','Finance/Accounting','Procurement/Purchasing/GA','Information Techonology','Legal/Litigation']);
            $table->enum('status_working_experience', ['Fulltime','Freelance','Internship']);
            $table->string('jobdescription',100);
            $table->integer('salary');
            $table->string('Reasonofleaving',100);
            
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_working_experience');
    }
}
