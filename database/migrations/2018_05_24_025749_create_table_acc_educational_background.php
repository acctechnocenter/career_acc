<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccEducationalBackground extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        
        
        Schema::create('acc_educational_background', function (Blueprint $table) {
            
            $table->increments('id_edu_back');
            $table->integer('id_user');
            $table->enum('lastestducation',['SMA','SMK','D1','D2','D3','D4','S1','S2']);
            $table->string('universityorschool',50);
            $table->string('faculty',50);
            $table->string('major',50);
            $table->integer('gpa');
            $table->integer('maxgpa');
            $table->date('startdate',50);
            $table->date('endate',50);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_educational_background');
    }
}
