<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('acc_job', function (Blueprint $table) {
    		$table->increments('id');
    		$table->dateTime('CreatedDate');
    		$table->date('EndDate');
    		$table->string('grade');
    		$table->string('IDJobField');
    		$table->longText('JobDescription');
    		$table->text('JobTitleName');
    		$table->enum('JobCategory', ['Freshgraduate', 'Experience', 'Internship']);
    		$table->datetime('LastModifiedDate');
    		$table->date('StartDate');
    		$table->string('Location');
    		$table->string('GPA');
    		$table->string('Degree');
    		$table->string('MinWorkExp');
    		$table->string('MaxAge');
    		$table->string('Image');
    		$table->text('JobRequirements');
			$table->string('ViewJob');
			$table->string('IdExternal');
			$table->string('StatusJob');
			$table->string('id_kategori_soal');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('acc_job');
    }
}
