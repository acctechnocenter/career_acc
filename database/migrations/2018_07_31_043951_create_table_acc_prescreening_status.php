<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccPrescreeningStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_prescreening_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_job');
            $table->integer('id_user');
            $table->double('skor_akhir');
            $table->string('status_prescreening');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('acc_prescreening_status');
    }
}
